<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeCompanyCategoryForm */

$this->title = Yii::t('app', 'Создать категорию');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Катогории типов компаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-company-category-form-create">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
