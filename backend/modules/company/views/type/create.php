<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeCompanyForm */

$this->title = Yii::t('app', 'Создать тип компании');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы компаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-form-create">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
