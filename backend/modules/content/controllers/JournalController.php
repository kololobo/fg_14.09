<?php

namespace backend\modules\content\controllers;

use Yii;
use common\models\forms\JournalForm;
use common\models\search\JournalSearch;
use backend\modules\translate\models\SourceMessage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\translate\models\SourceMessageSearch;

/**
 * JournalController implements the CRUD actions for JournalForm model.
 */
class JournalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JournalForm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JournalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JournalForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->status === JournalForm::STATUS_RAW) {
            $model->setShow();
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new JournalForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JournalForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JournalForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//        $translate_title = $this->findTranslate($model->title_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setEdit();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
//                'translate_title' => $translate_title,
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing JournalForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionTranslate($id)
    {
        $translate = $this->findTranslate($id);
        return $this->render('translate', [
            'translate' => $translate
        ]);
    }

    /**
     * Finds the JournalForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JournalForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JournalForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findTranslate($id)
    {
        if (($translate = SourceMessage::findOne(['content_id' => $id])) !== null) {
            return $translate;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        $model->setPublish();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->setEdit();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

//    public function actionTranslate()
//{
//
//    $searchModel = SourceMessageSearch::getInstance();
//    $dataProvider = $searchModel->search(\Yii::$app->getRequest()->get());
//
//    return $this->render('../translate/index', [
//        'searchModel' => $searchModel,
//        'dataProvider' => $dataProvider
//    ]);
//}
}
