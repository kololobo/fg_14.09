<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\forms\JournalForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form-form">
    <div class="box box-success" style="margin-bottom: 50px;">
        <div class="box-header with-border"></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">

            <div class="col-md-12">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'full'
                ]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'category')->dropDownList($model->getCategoriesList()) ?>
            </div>

            <div class="col-md-12">

            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать статью') : Yii::t('app', 'Изменить статью'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>










