<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\forms\JournalForm */

$this->title = Yii::t('app', 'Create Journal Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Journal Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
