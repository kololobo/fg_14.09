<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\JournalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Journal Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-12">
                <?= Html::a(Yii::t('app', 'Добавить статью'), ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app', 'Переводы'), ['translate'], ['class' => 'btn btn-info']) ?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12 table-responsive">

<?php Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Текст статьи'),
                'value' => function($data){return StringHelper::truncate($data->content, 100, '...', NULL, TRUE);}
            ],
            [
                'label' => Yii::t('app', 'Фото статьи'),
                'format' => 'raw',
                'value' => function($data){
                    if ($data->photo) {
                        return Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/userpics' . ($data->photo->file_small),[
                        'style' => 'width:100px;'
                    ]);
                    } else{
                        return '<span class="label label-danger">' . Yii::t('app', 'Нет фото') . '</span>';
                    }
                },
            ],
            [
                'attribute' => 'category',
                'value' => function($data){return $data->getCategoriesList()[$data->category];},
                'filter' => $searchModel::getStatusList(),
                'filterInputOptions' => [
                    'class' => 'form-control selectpicker show-tick',
                    'data' => [
                        'style' => 'btn-primary',
                        'title' => Yii::t('app', 'Категория'),
                    ]
                ],
            ],
            [
                'format' => 'html',
                'attribute' => 'status',
                'value' => function($data){return $data->getStatusLabels();},
                'filter' => $searchModel::getStatusList(),
                'filterInputOptions' => [
                    'class' => 'form-control selectpicker show-tick',
                    'data' => [
                        'style' => 'btn-primary',
                        'title' => Yii::t('app', 'Статус'),
                    ]
                ],
            ],
            // 'user_id',
            // 'status',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {translate_title} {translate_content}',
                'buttons'=>
                    [
                        'translate_title' =>function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-flag"></span>',
//                                ['translate', 'id'=>$model->title_id],
                                ['title' => Yii::t('app', 'Переводы заголовка'),
                                    'data-pjax'=> '0'
                                ]
                            );
                        },
                        'translate_content' =>function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-list-alt"></span>',
//                                ['translate', 'id'=>$model->content_id],
                                ['title' => Yii::t('app', 'Переводы статьи'),
                                    'data-pjax'=> '0'
                                ]
                            );
                        }
                    ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>

            </div>
        </div>
    </div>
</section>

