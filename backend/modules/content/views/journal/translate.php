<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\forms\JournalForm */

$this->title = Yii::t('app', 'Перевод заголовка {modelClass}: ', [
    'modelClass' => 'Journal Form',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление журналом'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Переводы');
?>
<div class="journal-form-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <h4> <?= Yii::t('app', 'Оригинальный текст').':</h4> ' ?>
        <p><?= $translate->message ?></p>

    <?php
    Pjax::begin([
        'enablePushState' => false,
        'timeout' => 9000
        ]); ?>
    <?= $this->render('/../../translate/views/manage/_message-tabs', [
        'model' => $translate,
        'key'   => $translate->id
    ]); ?>
    <?= Html::button(Yii::t('app', 'Сохранить'), [
        'onclick' =>
            '$.pjax({
                type: "POST",
                url: "/translate/manage/save/'.$translate->id.'",
                data: jQuery("#translationsForm-'.$translate->id.'").serialize(),
                container: "#translationGrid-'.$translate->id.'",
                push: false,
                scrollTo: false
            })',
        'class' => 'btn btn-success'
    ]) ?>
<?php
Pjax::end(); ?>

</div>
