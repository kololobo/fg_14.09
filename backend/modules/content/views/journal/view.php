<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\forms\JournalForm */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Journal Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-form-view">
    <section class="content">
        <div class="box box-success" style="margin-bottom: 50px;">
            <div class="box-header with-border">
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'html',
                'label' => Yii::t('app', 'Название статьи'),
                'value' => '<b>' . $model->title . '</b>'
            ],
            [
                'format' => 'html',
                'label' => Yii::t('app', 'Текст статьи'),
                'value' => $model->content
            ],
            // 'photo_id',
            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Категория статьи'),
                'value' => $model->getCategoriesList()[$model->category]
            ],
            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Автор статьи'),
                'value' => $model->author->email
            ],
            [
                'format' => 'raw',
                'label' => Yii::t('app', 'Редактор статьи'),
                'value' => $model->redactor->email
            ],
            [
                'label' => Yii::t('app', 'Дата создания'),
                'value' => gmdate("Y-m-d H:i", $model->created_at)
            ],
            [
                'label' => Yii::t('app', 'Дата последнего редактирования'),
                'value' => gmdate("Y-m-d H:i", $model->updated_at)
            ],
        ]
    ]) ?>
        </div>
        <p>
            <?php Pjax::begin(['enablePushState' => false]); ?>
            <?php if ($model->status == $model::STATUS_PUBLIC): ?>
                <?= Html::a(Yii::t('app', 'Убрать с главной'), ['remove', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
            <?php else: ?>
                <?= Html::a(Yii::t('app', 'Опубликовать'), ['publish', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?php endif ?>
            <?php Pjax::end(); ?>
        </p>
    </section>
</div>
