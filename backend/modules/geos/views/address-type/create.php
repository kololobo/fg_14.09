<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeAddressForm */

$this->title = Yii::t('app', 'Создать тип адреса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы адресов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-address-form-create">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
