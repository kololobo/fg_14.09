<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use phpnt\bootstrapNotify\BootstrapNotify;
/* @var $this yii\web\View */
/* @var $searchModel common\models\forms\TypeAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Типы адресов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-address-form-index">
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="col-md-12">
                    <?= Html::a(Yii::t('app', 'Добавить тип адреса'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12 table-responsive">
                    <?php Pjax::begin(['id' => 'pjaxGridBlock']); ?>
                    <?= BootstrapNotify::widget() ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            'category.name',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
