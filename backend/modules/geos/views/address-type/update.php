<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeAddressForm */

$this->title = Yii::t('app', 'Изменить тип: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы адресов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="type-address-form-update">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
