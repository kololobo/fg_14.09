<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\search\UserSearch;
use phpnt\bootstrapSelect\BootstrapSelectAsset;

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-team-form">
    <div class="box box-success">
        <div class="box-header with-border"></div>
        <?php
        BootstrapSelectAsset::register($this);
        ?>
        <?php
        $form = ActiveForm::begin([
            'id' => 'form',
            'action' => Url::to(['/ourteam/manage/create']),
            'options' => ['data-pjax' => true]]);
        ?>
        <div class="box-body">
            <div class="col-md-12">
                <?= $form->field($model, 'user_id', [
                    'template' => '{label}{input}{error}'])
                    ->dropDownList(UserSearch::getEmails(), [
                        'class'  => 'form-control selectpicker',
                        'data' => [
                            'style' => 'btn-primary',
                            'live-search' => true,
                            'size' => 7,
                            'title' => Yii::t('app', 'Выберите пользователя'),
                        ]])->label(Yii::t('app', 'E-mail пользователя')) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Добавить в команду'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>

        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>

</div>
