<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\User;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use phpnt\cropper\ImageLoadWidget
// use frontend\assets\ImgAreaSelect;

// ImgAreaSelect::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-team-form">
    <div class="box box-success">
        <div class="box-header with-border"></div>
        <?php
        BootstrapSelectAsset::register($this);
        ?>
        <?php
        $form = ActiveForm::begin([
            'id' => 'form',
            'action' => Url::to(['/ourteam/manage/update/' . $model->user_id])
            ])
        ?>
        <div class="box-body">

            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'profession')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'citate')->textarea(['rows' => 6]) ?>

<!--             < ?php
                $items = array();
                    foreach ($model->photos() as $photo) {
                        $items [$photo->id] = Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/userpics/' . $photo->file_small, [
                            'style' => 'width:120px;']);
                    }
            ?>
            < ?= $form->field($model, 'photo_id')->radioList( $items, ['separator'=>"<hr />",'encode'=>false]); ?>
 -->
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            </div>

            <br>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Обновить'), ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>
        <?= ImageLoadWidget::widget([
                        'id' => 'load-user-avatar',                                     // суффикс ID
                        'object_id' => $model->user_id,                                 // ID объекта
                        'imagesObject' => $model->photos(),                             // уже загруженные изображения
                        'images_num' => 1,                                              // максимальное количество изображений
                        'images_label' => 'mainpage',                                   // метка для изображения
                        'imageSmallWidth' => 120,                                       // ширина миниатюры
                        'imageSmallHeight' => 120,                                      // высота миниатюры
                        'baseUrl' => '@frontend/web/images/userpics',                  // алиас к изображениям
                        'sizeModal' => 'modal-md',
                        'imagePath' => '/pic_',                                             // путь, куда будут записыватся изображения относительно алиаса
                        'noImage' => 3,                                                 // 1 - no-logo, 2 - no-avatar, 3 - no-img или путь к другой картинке
                        'frontendUrl' => Yii::$app->urlManagerFrontend->baseUrl . '/images/userpics',
                        'buttonClass'=> 'btn btn-info',                                 // класс кнопки "обновить аватар"/"загрузить аватар" / по умолчанию btm btn-info
                        'previewSize'=> 'file',                                         // размер изображения для превью(либо file_small, либо просто file)
                        'pluginOptions' => [                                            // настройки плагина
                            'aspectRatio' => 1/1,                                       // установите соотношение сторон рамки обрезки. По умолчанию свободное отношение.
                            'strict' => true,                                          // true - рамка не может вызодить за холст, false - может
                            'guides' => true,                                           // показывать пунктирные линии в рамке
                            'center' => true,                                           // показывать центр в рамке изображения изображения
                            'autoCrop' => true,                                         // показывать рамку обрезки при загрузке
                            'autoCropArea' => 0.5,                                      // площидь рамки на холсте изображения при autoCrop (1 = 100% - 0 - 0%)
                            // 'dragCrop' => true,                                         // создание новой рамки при клики в свободное место хоста (false - нельзя)
                            // 'movable' => true,                                          // перемещать изображение холста (false - нельзя)
                            // 'rotatable' => true,                                        // позволяет вращать изображение
                             'scalable' => false,                                         // мастабирование изображения
                             'zoomable' => false,
                        ]]);
                    ?>
        </div>
    </div>
</div>
