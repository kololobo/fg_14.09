<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */

$this->title = Yii::t('app', 'Добавить в команду');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Наша команда'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-team-create">
    <section class="content">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_add', [
            'model' => $model,
        ]) ?>
    </section>
</div>
