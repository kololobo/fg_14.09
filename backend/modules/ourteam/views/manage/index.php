<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use phpnt\bootstrapNotify\BootstrapNotify;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OurTeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Наша команда');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-team-index">
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="col-md-12">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <p>
                        <?= Html::a(Yii::t('app', 'Добавить в команду'), ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>
            <div class="box-body">
                <?php Pjax::begin(); ?>
                <?= BootstrapNotify::widget() ?>
                <div class="col-md-12 table-responsive">
                    <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'user_id',
                                'position',
                                'name',
                                'profession',
                                'citate:ntext',
                                [
                                    'label' => Yii::t('app', 'Фото'),
                                    'format' => 'raw',
                                    'value' => function($data){
                                        if ($data->photo()) {
                                            return Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/images/userpics/' . ($data->photo()->file_small),[
                                            'style' => 'width:100px;'
                                        ]);
                                        } else{
                                            return '<span class="label label-danger">' . Yii::t('app', 'Нет фото') . '</span>';
                                        }
                                    },
                                ],
                                [
                                    'attribute' => 'show',
                                    'label' => Yii::t('app', 'Показывать на главной'),
                                    'format' => 'raw',
                                    'value' => function ($model, $key, $index, $column) {
                                        /* @var $model \common\models\OurTeam */
                                        return $model->getShowMark();
                                    },
                                    'filter' => false,
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                </div>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </section>
</div>
