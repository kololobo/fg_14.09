<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Our Team',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Our Teams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="our-team-update">
    <section class="content" style="padding-bottom: 300px;">
        <div class="box">

            <?= $this->render('_update', [
                'model' => $model,
            ]) ?>

        </div>
    </section>
</div>
