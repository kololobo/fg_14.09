<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Our Teams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-team-view">
    <section class="content" >

        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->user_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Вы действительно желаете удалить пользователя из "нашей команды"?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'user_id',
                'position',
                'name',
                'profession',
                'citate:ntext',
                [
                    'attribute' => Yii::t('app', 'Фото'),
                    'format' => $model->photo() != null ? ['image', ['width' => '120px']] : 'raw',
                    'value'=> $model->photo() != null ? Yii::$app->urlManagerFrontend->baseUrl . '/images/userpics/' . ($model->photo()->file_small) : '<span class="label label-danger">'. Yii::t('app', 'Нет фото!') .'</span>',
                ],
                'email:email',
                [
                    'attribute' => Yii::t('app', 'Показывать на главной'),
                    'format' => 'raw',
                    'value' => $model->getShowMark(),
                ]
            ],
            'formatter' => [
                'class' => '\yii\i18n\Formatter',
                'nullDisplay' => '<span class="label label-danger">'. Yii::t('app', 'Не задано!') .'</span>'//Пустая ячейка
            ]
        ]) ?>

        <p>
            <?php Pjax::begin(); ?>
            <?php if ($model->show): ?>
                <?= Html::a(Yii::t('app', 'Убрать с главной'), ['hide', 'id' => $model->user_id], ['class' => 'btn btn-danger']) ?>
            <?php else: ?>
                <?= Html::a(Yii::t('app', 'Показывать на главной*'), ['show', 'id' => $model->user_id], ['class' => 'btn btn-success']) ?>
                <br><i><?= Yii::t('app', '* Чтобы вывести члена команды на главный экран - необходимо заполнить все поля') ?></i>
            <?php endif ?>
            <?php Pjax::end(); ?>
        </p>

    </section>
</div>
