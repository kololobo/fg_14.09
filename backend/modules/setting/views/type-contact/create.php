<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeContactForm */

$this->title = Yii::t('app', 'Создать тип контакта');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы контактов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-contact-form-create">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
