<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TypeContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Типы контактов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-contact-form-index">
    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="col-md-12">
                    <?= Html::a(Yii::t('app', 'Добавить тип контакта'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12 table-responsive">
                    <?php Pjax::begin(['id' => 'pjaxGridBlock']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            //'format',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
