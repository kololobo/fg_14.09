<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 04.09.2016
 * Time: 17:54
 */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\forms\ProfileUserForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use yii\widgets\MaskedInput;
use common\widgets\Forms\CityField;
use yii\widgets\Pjax;
use phpnt\awesomeBootstrapCheckbox\AwesomeBootstrapCheckboxAsset;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
?>
<div class="identity-form">
    <div class="box box-success" style="margin-bottom: 50px;">
        <div class="box-header with-border"></div>
        <?php
        Pjax::begin([
            'id' => 'pjaxBlock',
            'enablePushState' => false,
        ]);
        AwesomeBootstrapCheckboxAsset::register($this);
        BootstrapSelectAsset::register($this);
        ?>
        <?php $form = ActiveForm::begin([
            'action' => $model->isNewRecord ? Url::to(['/user/manage/create']) : Url::to(['/user/manage/update', 'id' => $model->id]),
            'id' => 'form']) ?>
        <div class="box-body">
            <div class="col-md-12">
                <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'Имя']) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Фамилия']) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Электронная почта'])  ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'city_id', [
                    'template' => '{label}{input}{error}'])
                    ->widget(CityField::className(),['country_id' => $model->country_id, 'typeahead' => false]) ?>
            </div>

            <?php
            if ($model->country_id):
                ?>
                <div class="col-md-12 phone-input-mask">
                    <?= $form->field($model, 'phone', ['template' => '{label}
                            <div class="input-group">
                                <span class="input-group-addon">+'.\common\models\forms\GeoCountryForm::getCallingCode($model->country_id).'</span>
                                    <div class="text-left" slyle="background-color: #cc0000;">{input}</div>
                             </div>
                      <i>{hint}</i>{error}'])->widget(MaskedInput::className(),[
                        'name' => 'phone',
                        'mask' => $model->phoneMask]) ?>
                </div>
                <?php
            else:
                ?>
                <div class="col-md-12">
                    <?= $form->field($model, 'phone')->textInput(['class' => 'form-control disabled', 'disabled' => true, 'placeholder' => Yii::t('app', 'Телефон')]) ?>
                </div>
                <?php
            endif;
            ?>

            <div class="col-md-12">
                <?= $form->field($model, 'item_name')->dropDownList(isset($model->profileUser->company_id) ? $model->rolesOfCompanyList : $model->rolesOfUserList, [
                    'class'         => 'form-control selectpicker',
                    'data' => [
                        'style' => 'btn-primary',
                        'size' => 7,
                        'title' => Yii::t('app', 'Роль')
                    ]]) ?>
            </div>

            <?php
            if (!isset($model->profileUser->company_id) || $model->profileUser->company_id != null):
                ?>
                <div class="col-md-12">
                    <?= $form->field($model, 'tariff_name')->dropDownList($model->tariffesOfUserList, [
                        'class'         => 'form-control selectpicker',
                        'data' => [
                            'style' => 'btn-primary',
                            'size' => 7,
                            'title' => Yii::t('app', 'Тариф')
                        ]]) ?>
                </div>
                <?php
            endif;
            ?>

            <div class="col-md-12">
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Пароль')]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'confirm_password')->passwordInput(['placeholder' => Yii::t('app', 'Подтвердите пароль')]) ?>
            </div>

            <?= $form->field($model, 'country_id')->hiddenInput()->label(false) ?>

            <?= Html::hiddenInput('model', 'common\models\forms\UserForm') ?>
            <?= Html::hiddenInput('scenario', $model->scenario) ?>
            <?= Html::hiddenInput('form', '@backend/modules/user/views/manage/_form') ?>
            <?= Html::hiddenInput('id', $model->id) ?>

            <?= $form->field($model, 'model_scenario', ['template' => '{input}'])->hiddenInput(['value' => $model->scenario])->label(false) ?>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать пользователя') : Yii::t('app', 'Изменить пользователя'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

