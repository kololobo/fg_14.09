<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeUserCategoryForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-company-category-form-form">
    <div class="content-form-form">
        <div class="box box-success" style="margin-bottom: 50px;">
            <div class="box-header with-border"></div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
