<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use phpnt\bootstrapNotify\BootstrapNotify;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TypeUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Типы пользователей');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-12">
                <?= Html::a(Yii::t('app', 'Добавить тип пользователя'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12 table-responsive">
                <?php Pjax::begin(['id' => 'pjaxGridBlock']); ?>
                <?= BootstrapNotify::widget() ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'id',
                        'name',
                        [
                            'attribute' => 'category_id',
                            'label' => Yii::t('app', 'Категория'),
                            'value' => function ($model, $key, $index, $column) {
                                /* @var $model \common\models\forms\TypeUserForm */
                                return $model->category->name;
                            },
                            'filter' => false
                        ],
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</section>

