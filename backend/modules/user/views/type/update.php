<?php
/* @var $this yii\web\View */
/* @var $model common\models\forms\TypeUserForm */

$this->title = Yii::t('app', 'Изменить тип: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы пользователей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="content-form-create">
    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>
</div>
