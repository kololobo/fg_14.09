<?php
/* @var $this \yii\web\View */
/* @var $adminLteAsset \phpnt\adminLTE\AdminLteAsset */
/* @var $user \common\models\Identity */
/* @var $username string */
/* @var $avatar string */
/* @var $items array */

use yii\helpers\Url;
use yii\widgets\Menu;
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $avatar ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= 'username' ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?php
        if (Yii::$app->user->can('admin')) {
            $items = [
                [
                    'label' => '<i class="fa fa-pie-chart"></i> <span>' . Yii::t('app', 'Главная') . '</span>',
                    'url' => Url::to(['/']),
                    'options' => ['class' => (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'active' : '']
                ],
            ];
        }

        if (Yii::$app->user->can('admin')) {
            $items[] =  [
                'label' => '<i class="fa fa-user"></i> <span>'.Yii::t('app', 'Пользователи').'</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>',
                'url' => ['#'],
                'options'=>[
                    'class'=> Yii::$app->controller->module->id == 'user' ? 'treeview active' : 'treeview'
                ],
                //'template' => '<a href="{url}">{label}</a>',
                'items' => [
                    [
                        'label' => Yii::t('app', 'Администраторы'),
                        'url' => Url::to(['/user/admins/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'user' && Yii::$app->controller->id == 'admins') ? 'active' : '']
                    ],
                    [
                        'label' => Yii::t('app', 'Пользователи'),
                        'url' => Url::to(['/user/manage/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'user' && Yii::$app->controller->id == 'manage') ? 'active' : '']
                    ],
                    [
                        'label' => Yii::t('app', 'Типы пользователей'),
                        'url' => Url::to(['/user/type/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'user' && Yii::$app->controller->id == 'type') ? 'active' : '']
                    ],
                    [
                        'label' => Yii::t('app', 'Категории типов'),
                        'url' => Url::to(['/user/type-category/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'user' && Yii::$app->controller->id == 'type-category') ? 'active' : '']
                    ],
                ]
            ];
        }

        if (Yii::$app->user->can('admin')) {
            $items[] =  [
                'label' => '<i class="fa fa-users" aria-hidden="true"></i><span>'.Yii::t('app', 'Компании').'</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>',
                'url' => ['#'],
                'options'=>[
                    'class'=> Yii::$app->controller->module->id == 'company' ? 'treeview active' : 'treeview'
                ],
                //'template' => '<a href="{url}">{label}</a>',
                'items' => [
                    [
                        'label'     => Yii::t('app', 'Компании'),
                        'url'       => Url::to(['/company/manage/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'company' && Yii::$app->controller->id == 'manage') ? 'active' : '']
                    ],
                    [
                        'label'     => Yii::t('app', 'Типы компаний'),
                        'url'       => Url::to(['/company/type/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'company' && Yii::$app->controller->id == 'type') ? 'active' : '']
                    ],
                    [
                        'label' => Yii::t('app', 'Категории типов'),
                        'url' => Url::to(['/company/type-category/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'company' && Yii::$app->controller->id == 'type-category') ? 'active' : '']
                    ],
                ]
            ];
        }

        if (Yii::$app->user->can('admin')) {
            $items[] =  [
                'label' => '<i class="fa fa-newspaper-o"></i> <span>' . Yii::t('app', 'Контент') . '</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i>',
                'url' => ['#'],
                'options'=>[
                    'class'=> Yii::$app->controller->module->id == 'content' ? 'treeview active' : 'treeview'
                ],
                'items' => [
                    [
                        'label'     => Yii::t('app', 'Весь контент'),
                        'url'       => Url::to(['/content/manage/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'content' && Yii::$app->controller->id == 'manage') ? 'active' : '']
                    ],
                    [
                        'label'     => Yii::t('app', 'Журнал'),
                        'url'       => Url::to(['/content/journal/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'content' && Yii::$app->controller->id == 'journal') ? 'active' : '']
                    ],
                ]
            ];
            $items[] = [
                'label' => '<i class="fa fa-language"></i> <span>' . Yii::t('app', 'Управление переводом') . '</span>',
                'url' => Url::to(['/translate/manage/index']),
                'options' => ['class' => Yii::$app->controller->module->id == 'translate' ? 'active' : '']
            ];
        }


        if (Yii::$app->user->can('admin')) {
            $geoItems = [
                [
                    'label' => Yii::t('app', 'Типы адресов'),
                    'url' => Url::to(['/geos/address-type/index']),
                    'options'   => ['class' => (Yii::$app->controller->module->id == 'geos' && Yii::$app->controller->id == 'address-type') ? 'active' : '']
                ]
            ];
            if (Yii::$app->setting->show_all_cities == null) {
                $geoItems[] = [
                    'label' => Yii::t('app', 'Управление городами'),
                    'url' => Url::to(['/geos/city/index']),
                    'options'   => ['class' => (Yii::$app->controller->module->id == 'geos' && Yii::$app->controller->id == 'city') ? 'active' : '']
                ];
            }
            if (Yii::$app->setting->show_all_countries == null) {
                $geoItems[] = [
                    'label' => Yii::t('app', 'Управление странами'),
                    'url' => Url::to(['/geos/country/index']),
                    'options'   => ['class' => (Yii::$app->controller->module->id == 'geos' && Yii::$app->controller->id == 'country') ? 'active' : '']
                ];
            }

            $items[] =  [
                'label' => '<i class="fa fa-map"></i><span>'.Yii::t('app', 'Гео данные').'</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>',
                'url' => ['#'],
                'options'=>[
                    'class'=> Yii::$app->controller->module->id == 'geos' ? 'treeview active' : 'treeview'
                ],
                //'template' => '<a href="{url}">{label}</a>',
                'items' => $geoItems
            ];
            $items[] = [
                'label'     => '<i class="fa fa-users" aria-hidden="true"></i> <span>'.Yii::t('app', 'Наша команда').'</span>',
                'url'       => Url::to(['/ourteam/manage/index']),
                'options'   => ['class' => Yii::$app->controller->module->id == 'ourteam' ? 'active' : '']
            ];
        }

        if (Yii::$app->user->can('admin')) {
            $items[] =  [
                'label' => '<i class="fa fa-gear"></i> <span>'.Yii::t('app', 'Настройка').'</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>',
                'url' => ['#'],
                'options'=>[
                    'class'=> Yii::$app->controller->module->id == 'setting' ? 'treeview active' : 'treeview'
                ],
                //'template' => '<a href="{url}">{label}</a>',
                'items' => [
                    [
                        'label'     => Yii::t('app', 'Общие настройки'),
                        'url'       => Url::to(['/setting/manage/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'setting' && Yii::$app->controller->id == 'manage') ? 'active' : '']
                    ],
                    [
                        'label'     => Yii::t('app', 'Типы контактов'),
                        'url'       => Url::to(['/setting/type-contact/index']),
                        'options'   => ['class' => (Yii::$app->controller->module->id == 'setting' && Yii::$app->controller->id == 'type-contact') ? 'active' : '']
                    ],
                ]
            ];
        }

        ?>
        <?= Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'encodeLabels' => false,
                'submenuTemplate' => "\n<ul class='treeview-menu' role='menu'>\n{items}\n</ul>\n",
                'items' => $items
            ]
        ) ?>
    </section>
</aside>
