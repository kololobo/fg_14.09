<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "journal".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $photo_id
 * @property integer $category
 * @property integer $author_id
 * @property integer $redactor_id
 * @property integer $content_id
 * @property integer $title_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $author
 * @property Content $content0
 * @property Photo $photo
 * @property User $redactor
 * @property Content $title0
 */
class Journal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'author_id', 'redactor_id', 'created_at', 'updated_at'], 'required'],
            [['content'], 'string'],
            [['photo_id', 'category', 'author_id', 'redactor_id', 'content_id', 'title_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['content_id'], 'unique'],
            [['title_id'], 'unique'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['content_id'], 'exist', 'skipOnError' => true, 'targetClass' => Content::className(), 'targetAttribute' => ['content_id' => 'id']],
            [['photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Photo::className(), 'targetAttribute' => ['photo_id' => 'id']],
            [['redactor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['redactor_id' => 'id']],
            [['title_id'], 'exist', 'skipOnError' => true, 'targetClass' => Content::className(), 'targetAttribute' => ['title_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID статьи'),
            'title' => Yii::t('app', 'Название статьи'),
            'content' => Yii::t('app', 'Текст статьи'),
            'photo_id' => Yii::t('app', 'ID картинки, выводимой в списке'),
            'category' => Yii::t('app', 'Категория статьи'),
            'author_id' => Yii::t('app', 'ID автора'),
            'redactor_id' => Yii::t('app', 'ID редактора'),
            'content_id' => Yii::t('app', 'ID контента (для перевода)'),
            'title_id' => Yii::t('app', 'ID названия (для перевода)'),
            'status' => Yii::t('app', 'Статус (публиковать?)'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent0()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Photo::className(), ['id' => 'photo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedactor()
    {
        return $this->hasOne(User::className(), ['id' => 'redactor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle0()
    {
        return $this->hasOne(Content::className(), ['id' => 'title_id']);
    }
}
