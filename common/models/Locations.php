<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locations".
 *
 * @property integer $id
 * @property string $city
 * @property string $country
 *
 * @property UserLocations[] $userLocations
 */
class Locations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city'], 'string', 'max' => 50],
            [['country'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID города'),
            'city' => Yii::t('app', 'Город'),
            'country' => Yii::t('app', 'Страна'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocations()
    {
        return $this->hasMany(UserLocations::className(), ['location_id' => 'id']);
    }
}
