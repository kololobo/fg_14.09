<?php

namespace common\models;

use Yii;
use common\models\Photo;

/**
 * This is the model class for table "our_team".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $profession
 * @property string $citate
 * @property string $photo
 * @property string $email
 *
 * @property User $user
 */
class OurTeam extends \yii\db\ActiveRecord
{

    public $upload_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'our_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'email'], 'required'],
            [['user_id', 'position'], 'integer'],
            [['citate'], 'string'],
            [['name', 'profession'], 'string', 'max' => 255],
            [['photo_id', 'email'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['upload_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app','Отображаемое имя'),
            'profession' => Yii::t('app','Отображаемая должность'),
            'citate' => Yii::t('app','Цитата'),
            'photo_id' => Yii::t('app','Фото'),
            'email' => Yii::t('app','Email'),
            'upload_image' => Yii::t('app', 'Загрузить изображение'),
            'position' => Yii::t('app', 'Очередность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function photo()
    {
        return Photo::findOne($this->photo_id);
    }

    public function addToTeam()
    {
        $user = User::FindOne($this->user_id);
        $this->name = $user->username;
        $this->email = $user->email;
        $this->citate = $user->description;
        if ($this->photo()) {
            $this->photo_id = $this->photo()->id;
        }
        return $this->save();
    }

    public function getTeam()
    {
        return OurTeam::find()->where(['show' => 1])->orderBy('position')->all();
    }

    public function getShowMark()
    {
        $show = $this->show;
        if ($show) {
            return '<span class="label label-success">Показывать</span>';
        }
        return '<span class="label label-danger">Скрыть</span>';
    }

    public function hide()
    {
        $this->show = 0;
        if ($this->save(false)) {
            return true;
        } else {
            die('Ошибка записи в бд');
        }
    }

    public function show()
    {
        if ($this->name &&
            $this->profession &&
            $this->citate &&
            $this->photo_id &&
            $this->email) {
                $this->show = 1;
                if ($this->save(false)) {
                    return true;
                } else {
                    die('Ошибка записи в бд');
                }
        }
    }

    public function photos()
    {
        return Photo::find()->where(['object_id' => $this->user_id])->all();
    }

    public function load_pic()
    {
        $photo = Photo::find()->where(['object_id' => $this->user_id, 'deleted' => 0])->one();
        if ($photo) {
            $this->photo_id = $photo->id;
        } else{
            $this->photo_id = NULL;
        }
        return true;
    }

}
