<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OurTeam;

/**
 * OurTeamSearch represents the model behind the search form about `common\models\OurTeam`.
 */
class OurTeamSearch extends OurTeam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'position'], 'integer'],
            [['name', 'profession', 'citate', 'photo_id', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OurTeam::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'citate', $this->citate])
            ->andFilterWhere(['like', 'photo_id', $this->photo_id])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
