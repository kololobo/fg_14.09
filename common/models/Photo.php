<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property string $file
 * @property string $file_small
 * @property string $type
 * @property integer $object_id
 * @property integer $user_id
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'file_small', 'type'], 'required'],
            [['object_id', 'user_id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['file', 'file_small'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 32],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'file_small' => 'File Small',
            'type' => 'Type',
            'object_id' => 'Object ID',
            'user_id' => 'User ID',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
