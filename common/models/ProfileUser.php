<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $gender
 * @property string $tariff
 * @property integer $type
 * @property string $expert_list
 * @property string $skype
 * @property string $web_site
 * @property integer $what_show
 * @property string $date_of_birth
 * @property integer $contracted_or_not
 * @property integer $travel_ability
 * @property string $member_since
 * @property string $last_login
 * @property integer $company_id
 *
 * @property Compensation[] $compensations
 * @property Genres[] $genres
 * @property ProfileCompany $company
 * @property User $id0
 * @property QuestionaryBrand $questionaryBrand
 * @property QuestionaryBuyer $questionaryBuyer
 * @property QuestionaryDesigner $questionaryDesigner
 * @property QuestionaryExpert $questionaryExpert
 * @property QuestionaryMedia $questionaryMedia
 * @property QuestionaryModel $questionaryModel
 * @property QuestionaryPhotographer $questionaryPhotographer
 * @property QuestionaryPr $questionaryPr
 * @property QuestionaryRepresentative $questionaryRepresentative
 * @property Reasons[] $reasons
 * @property UserLocations[] $userLocations
 * @property UsersLanguages[] $usersLanguages
 */
class ProfileUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'type', 'what_show', 'contracted_or_not', 'travel_ability', 'company_id'], 'integer'],
            [['date_of_birth', 'member_since', 'last_login'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 32],
            [['tariff'], 'string', 'max' => 20],
            [['expert_list', 'skype', 'web_site'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileCompany::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID пользователя'),
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'gender' => Yii::t('app', 'Пол'),
            'tariff' => Yii::t('app', 'Тариф'),
            'type' => Yii::t('app', 'Тип пользователя'),
            'expert_list' => Yii::t('app', 'Перечень специализаций эксперта'),
            'skype' => Yii::t('app', 'Skype'),
            'web_site' => Yii::t('app', 'Веб-сайт'),
            'what_show' => Yii::t('app', 'Что показывать (возраст/дата рождения)'),
            'date_of_birth' => Yii::t('app', 'Дата рождения'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'travel_ability' => Yii::t('app', 'Готовность к переезду'),
            'member_since' => Yii::t('app', 'Дата регистрации'),
            'last_login' => Yii::t('app', 'Последний вход'),
            'company_id' => Yii::t('app', 'Компания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompensations()
    {
        return $this->hasMany(Compensation::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenres()
    {
        return $this->hasMany(Genres::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ProfileCompany::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryBrand()
    {
        return $this->hasOne(QuestionaryBrand::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryBuyer()
    {
        return $this->hasOne(QuestionaryBuyer::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryDesigner()
    {
        return $this->hasOne(QuestionaryDesigner::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryExpert()
    {
        return $this->hasOne(QuestionaryExpert::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryMedia()
    {
        return $this->hasOne(QuestionaryMedia::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryModel()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryPhotographer()
    {
        return $this->hasOne(QuestionaryPhotographer::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryPr()
    {
        return $this->hasOne(QuestionaryPr::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaryRepresentative()
    {
        return $this->hasOne(QuestionaryRepresentative::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReasons()
    {
        return $this->hasMany(Reasons::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocations()
    {
        return $this->hasMany(UserLocations::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersLanguages()
    {
        return $this->hasMany(UsersLanguages::className(), ['id' => 'id']);
    }
}
