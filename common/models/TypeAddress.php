<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_address".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 *
 * @property SpecAddress[] $specAddresses
 * @property TypeAddressCategory $category
 */
class TypeAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeAddressCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID типа компании'),
            'name' => Yii::t('app', 'Тип адреса'),
            'category_id' => Yii::t('app', 'Категория'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecAddresses()
    {
        return $this->hasMany(SpecAddress::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TypeAddressCategory::className(), ['id' => 'category_id']);
    }
}
