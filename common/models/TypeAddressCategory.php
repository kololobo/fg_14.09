<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_address_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property TypeAddress[] $typeAddresses
 */
class TypeAddressCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_address_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID категории'),
            'name' => Yii::t('app', 'Название категории'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeAddresses()
    {
        return $this->hasMany(TypeAddress::className(), ['category_id' => 'id']);
    }
}
