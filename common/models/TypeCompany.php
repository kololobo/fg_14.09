<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_company".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 *
 * @property SpecCompany[] $specCompanies
 * @property TypeCompanyCategory $category
 */
class TypeCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeCompanyCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID типа компании'),
            'name' => Yii::t('app', 'Тип компании'),
            'category_id' => Yii::t('app', 'Категория'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecCompanies()
    {
        return $this->hasMany(SpecCompany::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TypeCompanyCategory::className(), ['id' => 'category_id']);
    }
}
