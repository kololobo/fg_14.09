<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_contact".
 *
 * @property integer $id
 * @property string $name
 * @property string $format
 *
 * @property Contact[] $contacts
 */
class TypeContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['format'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID типа контакта'),
            'name' => Yii::t('app', 'Тип контакта'),
            'format' => Yii::t('app', 'Формат'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['type_contact_id' => 'id']);
    }
}
