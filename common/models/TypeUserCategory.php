<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_user_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property TypeUser[] $typeUsers
 */
class TypeUserCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_user_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID категории'),
            'name' => Yii::t('app', 'Название категории'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeUsers()
    {
        return $this->hasMany(TypeUser::className(), ['category_id' => 'id']);
    }
}
