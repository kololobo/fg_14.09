<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $alias
 * @property integer $payment_status
 * @property string $email
 * @property string $lang
 * @property string $full_phone
 * @property string $description
 * @property integer $status
 * @property string $image_main
 * @property string $password_hash
 * @property string $password_encrypted
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property Content[] $contents
 * @property OurTeam[] $ourTeams
 * @property Photo[] $photos
 * @property ProfileUser $profileUser
 * @property UserOnline $userOnline
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_status', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['status'], 'required'],
            [['alias', 'password_hash', 'password_encrypted', 'password_reset_token', 'email_confirm_token'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 64],
            [['lang'], 'string', 'max' => 2],
            [['full_phone'], 'string', 'max' => 15],
            [['image_main'], 'string', 'max' => 20],
            [['auth_key'], 'string', 'max' => 32],
            [['alias'], 'unique'],
            [['email'], 'unique'],
            [['full_phone'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID пользователя'),
            'alias' => Yii::t('app', 'Уникальная ссылка'),
            'payment_status' => Yii::t('app', 'Статус оплаты'),
            'email' => Yii::t('app', 'Электронная почта'),
            'lang' => Yii::t('app', 'Язык'),
            'full_phone' => Yii::t('app', 'Полный номер'),
            'description' => Yii::t('app', 'Описание'),
            'status' => Yii::t('app', 'Статус'),
            'image_main' => Yii::t('app', 'Метка изображения'),
            'password_hash' => Yii::t('app', 'Пароль'),
            'password_encrypted' => Yii::t('app', 'Зашифрованный пароль'),
            'auth_key' => Yii::t('app', 'Ключ авторизации'),
            'password_reset_token' => Yii::t('app', 'Ключ сброса пароля'),
            'email_confirm_token' => Yii::t('app', 'Ключ подтверждения эл. адреса'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOurTeams()
    {
        return $this->hasMany(OurTeam::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileUser()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOnline()
    {
        return $this->hasOne(UserOnline::className(), ['user_id' => 'id']);
    }
}
