<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_languages".
 *
 * @property integer $id
 * @property string $language
 *
 * @property ProfileUser $id0
 */
class UsersLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['language'], 'string', 'max' => 2],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'language' => Yii::t('app', 'Язык'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
