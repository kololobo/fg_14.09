<?php

namespace common\models\forms;

use common\models\Journal;
use common\models\forms\ContentForm;
use common\models\Content;
// use yii\behaviors\TimestampBehavior;
use Yii;

class JournalForm extends Journal
{

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */

    // Категории новостей
    const FG_NEWS = 1;
    const FASHION_SHOWS = 2;
    const PERSONS = 3;
    const FASHION_LIFE = 4;
    const BLOG = 0;

    // Статусы
    const STATUS_RAW = 0; // Не просмотренная
    const STATUS_SHOW = 1; // Просмотрена, но не отредактирована
    const STATUS_EDIT = 2; // На редактировании
    const STATUS_PUBLIC = 3; // Опубликованная

//    public $update_content_model = TRUE; //разрешает обновить модель контент для создания переводов

    public function rules()
    {
        return [
            ['category', 'in', 'range' => array_keys(self::getCategoriesList())],
            [['title', 'content', 'photo_id', 'status'], 'safe']
        ];
    }

    public static function getCategoriesList()
    {
        return [
            self::FG_NEWS => \Yii::t('app', 'Новости FG'),
            self::FASHION_SHOWS => \Yii::t('app', 'Fashion события'),
            self::PERSONS =>  \Yii::t('app', 'Личности'),
            self::FASHION_LIFE =>  \Yii::t('app', 'Модная жизнь'),
            self::BLOG =>  \Yii::t('app', 'Блог'),
        ];
    }

    public function setShow()
    {
        $this->status = self::STATUS_SHOW;
//        $this->update_content_model = FALSE;
        $this->save();
    }
    public function setEdit()
    {
        $this->status = self::STATUS_EDIT;
//        $this->update_content_model = FALSE;
        $this->save();
    }
    public function setPublish()
    {
        $this->status = self::STATUS_PUBLIC;
//        $this->update_content_model = FALSE;
        $this->save();
    }

    public function getStatusLabels()
    {
        switch ($this->status) {
            case self::STATUS_RAW:
                return '<span class="label label-default">' . Yii::t('app', 'Не просмотрена') . '</span>';
                break;
            case self::STATUS_SHOW:
                return '<span class="label label-primary">' . Yii::t('app', 'Просмотрена') . '</span>';
                break;
            case self::STATUS_EDIT:
                return '<span class="label label-warning">' . Yii::t('app', 'На редактировании') . '</span>';
                break;
            case self::STATUS_PUBLIC:
                return '<span class="label label-success">' . Yii::t('app', 'Опубликовано') . '</span>';
                break;
        }
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_RAW => \Yii::t('app', 'Не просмотрена'),
            self::STATUS_SHOW =>  \Yii::t('app', 'Просмотрена'),
            self::STATUS_EDIT =>  \Yii::t('app', 'На редактировании'),
            self::STATUS_PUBLIC =>  \Yii::t('app', 'Опубликовано'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
//        if ($this->update_content_model) {
//            $content = ContentForm::findOne($this->content_id);
//            $content->description = 'Article (id = ' . $this->id . ')';
//            $content->save();
//
//            $title = ContentForm::findOne($this->title_id);
//            $title->description = 'Title to Article (id = ' . $this->id . ')';
//            $title->save();
//        }
        parent::afterSave($insert, $changedAttributes);
    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeDelete()
    {
        parent::afterDelete();
//        ContentForm::deleteAll(['content_id' => $this->id]);
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->isNewRecord){
                $this->author_id = Yii::$app->user->id;
                $this->created_at = time();
            }
            $this->redactor_id = Yii::$app->user->id;
            $this->updated_at = time();

//            if ($this->update_content_model) {
//                $content = ($content = Content::findOne($this->content_id)) ? $content : new Content();
//                $content->category = 'journal';
//                $content->description = 'No';
//                $content->location = 'frontend/views/journal/index';
//                $content->user_id = $this->author_id;
//                $content->message = $this->content;
//                $content->save();
//                $this->content_id = $content->id;
//
//                $title = ($title = Content::findOne($this->title_id)) ? $title : new Content();
//                $title->category = 'journal';
//                $title->description = 'No';
//                $title->location = 'frontend/views/journal/index';
//                $title->user_id = $this->author_id;
//                $title->message = $this->title;
//                $title->save();
//                $this->title_id = $title->id;
//            }
            return true;
        }
        return false;
    }

}