<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 06.09.2016
 * Time: 15:14
 */

namespace common\models\forms;

use common\models\Identity;
use common\models\ProfileCompanyIdentity;
use common\models\ProfileUser;
use common\models\User;

/**
 *
 */
class ProfileCompanyForm extends ProfileCompanyIdentity
{
    public function multiblock() {
        $this->status = self::STATUS_BLOCKED;
        $this->save();
        foreach ($this->profileUsers as $one) {
            /* @var $one ProfileUser */
            $model = User::findOne($one->id);
            $model->status = Identity::STATUS_BLOCKED;
            $model->save();
            //dd($model);
        }
    }

    public function multiactive() {
        $this->status = self::STATUS_ACTIVE;
        $this->save();
        foreach ($this->profileUsers as $one) {
            /* @var $one ProfileUser */
            $model = User::findOne($one->id);
            $model->status = Identity::STATUS_ACTIVE;
            $model->save();
            //dd($model);
        }
    }
}