<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 20.09.2016
 * Time: 16:22
 */

namespace common\models\forms;

use common\models\TypeCompanyCategory;
use yii\helpers\ArrayHelper;

/**
 * @property array $categoriesList
 */

class TypeCompanyCategoryForm extends TypeCompanyCategory
{
    public static function getCategoriesList()
    {
        $items = self::find()
            ->orderBy('name')
            ->all();
        return ArrayHelper::map($items, 'id', 'name');
    }
}