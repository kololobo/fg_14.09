<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 20.09.2016
 * Time: 16:21
 */

namespace common\models\forms;

use common\models\TypeCompany;
use yii\helpers\ArrayHelper;

/**
 * @property array $companyTypeList
 * @property integer $countType
 * @property integer $oneType
 */
class TypeCompanyForm extends TypeCompany
{
    public static function getCompanyTypeList()
    {
        $model = self::find()->joinWith(['category'])->all();
        $items = ArrayHelper::map($model,'id','name','category.name');
        return $items;
    }

    public static function getCountType()
    {
        return self::find()->count();
    }

    public static function getOneType()
    {
        /* @var $model TypeCompanyForm */
        $model = self::find()->one();
        return $model->id;
    }

    public function getTypeCompany() {
        return $this->hasMany(ProfileCompanyForm::className(), ['id' => 'company_id'])
            ->viaTable('spec_company', ['type_id' => 'id']); 
    }
}