<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 20.09.2016
 * Time: 13:43
 */

namespace common\models\forms;

use common\models\TypeUserCategory;
use yii\helpers\ArrayHelper;

/**
 * @property array $categoriesList
 */

class TypeUserCategoryForm extends TypeUserCategory
{
    public static function getCategoriesList()
    {
        $items = self::find()
            ->orderBy('name')
            ->all();
        return ArrayHelper::map($items, 'id', 'name');
    }
}