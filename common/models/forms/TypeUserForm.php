<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 20.09.2016
 * Time: 13:30
 */

namespace common\models\forms;

use Yii;
use common\models\questionary\forms\ExpertForm;
use common\models\TypeUser;
use yii\helpers\ArrayHelper;

/**
 * @property integer $countType
 * @property integer $oneType
 * @property array $userTypeList
 */
class TypeUserForm extends TypeUser
{

    const MODEL = 1;
    const PHOTOGRAPHER = 2;
    const DESIGNER = 3;
    const AGENCY = 4;
    const BRAND = 5;
    const BUYER = 6;
    const CUSTOMER = 7;
    const REPRESANTATIVE = 8;
    const MEDIA = 9;
    const PR_MANAGER = 10;
    const EXPERT = 11;

    public static function getUserTypeList()
    {
//        $model = self::find()->joinWith(['category'])->all();
//        $items = ArrayHelper::map($model,'id','name','category.name');
        $items = [
            self::MODEL => Yii::t('app', 'Модель'),
            self::PHOTOGRAPHER => Yii::t('app', 'Фотограф'),
            self::DESIGNER => Yii::t('app', 'Дизайнер'),
            self::AGENCY => Yii::t('app', 'Модельное агенство'),
            self::BRAND => Yii::t('app', 'Бренд'),
            self::BUYER => Yii::t('app', 'Байер'),
            self::CUSTOMER => Yii::t('app', 'Заказчик'),
            self::REPRESANTATIVE => Yii::t('app', 'Представитель компании'),
            self::MEDIA => Yii::t('app', 'Медиа'),
            self::PR_MANAGER => Yii::t('app', 'PR-менеджер'),
            self::EXPERT => Yii::t('app', 'Эксперт')
        ];
        return $items;
    }



    public static function getExpertList()
    {
        $items = [
            ExpertForm::DESIGNER => Yii::t('app', 'Дизайнер'),
            ExpertForm::STYLIST => Yii::t('app', 'Стилист'),
            ExpertForm::HAIR => Yii::t('app', 'Парикмахер (Артист)'),
            ExpertForm::MUA => Yii::t('app', 'Визажист'),
            ExpertForm::PHOTOGRAPHER => Yii::t('app', 'Фотограф'),
            ExpertForm::MODEL => Yii::t('app', 'Модель')
        ];
        return $items;
    }


    public static function getCountType()
    {
        return self::find()->count();
    }

    public static function getOneType()
    {
        /* @var $model TypeUserForm */
        $model = self::find()->one();
        return [$model->id];
    }

    public function getTypeUser() {
        return $this->hasMany(UserForm::className(), ['id' => 'user_id'])
            ->viaTable('spec_user', ['type_id' => 'id']);
    }
}