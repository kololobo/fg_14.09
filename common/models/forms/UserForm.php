<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 15.09.2016
 * Time: 12:19
 */

namespace common\models\forms;


use common\models\Address;
use common\models\AuthAssignment;
use common\models\Locations;
use common\models\UserLocations;
use common\models\Identity;
use common\models\ProfileCompany;
use common\models\ProfileUser;
use common\models\UsersLanguages;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 *
 */
class UserForm extends Identity
{
    public $model_scenario;
//    public $lang;

    public $type_company;
    public $profession_num;

    public $item_name;
    public $tariff_name;

    public $first_name;
    public $last_name;
    public $name;

//    public $city;
    public $city_id;
    public $phone;
    public $type_expert;
    public $type_user;
//    public $description;
//    public $country_id;
//    public $calling_code;
//    public $phone_mask;

    public $password;
    public $confirm_password;
    public $password_encrypted;

    public $new_record = false;

    public function init()
    {
        parent::init();
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'phone', 'city_id', 'lang', 'tariff_name'], 'required',
                'on' => 'visitor'],
            [['first_name', 'last_name', 'email', 'phone', 'city_id', 'lang', 'profession_num', 'type_user', 'description'], 'required',
                'on' => 'classic'],
            [['first_name', 'last_name', 'email', 'phone', 'city_id', 'lang', 'profession_num', 'type_user', 'description'], 'required',
                'on' => 'vip'],
            [['first_name', 'last_name', 'email', 'phone', 'city_id', 'lang', 'name', 'type_company'], 'required',
                'on' => 'corporate'],
            [['email', 'phone', 'city_id'], 'required',
                'on' => 'create'],
            [['email', 'phone', 'city_id'], 'required',
                'on' => 'update'],
            [['city_id', 'type_company', 'profession_num'], 'integer'],
            ['description', 'string'],
            [['full_phone', 'name', 'tariff_name', 'item_name', 'model_scenario'], 'string'],
            [['auth_key', 'first_name', 'last_name'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 13],
            ['email', 'email'],
            [['lang'], 'in', 'range' => ['ru', 'en', 'ch', 'fr']],
            ['phone', 'validatePhone'],
            ['type_user', 'validateProfs'],
            ['type_expert', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys(TypeUserForm::getExpertList())]],

//            ['city', 'validateCity'],
            [['password'], 'string', 'min' => 6, 'max' => 300],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message'=> \Yii::t('app', 'Пароли не совпадают.')],
            [['full_phone', 'email'], 'unique'],
            [['first_name', 'last_name', 'name', 'email', 'password'], 'trim'],
        ];
    }

    public function attributeLabels()
    {
        $labels = Identity::attributeLabels();
        $model =  new ProfileUser();
        $labels += $model->attributeLabels();
        $model =  new ProfileCompany();
        $labels += $model->attributeLabels();
//        $model =  new GeoCity();
//        $labels += $model->attributeLabels();
//        $model =  new GeoRegion();
//        $labels += $model->attributeLabels();
//        $model =  new GeoCountry();
//        $labels += $model->attributeLabels();
//        $model =  new Address();
//        $labels += $model->attributeLabels();
        $labels += [
            'type_company'      => Yii::t('app', 'Тип компании'),
            'type_user'         => Yii::t('app', 'Профессия'),
            'item_name'         => Yii::t('app', 'Рoль'),
            'city'              => Yii::t('app', 'Город'),
            'tariff_name'       => Yii::t('app', 'Тариф'),
            'password'          => Yii::t('app', 'Пароль'),
            'confirm_password'  => Yii::t('app', 'Повторите пароль'),
            'lang'              => Yii::t('app', 'Язык')
        ];
        return $labels;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function validatePhone() {
        $phone = $this->clearString($this->phone);
        if ((strlen($phone) > 9) AND (strlen($phone) < 17)){
            $this->full_phone = $phone;
        } else{
            $this->addError('phone', Yii::t('app', 'Не верный номер телефона'));
        }
//        $model = GeoCountry::findOne($this->country_id);
//        $this->calling_code = $model->calling_code;
//        if ($model->phone_number_digits_code != strlen($phone)) {
//            $this->addError('phone', Yii::t('app', 'Не верный номер телефона'));
//        } else {
//            $this->phone = $phone;
//        }
    }

    public function validateProfs()
    {
        if (count($this->type_expert) > ($this->profession_num)){
            $this->addError('type_user', Yii::t('app', 'Выбрано много профессий'));
        }
        if (($this->type_user != TypeUserForm::EXPERT) && ($this->type_expert)){
            $this->addError('type_user', Yii::t('app', 'Только эксперты имеют специализации'));
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->new_record = true;
                $this->password = \Yii::$app->security->generateRandomString(8);
                if ($this->tariff_name == 'visitor') {
                    $this->status   = self::STATUS_ACTIVE;
                } else {
                    $this->status   = self::STATUS_WAIT;
                }
                /* Временно!!! В продакшне убрать */
                $this->setPassword('12345678');
                $this->status   = self::STATUS_ACTIVE;

//                $this->setPassword($this->password);
                $this->generateAuthKey();
                $this->generateEmailConfirmToken();
                if ($this->tariff_name == 'corporate') {
                    $this->item_name = 'adminCompany';
                } else {
                    $this->item_name = 'user';
                }

                return true;

            } else {
//                $this->setFullPhone($this->calling_code.$this->phone);
                if ($this->password != '') {
                    $this->setPassword($this->password);
                }

                return true;
            }
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->new_record) {
            $auth = \Yii::$app->authManager;
            /* Временно!!! В продакшне убрать */
            $this->item_name = 'admin';
            $role = $auth->getRole($this->item_name);
            $auth->assign($role, $this->id);
        } else {
            if ($this->item_name != '') {
                AuthAssignment::deleteAll(['item_name' => $this->roleName, 'user_id' => $this->id]);
                $auth = \Yii::$app->authManager;
                $role = $auth->getRole($this->item_name);
                $auth->assign($role, $this->id);
            }
        }

        if ($this->tariff_name) {
            AuthAssignment::deleteAll(['item_name' => $this->tariffName, 'user_id' => $this->id]);
            $auth = \Yii::$app->authManager;
            $role = $auth->getPermission($this->tariff_name);
            $auth->assign($role, $this->id);
        }

        $modelCompany           = new ProfileCompany();

        if ($this->tariff_name == 'corporate') {
            $modelCompany->type     = $this->type_company ? $this->type_company : 1;
            $modelCompany->tariff   = $this->tariff_name ? $this->tariff_name : null;
            $modelCompany->status   = self::STATUS_WAIT;
            $modelCompany->name     = $this->name;
            $modelCompany->save();
        }

        $modelUser              = $this->new_record ? new ProfileUser() : ProfileUser::findOne($this->id);
        $modelUser->type        = $this->type_user;
        $modelUser->id          = $this->id;
        $modelUser->tariff      = $this->tariff_name ? $this->tariff_name : null;
        $modelUser->first_name  = $this->first_name;
        $modelUser->last_name   = $this->last_name;
        $modelUser->company_id  = $modelCompany->id ? $modelCompany->id : null;
        $modelUser->member_since= date('Y-m-d');
        if (($this->scenario == 'classic' || $this->scenario == 'vip') && ($this->type_user == TypeUserForm::EXPERT)){
            $modelUser->expert_list = implode(',', $this->type_expert);
        }
        $modelUser->save();
        $location = new UserLocations();
        $location->location_id   = $this->city_id;
        $location->user_id   = $this->id;
        $location->save();
        $languages = new UsersLanguages();
        $languages->language  = $this->lang;
        $languages->id   = $this->id;
        $languages->save();
//        $this->new_record ? $modelUser->link('id0', $this) : $modelUser->save();
//        $model              = $this->new_record ? new Address() : Address::findOne(['user_id' => $this->id, 'main' => 1]);
//        $model->type        = self::ADDRESS_REAL;
//        $model->city_id     = $this->city_id;
////        $model->country_id  = $this->country_id;
//        $model->user_id     = $this->id;
//        $model->main        = self::ITEM_MAIN;
//        $model->save();
        if ($this->tariff_name == 'corporate') {
            $model              = $this->new_record ? new Address() : Address::findOne(['user_id' => $modelCompany->id, 'main' => 1]);
            $model->type        = self::ADDRESS_REAL;
            $model->city_id     = $this->city_id;
//            $model->country_id  = $this->country_id;
            $model->company_id  = $modelCompany->id;
            $model->main        = self::ITEM_MAIN;
            $model->save();
        }

//        if ($this->type_user != null) {
//            SpecUserForm::deleteAll(['user_id' => $this->id]);
//            foreach ($this->type_user as $one) {
//                $modelTypeUser = TypeUserForm::findOne($one);
//                $this->link('typeUser', $modelTypeUser);
//            }
//        }

        if ($this->model_scenario == 'corporate') {
            $modelTypeCompany = TypeCompanyForm::findOne($this->type_company);
            $modelTypeCompany->link('typeCompany', $modelCompany);
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function afterFind()
    {
        $this->first_name = $this->profileUser->first_name;
        $this->last_name = $this->profileUser->last_name;
        /*$this->day_birth = $this->profileUser->day_birth;
        $this->month_birth = $this->profileUser->month_birth;
        $this->year_birth = $this->profileUser->year_birth;
        $this->sex = $this->profileUser->sex;*/
//        $this->country_id = $this->mainAddressUser->country_id;
//        $this->city = GeoCityForm::getCityName($this->mainAddressUser);
//        $this->city_id = $this->mainAddressUser->city_id;
//        $this->calling_code = $this->mainAddressUser->country->calling_code;
        $this->item_name = $this->roleName;
        $this->tariff_name = $this->tariffName;
        return true;
    }

    public function sendActivationEmail($model)
    {
        return \Yii::$app->mailer->compose('activationEmail', ['user' => $model])
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::t('app', '{app_name} (отправлено роботом).', ['app_name' => \Yii::$app->name])])
            ->setTo($this->email)
            ->setSubject(\Yii::t('app', 'Активация для {app_name}.', ['app_name' => \Yii::$app->name]))
            ->send();
    }
}