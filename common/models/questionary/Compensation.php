<?php

namespace common\models\questionary;

use common\models\ProfileUser;
use Yii;

/**
 * This is the model class for table "compensation".
 *
 * @property integer $id
 * @property integer $profession
 * @property integer $compensation
 *
 * @property ProfileUser $id0
 */
class Compensation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compensation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'profession', 'compensation'], 'required'],
            [['id', 'profession', 'compensation'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'profession' => Yii::t('app', 'профессия / специализация'),
            'compensation' => Yii::t('app', 'Компенсация'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
