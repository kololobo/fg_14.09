<?php

namespace common\models\questionary;

use common\models\forms\ProfileUserForm;
use common\models\ProfileUser;
use Yii;

/**
 * This is the model class for table "genres".
 *
 * @property integer $id
 * @property integer $profession
 * @property integer $genre
 *
 * @property ProfileUser $id0
 */
class Genres extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'genres';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'profession', 'genre'], 'required'],
            [['id', 'profession', 'genre'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'profession' => Yii::t('app', 'профессия / специализация'),
            'genre' => Yii::t('app', 'Жанр работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
