<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_brand".
 *
 * @property integer $id
 * @property string $company_name
 * @property integer $professional_experience
 * @property integer $trading
 *
 * @property BrandRange[] $brandRanges
 * @property ProfileUser $id0
 */
class QuestionaryBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'professional_experience'], 'required'],
            [['professional_experience', 'trading'], 'integer'],
            [['company_name'], 'string', 'max' => 255],
            [['company_name'], 'unique'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'company_name' => Yii::t('app', 'Название компании'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'trading' => Yii::t('app', 'Торговля'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandRanges()
    {
        return $this->hasMany(BrandRange::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
