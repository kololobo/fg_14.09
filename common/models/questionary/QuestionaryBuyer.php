<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_buyer".
 *
 * @property integer $id
 * @property string $clients_work
 * @property string $brands_work
 * @property integer $professional_experience
 * @property integer $trading
 *
 * @property BuyerRange[] $buyerRanges
 * @property ProfileUser $id0
 */
class QuestionaryBuyer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_buyer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clients_work', 'professional_experience'], 'required'],
            [['professional_experience', 'trading'], 'integer'],
            [['clients_work', 'brands_work'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'clients_work' => Yii::t('app', 'Клиенты, на которых работаете:'),
            'brands_work' => Yii::t('app', 'Бренды, с которыми вы работаете:'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'trading' => Yii::t('app', 'Торговля'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuyerRanges()
    {
        return $this->hasMany(BuyerRange::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
