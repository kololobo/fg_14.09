<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_designer".
 *
 * @property integer $id
 * @property string $workplace
 * @property string $education
 * @property integer $professional_experience
 *
 * @property DesignerType[] $designerTypes
 * @property ProfileUser $id0
 */
class QuestionaryDesigner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_designer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workplace', 'education'], 'string'],
            [['professional_experience'], 'required'],
            [['professional_experience'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'workplace' => Yii::t('app', 'Рабочее пространство'),
            'education' => Yii::t('app', 'Образование / Курсы / Мастер-классы'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignerTypes()
    {
        return $this->hasMany(DesignerType::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
