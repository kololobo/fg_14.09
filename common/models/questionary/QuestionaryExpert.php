<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_expert".
 *
 * @property integer $id
 *
 * @property ExpertDesigner $expertDesigner
 * @property ExpertHair $expertHair
 * @property ExpertModel $expertModel
 * @property ExpertMua $expertMua
 * @property ExpertPhotographer $expertPhotographer
 * @property ExpertStylist $expertStylist
 * @property ProfileUser $id0
 */
class QuestionaryExpert extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_expert';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertDesigner()
    {
        return $this->hasOne(ExpertDesigner::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertHair()
    {
        return $this->hasOne(ExpertHair::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModel()
    {
        return $this->hasOne(ExpertModel::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertMua()
    {
        return $this->hasOne(ExpertMua::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertPhotographer()
    {
        return $this->hasOne(ExpertPhotographer::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertStylist()
    {
        return $this->hasOne(ExpertStylist::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
