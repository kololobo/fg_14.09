<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_media".
 *
 * @property integer $id
 * @property string $name
 * @property string $position
 * @property integer $professional_experience
 *
 * @property ProfileUser $id0
 */
class QuestionaryMedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'professional_experience'], 'required'],
            [['professional_experience'], 'integer'],
            [['name', 'position'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'name' => Yii::t('app', 'Название журнала / блога'),
            'position' => Yii::t('app', 'Должность'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
