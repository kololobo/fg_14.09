<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_model".
 *
 * @property integer $id
 * @property integer $legal_agree
 * @property string $agency_info
 * @property integer $height
 * @property integer $weight
 * @property integer $chest
 * @property integer $waist
 * @property integer $hips
 * @property integer $dress
 * @property integer $cup
 * @property integer $shoe
 * @property string $ethnicity
 * @property integer $skin_color
 * @property integer $eye_color
 * @property integer $natural_hair_color
 * @property integer $hair_coloration
 * @property integer $readness_to_change_color
 * @property integer $hair_lenght
 * @property integer $has_tatoo
 * @property integer $has_piercing
 * @property integer $professional_experience
 * @property integer $rates_for_photoshoot_hour
 * @property integer $rates_for_photoshoot_half
 * @property integer $rates_for_photoshoot_full
 * @property string $rates_for_runway
 *
 * @property ModelsAdditionalSkills[] $modelsAdditionalSkills
 * @property ModelsCategory[] $modelsCategories
 * @property ModelsPiercing[] $modelsPiercings
 * @property ModelsTatoos[] $modelsTatoos
 * @property ProfileUser $id0
 */
class QuestionaryModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['legal_agree', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'shoe', 'ethnicity', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'has_tatoo', 'has_piercing', 'professional_experience'], 'required'],
            [['legal_agree', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'cup', 'shoe', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'has_tatoo', 'has_piercing', 'professional_experience', 'rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full'], 'integer'],
            [['agency_info', 'rates_for_runway'], 'string'],
            [['ethnicity'], 'string', 'max' => 50],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'legal_agree' => Yii::t('app', 'Разрешение работать (до 18 лет)'),
            'agency_info' => Yii::t('app', 'Информация об агенстве'),
            'height' => Yii::t('app', 'Рост'),
            'weight' => Yii::t('app', 'Вес'),
            'chest' => Yii::t('app', 'Объем груди'),
            'waist' => Yii::t('app', 'Объем талии'),
            'hips' => Yii::t('app', 'Объем бедер'),
            'dress' => Yii::t('app', 'Размер одежды, EU'),
            'cup' => Yii::t('app', 'Размер чашечки/груди'),
            'shoe' => Yii::t('app', 'Размер обуви, EU'),
            'ethnicity' => Yii::t('app', 'Национальность'),
            'skin_color' => Yii::t('app', 'Цвет кожи'),
            'eye_color' => Yii::t('app', 'Цвет глаз'),
            'natural_hair_color' => Yii::t('app', 'Натуральный цвет волос'),
            'hair_coloration' => Yii::t('app', 'Цвет окраса волос'),
            'readness_to_change_color' => Yii::t('app', 'Готовность изменить цвет волос'),
            'hair_lenght' => Yii::t('app', 'Длина волос'),
            'has_tatoo' => Yii::t('app', 'Наличие татуировки'),
            'has_piercing' => Yii::t('app', 'Наличие пирсинга'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'rates_for_photoshoot_hour' => Yii::t('app', 'Цена за фотосессию (в час), $'),
            'rates_for_photoshoot_half' => Yii::t('app', 'Цена за фотосессию (за пол дня / 4 часа), $'),
            'rates_for_photoshoot_full' => Yii::t('app', 'Цена за фотосессию (полный день / 8 часов), $'),
            'rates_for_runway' => Yii::t('app', 'Цены за переезд'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelsAdditionalSkills()
    {
        return $this->hasMany(ModelsAdditionalSkills::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelsCategories()
    {
        return $this->hasMany(ModelsCategory::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelsPiercings()
    {
        return $this->hasMany(ModelsPiercing::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelsTatoos()
    {
        return $this->hasMany(ModelsTatoos::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
