<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_photographer".
 *
 * @property integer $id
 * @property integer $professional_experience
 * @property string $equipment
 * @property integer $studio
 * @property integer $makeup_artist
 * @property integer $hair_stylist
 * @property integer $rates_for_photoshoot_hour
 * @property integer $rates_for_photoshoot_half
 * @property integer $rates_for_photoshoot_full
 *
 * @property PhotographerAreas[] $photographerAreas
 * @property ProfileUser $id0
 */
class QuestionaryPhotographer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_photographer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['professional_experience', 'studio', 'makeup_artist', 'hair_stylist'], 'required'],
            [['professional_experience', 'studio', 'makeup_artist', 'hair_stylist', 'rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full'], 'integer'],
            [['equipment'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'equipment' => Yii::t('app', 'Тип фотооборудования'),
            'studio' => Yii::t('app', 'Студия'),
            'makeup_artist' => Yii::t('app', 'Визажист'),
            'hair_stylist' => Yii::t('app', 'Парикмахер)'),
            'rates_for_photoshoot_hour' => Yii::t('app', 'Рейты на фотосъемку (в час), $'),
            'rates_for_photoshoot_half' => Yii::t('app', 'Рейты на фотосъемку (за пол дня / 4 часа), $'),
            'rates_for_photoshoot_full' => Yii::t('app', 'Рейты на фотосъемку (полный день / 8 часов), $'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotographerAreas()
    {
        return $this->hasMany(PhotographerAreas::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
