<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_pr".
 *
 * @property integer $id
 * @property string $workplace
 * @property string $scope_of_activity
 * @property integer $professional_experience
 *
 * @property ProfileUser $id0
 */
class QuestionaryPr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_pr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workplace', 'professional_experience'], 'required'],
            [['professional_experience'], 'integer'],
            [['workplace', 'scope_of_activity'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'workplace' => Yii::t('app', 'Рабочее место'),
            'scope_of_activity' => Yii::t('app', 'Сфера деятельности'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
