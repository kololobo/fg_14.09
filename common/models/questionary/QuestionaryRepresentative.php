<?php

namespace common\models\questionary;

use Yii;

/**
 * This is the model class for table "questionary_representative".
 *
 * @property integer $id
 * @property integer $position
 * @property string $scope_of_activity
 * @property string $workplace
 * @property integer $professional_experience
 *
 * @property ProfileUser $id0
 */
class QuestionaryRepresentative extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionary_representative';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'workplace', 'professional_experience'], 'required'],
            [['position', 'professional_experience'], 'integer'],
            [['scope_of_activity', 'workplace'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'position' => Yii::t('app', 'Должность'),
            'scope_of_activity' => Yii::t('app', 'Сфера деятельности'),
            'workplace' => Yii::t('app', 'Рабочее место'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
