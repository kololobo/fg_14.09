<?php

namespace common\models\questionary;

use common\models\ProfileUser;
use Yii;

/**
 * This is the model class for table "reasons".
 *
 * @property integer $id
 * @property integer $reason
 *
 * @property ProfileUser $id0
 */
class Reasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reason'], 'required'],
            [['id', 'reason'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileUser::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'reason' => Yii::t('app', 'Причина'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProfileUser::className(), ['id' => 'id']);
    }
}
