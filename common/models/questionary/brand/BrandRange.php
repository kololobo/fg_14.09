<?php

namespace common\models\questionary\brand;

use Yii;
use common\models\questionary\QuestionaryBrand;

/**
 * This is the model class for table "brand_range".
 *
 * @property integer $id
 * @property string $range
 *
 * @property QuestionaryBrand $id0
 */
class BrandRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'range'], 'required'],
            [['id'], 'integer'],
            [['range'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryBrand::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'range' => Yii::t('app', 'Ассортимент товара'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryBrand::className(), ['id' => 'id']);
    }
}
