<?php

namespace common\models\questionary\buyer;

use common\models\questionary\QuestionaryBuyer;
use Yii;

/**
 * This is the model class for table "buyer_range".
 *
 * @property integer $id
 * @property string $range
 *
 * @property QuestionaryBuyer $id0
 */
class BuyerRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buyer_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'range'], 'required'],
            [['id'], 'integer'],
            [['range'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryBuyer::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'range' => Yii::t('app', 'Тип продукции'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryBuyer::className(), ['id' => 'id']);
    }
}
