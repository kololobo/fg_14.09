<?php

namespace common\models\questionary\designer;

use common\models\questionary\QuestionaryDesigner;
use Yii;

/**
 * This is the model class for table "designer_type".
 *
 * @property integer $id
 * @property integer $work_type
 *
 * @property QuestionaryDesigner $id0
 */
class DesignerType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'designer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_type'], 'required'],
            [['id', 'work_type'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryDesigner::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'work_type' => Yii::t('app', 'Область работы дизайнера'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryDesigner::className(), ['id' => 'id']);
    }
}
