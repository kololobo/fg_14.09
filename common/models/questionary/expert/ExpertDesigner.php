<?php

namespace common\models\questionary\expert;

use common\models\questionary\QuestionaryExpert;
use Yii;

/**
 * This is the model class for table "expert_designer".
 *
 * @property integer $id
 * @property string $workplace
 * @property string $education
 * @property integer $professional_experience
 *
 * @property QuestionaryExpert $id0
 * @property ExpertDesignerType[] $expertDesignerTypes
 */
class ExpertDesigner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_designer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workplace', 'education'], 'string'],
            [['professional_experience'], 'required'],
            [['professional_experience'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryExpert::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'workplace' => Yii::t('app', 'Рабочее пространство'),
            'education' => Yii::t('app', 'Образование / Курсы / Мастер-классы'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryExpert::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertDesignerTypes()
    {
        return $this->hasMany(ExpertDesignerType::className(), ['id' => 'id']);
    }
}
