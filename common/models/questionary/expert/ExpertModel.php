<?php

namespace common\models\questionary\expert;

use common\models\questionary\QuestionaryExpert;
use Yii;

/**
 * This is the model class for table "expert_model".
 *
 * @property integer $id
 * @property integer $height
 * @property integer $weight
 * @property integer $chest
 * @property integer $waist
 * @property integer $hips
 * @property integer $dress
 * @property integer $cup
 * @property integer $shoe
 * @property string $ethnicity
 * @property integer $skin_color
 * @property integer $eye_color
 * @property integer $natural_hair_color
 * @property integer $hair_coloration
 * @property integer $readness_to_change_color
 * @property integer $hair_lenght
 * @property integer $has_tatoo
 * @property integer $has_piercing
 * @property integer $professional_experience
 *
 * @property QuestionaryExpert $id0
 * @property ExpertModelAdditionalSkills[] $expertModelAdditionalSkills
 * @property ExpertModelCategory[] $expertModelCategories
 * @property ExpertModelPiercing[] $expertModelPiercings
 * @property ExpertModelTatoos[] $expertModelTatoos
 */
class ExpertModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['height', 'weight', 'chest', 'waist', 'hips', 'dress', 'shoe', 'ethnicity', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'has_tatoo', 'has_piercing', 'professional_experience'], 'required'],
            [['height', 'weight', 'chest', 'waist', 'hips', 'dress', 'cup', 'shoe', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'has_tatoo', 'has_piercing', 'professional_experience'], 'integer'],
            [['ethnicity'], 'string', 'max' => 50],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryExpert::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'height' => Yii::t('app', 'Рост'),
            'weight' => Yii::t('app', 'Вес'),
            'chest' => Yii::t('app', 'Объем груди'),
            'waist' => Yii::t('app', 'Объем талии'),
            'hips' => Yii::t('app', 'Объем бедер'),
            'dress' => Yii::t('app', 'Размер одежды, EU'),
            'cup' => Yii::t('app', 'Размер чашечки/груди'),
            'shoe' => Yii::t('app', 'Размер обуви, EU'),
            'ethnicity' => Yii::t('app', 'Национальность'),
            'skin_color' => Yii::t('app', 'Цвет кожи'),
            'eye_color' => Yii::t('app', 'Цвет глаз'),
            'natural_hair_color' => Yii::t('app', 'Натуральный цвет волос'),
            'hair_coloration' => Yii::t('app', 'Цвет окраса волос'),
            'readness_to_change_color' => Yii::t('app', 'Готовность изменить цвет волос'),
            'hair_lenght' => Yii::t('app', 'Длина волос'),
            'has_tatoo' => Yii::t('app', 'Наличие татуировки'),
            'has_piercing' => Yii::t('app', 'Наличие пирсинга'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryExpert::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModelAdditionalSkills()
    {
        return $this->hasMany(ExpertModelAdditionalSkills::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModelCategories()
    {
        return $this->hasMany(ExpertModelCategory::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModelPiercings()
    {
        return $this->hasMany(ExpertModelPiercing::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModelTatoos()
    {
        return $this->hasMany(ExpertModelTatoos::className(), ['id' => 'id']);
    }
}
