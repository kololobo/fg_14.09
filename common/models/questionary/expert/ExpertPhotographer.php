<?php

namespace common\models\questionary\expert;

use common\models\questionary\expert\photographer\ExpertPhotographerAreas;
use common\models\questionary\QuestionaryExpert;
use Yii;

/**
 * This is the model class for table "expert_photographer".
 *
 * @property integer $id
 * @property integer $professional_experience
 * @property string $equipment
 * @property integer $studio
 * @property integer $makeup_artist
 * @property integer $hair_stylist
 *
 * @property QuestionaryExpert $id0
 * @property ExpertPhotographerAreas[] $expertPhotographerAreas
 */
class ExpertPhotographer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_photographer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['professional_experience', 'studio', 'makeup_artist', 'hair_stylist'], 'required'],
            [['professional_experience', 'studio', 'makeup_artist', 'hair_stylist'], 'integer'],
            [['equipment'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryExpert::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'equipment' => Yii::t('app', 'Тип фотооборудования'),
            'studio' => Yii::t('app', 'Студия'),
            'makeup_artist' => Yii::t('app', 'Визажист'),
            'hair_stylist' => Yii::t('app', 'Парикмахер)'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryExpert::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertPhotographerAreas()
    {
        return $this->hasMany(ExpertPhotographerAreas::className(), ['id' => 'id']);
    }
}
