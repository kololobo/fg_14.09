<?php

namespace common\models\questionary\expert;

use common\models\questionary\QuestionaryModel;
use Yii;

/**
 * This is the model class for table "expert_reason".
 *
 * @property integer $id
 * @property integer $reason
 *
 * @property QuestionaryModel $id0
 */
class ExpertReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reason'], 'required'],
            [['id', 'reason'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'reason' => Yii::t('app', 'Причина'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }
}
