<?php

namespace common\models\questionary\expert;

use common\models\questionary\expert\stylist\ExpertStylistType;
use common\models\questionary\QuestionaryExpert;
use Yii;

/**
 * This is the model class for table "expert_stylist".
 *
 * @property integer $id
 * @property integer $contracted_or_not
 * @property string $workplace
 * @property string $education
 * @property integer $professional_experience
 *
 * @property QuestionaryExpert $id0
 * @property ExpertStylistType[] $expertStylistTypes
 */
class ExpertStylist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_stylist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contracted_or_not', 'professional_experience'], 'required'],
            [['contracted_or_not', 'professional_experience'], 'integer'],
            [['workplace', 'education'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryExpert::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'workplace' => Yii::t('app', 'Рабочее пространство'),
            'education' => Yii::t('app', 'Образование / Курсы / Мастер-классы'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryExpert::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertStylistTypes()
    {
        return $this->hasMany(ExpertStylistType::className(), ['id' => 'id']);
    }
}
