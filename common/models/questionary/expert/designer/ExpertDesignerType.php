<?php

namespace common\models\questionary\expert\designer;

use common\models\questionary\expert\ExpertDesigner;
use Yii;

/**
 * This is the model class for table "expert_designer_type".
 *
 * @property integer $id
 * @property integer $work_type
 *
 * @property ExpertDesigner $id0
 */
class ExpertDesignerType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_designer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_type'], 'required'],
            [['id', 'work_type'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertDesigner::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'work_type' => Yii::t('app', 'Область работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertDesigner::className(), ['id' => 'id']);
    }
}
