<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 18.10.2016
 * Time: 11:08
 */

namespace common\models\questionary\expert\forms;




use common\models\questionary\expert\ExpertPhotographer;
use common\models\questionary\expert\photographer\ExpertPhotographerAreas;
use common\models\questionary\expert\photographer\ExpertPhotographerCompensation;
use common\models\questionary\expert\photographer\ExpertPhotographerType;
use common\models\questionary\forms\ExpertForm;
use common\models\questionary\forms\PhotographerForm;
use common\models\questionary\forms\related\CompensationModel;
use common\models\questionary\forms\related\GenresModel;

class ExpertPhotographerForm extends ExpertPhotographer
{

    public function getTitle()
    {
        return \Yii::t('app', 'Специализация: фотограф');
    }

    public $view = '_expert_photo';

    public $work_areas;
    public $work_type;
    public $compensation;

    function init()
    {
        $this->photographerForm = new PhotographerForm();
        parent::init(); // TODO: Change the autogenerated stub
    }

    public $photographerForm;
    public function rules()
    {
        return [
            [['work_type', 'work_areas', 'studio', 'professional_experience'], 'required'],
            ['professional_experience', 'in', 'range' => array_keys($this->photographerForm->professionalExperiences)],
//            ['contracted_or_not', 'in', 'range' => array_keys($this->photographerForm->contractedList)],
            ['work_type', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->photographerForm->types)]],
            ['work_areas', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->photographerForm->areas)]],
            ['compensation', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->photographerForm->compensations)]],
            [['studio', 'makeup_artist', 'hair_stylist'], 'in', 'range' => array_keys($this->photographerForm->provides)],
        ];
    }

    public function attributeLabels()
    {
        return (new PhotographerForm)->attributeLabels();
    }

    private $new;
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$this->new) {
            ExpertPhotographerAreas::deleteAll(['id' => $this->id]);
        }
        foreach ($this->work_areas as $value) {
            $row = new ExpertPhotographerAreas();
            $row->id = $this->id;
            $row->area = $value;
            $row->save();
        }

        //        if (!$this->new) {
//            GenresModel::deleteAll(['id' => $this->id]);
//        }
        foreach ($this->work_type as $value) {
            $row = new GenresModel();
            $row->id = $this->id;
            $row->genre = $value;
            $row->profession = ExpertForm::PHOTOGRAPHER;
            $row->save();
        }


//        if (!$this->new) {
//            CompensationModel::deleteAll(['id' => $this->id]);
//        }
        if  ($this->compensation) {
            foreach ($this->compensation as $value) {
                $row = new CompensationModel();
                $row->id = $this->id;
                $row->compensation = $value;
                $row->profession = ExpertForm::PHOTOGRAPHER;
                $row->save();
            }
        }


    }

}