<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 18.10.2016
 * Time: 11:08
 */

namespace common\models\questionary\expert\forms;

use common\models\questionary\expert\ExpertStylist;
use common\models\questionary\expert\stylist\ExpertStylistCompensation;
use common\models\questionary\expert\stylist\ExpertStylistGenre;
use common\models\questionary\expert\stylist\ExpertStylistType;
use common\models\questionary\forms\ExpertForm;
use common\models\questionary\forms\related\CompensationModel;
use common\models\questionary\forms\related\GenresModel;
use Yii;


class ExpertStylistForm extends ExpertStylist
{

    public function getTitle()
    {
        return \Yii::t('app', 'Специализация: стилист');
    }

    public $view = '_expert_stylist';

    public $work_type;

    const HAIR_STYLIST = 1;
    const MAKEUP_STYLIST = 2;
    const NAIL_STYLIST = 3;
    const CLOTHES_STYLIST = 4;
    const WARDROBE_STYLIST = 5;

    public function getTypes()
    {
        return [
            self::HAIR_STYLIST => Yii::t('app', 'Стилист-парикмахер'),
            self::MAKEUP_STYLIST => Yii::t('app', 'Стилист-визажист'),
            self::NAIL_STYLIST => Yii::t('app', 'Стилист маникюра'),
            self::CLOTHES_STYLIST => Yii::t('app', 'Стилист-модельер'),
            self::WARDROBE_STYLIST => Yii::t('app', 'Персональный шоппер')
        ];
    }


    public $genre;

    const EDITORIALS = 1;
    const RUNWAY = 2;
    const BEAUTY = 3;
    const PORTRAITS_CELEBRITY = 4;
    const GLAMOUR_NUDE = 5;
    const COMMERCIAL_ADVERTISING = 6;
    const STREET_FASHION = 7;
    const CREATIVE_PROJECTS = 8;
    const TYPE_TEST_SHOOT = 9;
    const OTHER = 10;

    public function getGenres() {
        return [
            self::EDITORIALS => Yii::t('app', 'Editorials'),
            self::RUNWAY => Yii::t('app', 'Подиумное фото'),
            self::BEAUTY => Yii::t('app', 'Beauty фото'),
            self::PORTRAITS_CELEBRITY => Yii::t('app', 'Портреты/знаменитости'),
            self::GLAMOUR_NUDE => Yii::t('app', 'Гламур/Ню'),
            self::COMMERCIAL_ADVERTISING => Yii::t('app', 'Рекламное фото'),
            self::STREET_FASHION => Yii::t('app', 'Уличная мода'),
            self::CREATIVE_PROJECTS => Yii::t('app', 'Креативное фото'),
            self::TYPE_TEST_SHOOT => Yii::t('app', 'Test Shoot'),
            self::OTHER => Yii::t('app', 'Другое')
        ];
    }


//    public function getContractedList()
//    {
//        return [
//            0 => Yii::t('app', 'Нет контракта'),
//            1 => Yii::t('app', 'Подписан контракт')
//        ];
//    }


    public function getProfessionalExperiences()
    {
        return [
            3 => Yii::t('app', 'до 5 лет'),
            5 => Yii::t('app', '5-10 лет'),
            9 => Yii::t('app', 'Более 10 лет'),
        ];
    }

    public $compensation;

    const PAYMENT_ONLY = 1;
    const EXCHANGE_EXPERIENCE = 2;
    const TEST_SHOOT = 3;
    const NEGOTIABLE = 4;

    public function getCompensations()
    {
        return [
            self::PAYMENT_ONLY => Yii::t('app', 'Только оплата'),
            self::EXCHANGE_EXPERIENCE => Yii::t('app', 'Обмен опытом'),
            self::TEST_SHOOT => Yii::t('app', 'Тестовая съёмка'),
            self::NEGOTIABLE => Yii::t('app', 'Договорная'),
        ];
    }


    public function rules()
    {
        return [
            [['genre', 'work_type', 'professional_experience'], 'required'],
            ['work_type', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->types)]],
            ['genre', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->genres)]],
//            ['contracted_or_not', 'in', 'range' => array_keys($this->contractedList)],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            [['workplace', 'education'], 'string'],
            ['compensation', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->compensations)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = (new ExpertHairForm)->attributeLabels();
        $labels += [
            'work_type' => Yii::t('app', 'Типы работ')
        ];
        return $labels;
    }

    private $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

//        if (!$this->new) {
//            GenresModel::deleteAll(['id' => $this->id]);
//        }
        foreach ($this->genre as $value) {
            $row = new GenresModel();
            $row->id = $this->id;
            $row->genre = $value;
            $row->profession = ExpertForm::STYLIST;
            $row->save();
        }

        if (!$this->new) {
            ExpertStylistType::deleteAll(['id' => $this->id]);
        }
        foreach ($this->genre as $value) {
            $row = new ExpertStylistType();
            $row->id = $this->id;
            $row->work_type = $value;
            $row->save();
        }

//        if (!$this->new) {
//            CompensationModel::deleteAll(['id' => $this->id]);
//        }
        if  ($this->compensation) {
            foreach ($this->compensation as $value) {
                $row = new CompensationModel();
                $row->id = $this->id;
                $row->compensation = $value;
                $row->profession = ExpertForm::STYLIST;
                $row->save();
            }
        }
    }

}