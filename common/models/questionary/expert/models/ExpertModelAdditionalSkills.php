<?php

namespace common\models\questionary\expert\models;

use common\models\questionary\expert\ExpertModel;
use Yii;

/**
 * This is the model class for table "expert_model_additional_skills".
 *
 * @property integer $id
 * @property integer $additional_skill
 *
 * @property ExpertModel $id0
 */
class ExpertModelAdditionalSkills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model_additional_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'additional_skill'], 'required'],
            [['id', 'additional_skill'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'additional_skill' => Yii::t('app', 'Дополнительный навык'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertModel::className(), ['id' => 'id']);
    }
}
