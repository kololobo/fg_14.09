<?php

namespace common\models\questionary\expert\models;
use common\models\questionary\expert\ExpertModel;

use Yii;

/**
 * This is the model class for table "expert_model_category".
 *
 * @property integer $id
 * @property integer $category
 *
 * @property ExpertModel $id0
 */
class ExpertModelCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category'], 'required'],
            [['id', 'category'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'category' => Yii::t('app', 'Категория'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertModel::className(), ['id' => 'id']);
    }
}
