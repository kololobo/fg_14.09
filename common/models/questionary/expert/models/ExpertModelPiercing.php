<?php

namespace common\models\questionary\expert\models;

use Yii;
use common\models\questionary\expert\ExpertModel;

/**
 * This is the model class for table "expert_model_piercing".
 *
 * @property integer $id
 * @property integer $piercing_place
 *
 * @property ExpertModel $id0
 */
class ExpertModelPiercing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model_piercing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'piercing_place'], 'required'],
            [['id', 'piercing_place'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'piercing_place' => Yii::t('app', 'Место пирсинга'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertModel::className(), ['id' => 'id']);
    }
}
