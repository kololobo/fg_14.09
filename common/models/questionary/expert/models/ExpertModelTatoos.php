<?php

namespace common\models\questionary\expert\models;

use Yii;
use common\models\questionary\expert\ExpertModel;

/**
 * This is the model class for table "expert_model_tatoos".
 *
 * @property integer $id
 * @property integer $tatoo_place
 *
 * @property ExpertModel $id0
 */
class ExpertModelTatoos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model_tatoos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tatoo_place'], 'required'],
            [['id', 'tatoo_place'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'tatoo_place' => Yii::t('app', 'Место татуировки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertModel::className(), ['id' => 'id']);
    }
}
