<?php

namespace common\models\questionary\expert\photographer;

use common\models\questionary\expert\ExpertPhotographer;
use Yii;

/**
 * This is the model class for table "expert_photographer_areas".
 *
 * @property integer $id
 * @property integer $area
 *
 * @property ExpertPhotographer $id0
 */
class ExpertPhotographerAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_photographer_areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'area'], 'required'],
            [['id', 'area'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertPhotographer::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'area' => Yii::t('app', 'Жанры работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertPhotographer::className(), ['id' => 'id']);
    }
}
