<?php

namespace common\models\questionary\expert\stylist;

use Yii;
use common\models\questionary\expert\ExpertStylist;

/**
 * This is the model class for table "expert_stylist_type".
 *
 * @property integer $id
 * @property integer $work_type
 *
 * @property ExpertStylist $id0
 */
class ExpertStylistType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_stylist_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_type'], 'required'],
            [['id', 'work_type'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpertStylist::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'work_type' => Yii::t('app', 'Область работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ExpertStylist::className(), ['id' => 'id']);
    }
}
