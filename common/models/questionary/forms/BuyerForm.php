<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 20:59
 */

namespace common\models\questionary\forms;


use common\models\questionary\buyer\BuyerReason;
use common\models\questionary\buyer\BuyerRange;
use common\models\questionary\forms\related\ProfessionalExperience;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\QuestionaryBuyer;
use Yii;

class BuyerForm extends QuestionaryBuyer
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета Байер');
    }

    public $view = '_buyer';


    public function getProfessionalExperiences()
    {
        return ProfessionalExperience::getExperiences();
    }

    public $product_range;

    public function getProductRanges()
    {
        return [
            'clothes' => Yii::t('app', ''),
            'underwear' => Yii::t('app', ''),
            'swimwear' => Yii::t('app', ''),
            'shoes' => Yii::t('app', ''),
            'accessories' => Yii::t('app', ''),
            'handbags' => Yii::t('app', ''),
            'jewelry' => Yii::t('app', ''),
            'bijouterie' => Yii::t('app', ''),
            'other' => Yii::t('app', '')
        ];
    }

    const DOSMESTIC_MARKET = 1;
    const INTERNATIONAL_MARKET = 2;
    const DOS_INT_MARKET = 3;

    public function getTradingList()
    {
        return [
            self::DOSMESTIC_MARKET => Yii::t('app', ''),
            self::INTERNATIONAL_MARKET => Yii::t('app', ''),
            self::DOS_INT_MARKET => Yii::t('app', '')
            ];
    }

    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsBuyer();
    }

    public function rules()
    {
        return [
            [['clients_work', 'product_range', 'professional_experience'], 'required'],
            [['clients_work', 'brands_work'], 'string'],
            ['product_range', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->productRanges)]],
            ['trading', 'in', 'range' => array_keys($this->TradingList)],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            ['main_reason','each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryBuyer::attributeLabels();
        // $labels += [
        // ];
        return $labels;
    }

    public $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if (!$this->new) {
            BuyerRange::deleteAll(['id' => $this->id]);
        }
        foreach ($this->product_range as $value){
            $row = new BuyerRange();
            $row->id = $this->id;
            $row->range = $value;
            $row->save();
        }

        if (!$this->new) {
            ReasonsModel::deleteAll(['id' => $this->id]);
        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}