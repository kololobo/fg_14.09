<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 20:59
 */

namespace common\models\questionary\forms;


use common\models\forms\TypeUserForm;
use common\models\questionary\Compensation;
use common\models\questionary\designer\DesignerCompensation;
use common\models\questionary\designer\DesignerReason;
use common\models\questionary\designer\DesignerType;
use common\models\questionary\forms\related\CompensationModel;
use common\models\questionary\forms\related\ProfessionalExperience;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\QuestionaryDesigner;
use Yii;
class DesignerForm extends QuestionaryDesigner
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета дизайнера');
    }

    public $view = '_designer';

    public $work_type;

    const FASHION_DESIGN = 1;
    const PRODUCT_DESIGN = 2;
    const COSTUME_DESIGN = 3;
    const INTERIOR_STYLING = 4;

    public function getTypes()
    {
        return [
            self::FASHION_DESIGN => Yii::t('app', 'Фэшн дизайн'),
            self::PRODUCT_DESIGN => Yii::t('app', 'Дизайн изделий/продукции'),
            self::COSTUME_DESIGN => Yii::t('app', 'Художник по костюмам'),
            self::INTERIOR_STYLING => Yii::t('app', 'Дизайн интерьера')
        ];
    }

//    public function getContractedList()
//    {
//        return [
//            0 => Yii::t('app', 'Нет контракта'),
//            1 => Yii::t('app', 'Подписан контракт')
//        ];
//    }

    public function getProfessionalExperiences()
    {
        return ProfessionalExperience::getExperiences();
    }

    public $compensation;

    public function getCompensations()
    {
        return CompensationModel::getCompensations();
    }


    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsDesigner();
    }

    public function rules()
    {
        return [
            [['work_type', 'professional_experience', 'main_reason'], 'required'],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
//            ['contracted_or_not', 'in', 'range' => array_keys($this->contractedList)],
//            ['travel_ability', 'in', 'range' => array_keys($this->travelList)],
            ['work_type', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->types)]],
            ['compensation', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->compensations)]],
            [['workplace', 'education'], 'string'],
            ['main_reason','each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryDesigner::attributeLabels();
        $labels += [
            'work_type' => Yii::t('app', 'Тип работ'),
            'compensation'      => Yii::t('app', 'Оплата'),
            'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте')
        ];
        return $labels;
    }

    public $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if (!$this->new) {
            DesignerType::deleteAll(['id' => $this->id]);
        }
        foreach ($this->work_type as $value){
            $row = new DesignerType();
            $row->id = $this->id;
            $row->work_type = $value;
            $row->save();
        }

//        if (!$this->new) {
//            Compensation::deleteAll(['id' => $this->id]);
//        }
        if  ($this->compensation) {
            foreach ($this->compensation as $value) {
                $row = new Compensation();
                $row->id = $this->id;
                $row->compensation = $value;
                $row->profession = TypeUserForm::DESIGNER;
                $row->save();
            }
        }

//        if (!$this->new) {
//            ReasonsModel::deleteAll(['id' => $this->id]);
//        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}