<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 20:59
 */

namespace common\models\questionary\forms;


use common\models\questionary\forms\related\ProfessionalExperience;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\QuestionaryMedia;
use Yii;

class MediaForm extends QuestionaryMedia
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета медиа');
    }

    public $view = '_media';

    public $position_input;

    const CHIEF_EDITOR = 'chief_editor';
    const EDITOR = 'editor';
    const BLOGGER = 'blogger';

    public function getPositions()
    {
        return [
            self::CHIEF_EDITOR => Yii::t('app', 'Менеджер'),
            self::EDITOR => Yii::t('app', 'Директор'),
            self::BLOGGER => Yii::t('app', 'Директор'),
            'other' => Yii::t('app', 'Другая:'),
        ];
    }


    public function getProfessionalExperiences()
    {
        return ProfessionalExperience::getExperiences();
    }

    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsMedia();
    }

    public function rules()
    {
        return [
            [['name', 'professional_experience'], 'required'],
            [['name', 'position', 'position_input'], 'string'],
            ['name', 'unique'],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            ['main_reason', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryMedia::attributeLabels();
         $labels += [
             'position_input' => Yii::t('app', 'Введите должность:'),
             'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте')
         ];
        return $labels;
    }

    public $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

//        if (!$this->new) {
//            ReasonsModel::deleteAll(['id' => $this->id]);
//        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}