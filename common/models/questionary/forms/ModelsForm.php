<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 14.10.2016
 * Time: 14:35
 */
namespace common\models\questionary\forms;

use common\models\forms\TypeUserForm;
use common\models\questionary\forms\related\CompensationModel;
use common\models\questionary\forms\related\ProfessionalExperience;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\General;
use common\models\questionary\models\ModelsAdditionalSkills;
use common\models\questionary\models\ModelsCategory;
use common\models\questionary\models\ModelsCompensation;
use common\models\questionary\models\ModelsPiercing;
use common\models\questionary\models\ModelsReason;
use common\models\questionary\models\ModelsTatoos;
use common\models\questionary\QuestionaryModel;
use Yii;

class ModelsForm extends QuestionaryModel
{

    public function getTitle()
    {
        return \Yii::t('app', 'Анкета модели');
    }

    public $view = '_models';
    public $age;
    /* разрешение работать (до 18 лет) */
    const AGREE_HAS = 1;
    const AGREE_HAS_NOT = 2;
    const AGREE_NOT_NEED = 0;

    public function getLegalList()
    {
        return [
            self::AGREE_HAS => Yii::t('app', 'Есть'),
            self::AGREE_HAS_NOT => Yii::t('app', 'Нет')
        ];
    }

    public $metrical;

    public function getMetricSys()
    {
        return [
            1 => Yii::t('app', 'см'),
            0 => Yii::t('app', 'дюймы')
        ];
    }

    public $ethnicity_input;

    public function getEthnicityList ()
    {
        return [
            'African'       => Yii::t('app', 'Африканская'),
            'Asian'         => Yii::t('app', 'Азиатская'),
            'European'      => Yii::t('app', 'Европейская'),
            'Hispanic'      => Yii::t('app', 'Испанская'),
            'Indian'        => Yii::t('app', 'Индийская'),
            'Latin America' => Yii::t('app', 'Латиноамериканская'),
            'Mediterranean' => Yii::t('app', 'Средиземноморская'),
            'Near East'     => Yii::t('app', 'Ближневосточная'),
            'Scandinavian'  => Yii::t('app', 'Скандинавская'),
            'Mixed'         => Yii::t('app', 'Смешанная'),
            'Other:'        => Yii::t('app', 'Прочие:')
        ];

    }
    public $gender = General::FEMALE;
    const CUP_A = 1;
    const CUP_B = 2;
    const CUP_C = 3;
    const CUP_D = 4;
    const CUP_DD = 5;
    const CUP_DDD = 6;
    const CUP_DDDD = 7;

    public $cups = [
        self::CUP_A => 'A',
        self::CUP_B => 'B',
        self::CUP_C => 'C',
        self::CUP_D => 'D',
        self::CUP_DD => 'DD',
        self::CUP_DDD => 'DDD',
        self::CUP_DDDD => 'DDDD'
    ];


    const SKIN_PALE_WHITE = 1;
    const SKIN_WHITE = 2;
    const SKIN_OLIVE = 3;
    const SKIN_LIGHT_BROWN = 4;
    const SKIN_BROWN = 5;
    const SKIN_DARK_BROWN = 6;
    const SKIN_BLACK = 7;

    public function getSkins ()
    {
        return [
            self::SKIN_PALE_WHITE => Yii::t('app', 'Бледно-белый'),
            self::SKIN_WHITE => Yii::t('app', 'Белый'),
            self::SKIN_OLIVE=> Yii::t('app', 'Оливковый'),
            self::SKIN_LIGHT_BROWN => Yii::t('app', 'Светло-коричневый'),
            self::SKIN_BROWN => Yii::t('app', 'Коричневый'),
            self::SKIN_DARK_BROWN => Yii::t('app', 'Темно-коричневый'),
            self::SKIN_BLACK => Yii::t('app', 'Черный')
        ];
    }


    const EYE_BLACK = 1;
    const EYE_BLUE  = 2;
    const EYE_GREEN = 3;
    const EYE_GREY  = 4;
    const EYE_HAZEL = 5;
    const EYE_OTHER = 6;

    public function getEyes ()
    {
        return [
            self::EYE_BLACK => Yii::t('app', 'Черный'),
            self::EYE_BLUE  => Yii::t('app', 'Голубой'),
            self::EYE_GREEN => Yii::t('app', 'Зеленый'),
            self::EYE_GREY => Yii::t('app', 'Серый'),
            self::EYE_HAZEL => Yii::t('app', 'Карий'),
            self::EYE_OTHER => Yii::t('app', 'Другой'),
        ];
    }


    const HAIR_BLONDE = 1;
    const HAIR_STR_BLONDE = 2;
    const HAIR_AUBURN = 3;
    const HAIR_GINGER = 4;
    const HAIR_LIGHT_BROWN = 5;
    const HAIR_BROWN = 6;
    const HAIR_DARK_BROWN = 7;
    const HAIR_SALT_PEPPER = 8;
    const HAIR_BLACK = 9;
    const HAIR_OTHER = 10;

    public function getHairs ()
    {
        return [
            self::HAIR_BLONDE       => Yii::t('app', 'Блонд'),
            self::HAIR_STR_BLONDE   => Yii::t('app', 'Рыжеватый блонд'),
            self::HAIR_AUBURN       => Yii::t('app', 'Рыжватый'),
            self::HAIR_GINGER       => Yii::t('app', 'Рыжий'),
            self::HAIR_LIGHT_BROWN  => Yii::t('app', 'Светло-каштановый'),
            self::HAIR_BROWN        => Yii::t('app', 'Коричнивые'),
            self::HAIR_DARK_BROWN   => Yii::t('app', 'Темно-коричнивые'),
            self::HAIR_SALT_PEPPER  => Yii::t('app', 'Соль и перец'),
            self::HAIR_BLACK        => Yii::t('app', 'Черный'),
            self::HAIR_OTHER        => Yii::t('app', 'Другой')
        ];
    }

    const HAIR_NOT_COLORED = 0;
    const HAIR_COLORED = 1;

    public function getHairColorations()
    {
        return [
            self::HAIR_NOT_COLORED  => Yii::t('app', 'Не окрашены'),
            self::HAIR_COLORED  => Yii::t('app', 'Окрашены'),
            self::HAIR_BLONDE       => Yii::t('app', 'Блонд'),
            self::HAIR_STR_BLONDE   => Yii::t('app', 'Рыжеватый блонд'),
            self::HAIR_AUBURN       => Yii::t('app', 'Рыжеватый'),
            self::HAIR_GINGER       => Yii::t('app', 'Рыжий'),
            self::HAIR_LIGHT_BROWN  => Yii::t('app', 'Светло-каштановый'),
            self::HAIR_BROWN        => Yii::t('app', 'Каштановый'),
            self::HAIR_DARK_BROWN   => Yii::t('app', 'Темно-каштановый'),
            self::HAIR_SALT_PEPPER  => Yii::t('app', 'Седой'),
            self::HAIR_BLACK        => Yii::t('app', 'Черный'),
            self::HAIR_OTHER        => Yii::t('app', 'Другое')
        ];
    }


    const CHANGE_HAIR_READY = 1;
    const CHANGE_HAIR_NOT_READY = 0;

    public function getReadnessList()
    {
        return [
            self::CHANGE_HAIR_READY => Yii::t('app', 'Готов(а) изменить цвет волос'),
            self::CHANGE_HAIR_NOT_READY => Yii::t('app', 'Не готов(а) изменить цвет волос'),
        ];
    }


    const HAIR_BALD = 1;
    const HAIR_SHORT = 2;
    const HAIR_MEDIUM = 3;
    const HAIR_SHOULDER = 4;
    const HAIR_LONG = 5;

    public function getHairLenghts()
    {
        return [
            self::HAIR_BALD => Yii::t('app', 'Бритые'),
            self::HAIR_SHORT => Yii::t('app', 'Короткие'),
            self::HAIR_MEDIUM => Yii::t('app', 'Средняя длина'),
            self::HAIR_SHOULDER => Yii::t('app', 'До плеч'),
            self::HAIR_LONG => Yii::t('app', 'Длинные'),
        ];
    }


    const NECK = 1;
    const SHOULDER = 2;
    const BICEPS = 3;
    const WRIST = 4;
    const CHEST = 5;
    const BELLY = 6;
    const BACK = 7;
    const LOIN = 8;
    const HIP = 9;
    const SHIN = 10;
    const ANKLE = 11;
    const FOOT = 12;
    const OTHER_TATOO = 13;

    public $tatoos;

    public function getHasTatoos()
    {
        return [
            0 => Yii::t('app', 'Нет'),
            1 => Yii::t('app', 'Есть'),
        ];
    }

    public function getTatoo()
    {
        return [
            self::NECK      => Yii::t('app', 'Шея'),
            self::SHOULDER  => Yii::t('app', 'Плечо'),
            self::BICEPS    => Yii::t('app', 'Бицепс'),
            self::WRIST     => Yii::t('app', 'Запястье'),
            self::CHEST     => Yii::t('app', 'Грудь'),
            self::BELLY     => Yii::t('app', 'Живот'),
            self::BACK      => Yii::t('app', 'Спина'),
            self::LOIN      => Yii::t('app', 'Поясница'),
            self::HIP       => Yii::t('app', 'Бедро'),
            self::ANKLE     => Yii::t('app', 'Лодыжка'),
            self::FOOT      => Yii::t('app', 'Стопа'),
            self::OTHER_TATOO => Yii::t('app', 'Прочее')
        ];
    }


    public $has_piercing;
    public $piercing;

    public function getHasPiercing()
    {
        return [
            0 => Yii::t('app', 'Нет'),
            1 => Yii::t('app', 'Есть'),
        ];
    }

    const EAR_TUNNELS = 1;
    const EYEBROWS = 2;
    const TONQUE = 3;
    const NOSE = 4;
    const NIPPLE = 5;
    const NAVEL = 6;
    const BODY_IMPLANT = 7;
    const OTHER_PIERCING = 8;

    public function getPiercings()
    {
        return [
            self::EAR_TUNNELS   => Yii::t('app', 'Туннели в ушах'),
            self::EYEBROWS      => Yii::t('app', 'Бровь'),
            self::TONQUE        => Yii::t('app', 'Язык'),
            self::NOSE          => Yii::t('app', 'Нос'),
            self::NIPPLE        => Yii::t('app', 'Сосок'),
            self::NAVEL         => Yii::t('app', 'Пупок'),
            self::BODY_IMPLANT  => Yii::t('app', 'Подкожные импланты'),
            self::OTHER_PIERCING => Yii::t('app', 'Другое')
        ];
    }


    const ALTERNATIVE_MODEL = 1;
    const BODY_PAINT = 2;
    const BODY_PARTS = 3;
    const BODY_EARS = 4;
    const BODY_EYES = 5;
    const BODY_FINGERS = 6;
    const BODY_FOOT = 7;
    const BODY_HAIR = 8;
    const BODY_HANDS = 9;
    const BODY_LIPS = 10;
    const BODY_NECK = 11;
    const PRINT_PHOTO_MODEL = 12;
    const PRINT_EDITORIAL = 13;
    const PRINT_ADS = 14;
    const HIGH_FASHION = 15;
    const RUNWAY_MODEL = 16;
    const GLAMOUR_MODEL = 17;
    const LINGERIE_MODEL = 18;
    const SWIMWEAR_MODEL = 19;
    const FITNESS_MODEL = 20;
    const BEAUTY_MODEL = 21;
    const NUDE_MODEL = 22;
    const PROMOTIONAL_MODEL = 23;
    const PLUS_SIZE_MODEL = 24;
    const STREET_FASHION = 25;
    const SPORT_MODEL = 26;
    const SENIOR_MODEL = 27;
    const VIDEO_MODEL = 28;

    public $model_category;

    public function getCategories()
    {
        return [
            self::ALTERNATIVE_MODEL => Yii::t('app', 'Альтернативная модель'),
            self::BODY_PAINT => Yii::t('app', 'Боди-арт'),
            self::BODY_PARTS => Yii::t('app', 'Части тела'),
            self::BODY_EARS => Yii::t('app', 'Уши'),
            self::BODY_EYES => Yii::t('app', 'Глаза'),
            self::BODY_FINGERS => Yii::t('app', 'Пальцы'),
            self::BODY_FOOT => Yii::t('app', 'Ноги'),
            self::BODY_HAIR => Yii::t('app', 'Волосы'),
            self::BODY_HANDS => Yii::t('app', 'Руки'),
            self::BODY_LIPS => Yii::t('app', 'Губы'),
            self::BODY_NECK => Yii::t('app', 'Шея'),
            self::PRINT_PHOTO_MODEL => Yii::t('app', 'Печатные издания'),
            self::PRINT_EDITORIAL => Yii::t('app', 'Редакционная печать'),
            self::PRINT_ADS => Yii::t('app', 'Печатная реклама'),
            self::HIGH_FASHION => Yii::t('app', 'High fashion модель'),
            self::RUNWAY_MODEL => Yii::t('app', 'Подиумная модель'),
            self::GLAMOUR_MODEL => Yii::t('app', 'Гламур'),
            self::LINGERIE_MODEL => Yii::t('app', 'Белье'),
            self::SWIMWEAR_MODEL => Yii::t('app', 'Купальники'),
            self::FITNESS_MODEL => Yii::t('app', 'Фитнесс-модель'),
            self::BEAUTY_MODEL => Yii::t('app', 'Бьюти модель(волосы/макияж)'),
            self::NUDE_MODEL => Yii::t('app', 'Ню'),
            self::PROMOTIONAL_MODEL => Yii::t('app', 'Промо-модель'),
            self::PLUS_SIZE_MODEL => Yii::t('app', 'Модель размер+'),
            self::STREET_FASHION => Yii::t('app', 'Уличная мода'),
            self::SPORT_MODEL => Yii::t('app', 'Спортивная модель'),
            self::SENIOR_MODEL => Yii::t('app', 'Зрелая модель (40+)'),
            self::VIDEO_MODEL => Yii::t('app', 'Видео'),
        ];
    }


//    public $professional_experience;

    public function getProfessionalExperiences()
    {
        return ProfessionalExperience::getExperiencesSmall();
    }


    public $additional_skills;

    const ACTOR = 1;
    const FILM = 2;
    const TV = 3;
    const DANCE = 4;
    const MUSIC = 5;
    const EXTREME_SPORT = 6;
    const STAGE = 7;
    const COMMERCICAL = 8;
    const SINGER = 9;
    const PERFOMANCE = 10;
    const VOICE_OVER = 11;

    public function getAdditionalSkills()
    {
        return [
            self::ACTOR => Yii::t('app', 'Актер/актрисса'),
            self::FILM => Yii::t('app', 'Съемки в фильмах'),
            self::TV => Yii::t('app', 'Съемки на телевиденьи'),
            self::DANCE => Yii::t('app', 'Танец'),
            self::MUSIC => Yii::t('app', 'Музыка'),
            self::EXTREME_SPORT => Yii::t('app', 'Экстремальные виды спрота'),
            self::STAGE => Yii::t('app', 'Сценическая игра'),
            self::COMMERCICAL => Yii::t('app', 'Съемки в рекламе'),
            self::SINGER => Yii::t('app', 'Вокал'),
            self::PERFOMANCE => Yii::t('app', 'Перфоманс'),
            self::VOICE_OVER => Yii::t('app', 'Озвучивание')
        ];
    }


    public $compensation;

    public function getCompensations()
    {
        return CompensationModel::getCompensationsModel();
    }

    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsModel();
    }
      /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'safe'],
            [['age', 'rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full'], 'integer'],
            [['model_category', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'shoe', 'ethnicity', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'has_tatoo', 'has_piercing', 'professional_experience'], 'required'],
            ['legal_agree', 'in', 'range' => [self::AGREE_NOT_NEED, self::AGREE_HAS_NOT, self::AGREE_HAS]],
            ['height', 'number', 'min' => 61, 'max' => 227],
            ['weight', 'number', 'min' => 30, 'max' => 170],
            ['chest', 'number', 'min' => 53, 'max' => 230],
            ['waist', 'number', 'min' => 48, 'max' => 134],
            ['hips', 'number', 'min' => 48, 'max' => 134],
            ['dress', 'number', 'min' => 32, 'max' => 60],
            ['cup', 'validateCup'],
            ['shoe', 'number', 'min' => 34, 'max' => 47],
            ['ethnicity', 'string'],
            [['ethnicity', 'ethnicity_input'], 'validateEthnicity'],
            ['skin_color', 'in', 'range' => array_keys($this->skins)],
            ['eye_color', 'in', 'range' => array_keys($this->eyes)],
            ['natural_hair_color', 'in', 'range' => array_keys($this->hairs)],
            ['hair_coloration', 'in', 'range' => array_keys($this->hairColorations)],
            ['readness_to_change_color', 'in', 'range' => array_keys($this->readnessList)],
            ['hair_lenght', 'in', 'range' => array_keys($this->hairLenghts)],
            ['tatoos', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->tatoo)]],
            ['piercing', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->piercings)]],
            ['model_category', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->categories)]],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            ['additional_skills', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->additionalSkills)]],
            ['compensation', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->compensations)]],
            ['main_reason','each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryModel::attributeLabels();
        $labels += [
            'metrical'          => Yii::t('app', 'Система измерения (см/дюйм)'),
            'ethnicity_input'   => Yii::t('app', 'Введите национальность:'),
            'has_tatoos'        => Yii::t('app', 'Есть татуировки'),
            'has_piercing'      => Yii::t('app', 'Есть пирсинг'),
            'tatoos'            => Yii::t('app', 'Татуировки'),
            'piercing'          => Yii::t('app', 'Пирсинг'),
            'compensation'      => Yii::t('app', 'Оплата'),
            'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте'),
            'additional_skills' => Yii::t('app', 'Дополнительные навыки'),
            'model_category'    => Yii::t('app', 'Категоия модели ')
        ];
        return $labels;
    }
    /* Функции вадидации */

    public function beforeValidate()
    {
        if ($this->age >= 18) {
            $this->setAttribute('legal_agree', self::AGREE_NOT_NEED);
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }


    public function validateEthnicity()
    {
        if ($this->ethnicity == 'Other:'){
            if ($this->ethnicity_input){
                $this->ethnicity = $this->ethnicity_input;
            } else{
                $this->addError('ethnicity', Yii::t('app', 'Этническая пренадлежность'));
            }
        }
    }

    public function validateCup()
    {
        if ($this->gender == General::MALE){
            $this->cup = NULL;
        } elseif ($this->gender == General::FEMALE){
            foreach ($this->cups as $key => $value){
                if ($this->cup == $key){
                    return true;
                }
            }
            $this->addError('cup', Yii::t('app', 'Не верно указано значение'));
        }
    }

    private $new = false;
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
//            if (!$this->metrical){
            //... тут будет перевод данных из западной системы в метрическую
//            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if ($this->has_piercing){
            if (!$this->new){
                ModelsPiercing::deleteAll(['id' => $this->id]);
            }
            foreach ($this->piercing as $value){
                $row = new ModelsPiercing();
                $row->id = $this->id;
                $row->piercing_place = $value;
                $row->save();
            }
        }

        if ($this->has_tatoo){
            if (!$this->new) {
                ModelsTatoos::deleteAll(['id' => $this->id]);
            }
            foreach ($this->tatoos as $value){
                $row = new ModelsTatoos();
                $row->id = $this->id;
                $row->tatoo_place = $value;
                $row->save();
            }
        }

        if (!$this->new) {
            ModelsCategory::deleteAll(['id' => $this->id]);
        }
        foreach ($this->model_category as $value){
            $row = new ModelsCategory();
            $row->id = $this->id;
            $row->models_category = $value;
            $row->save();
        }

        if (!$this->new) {
            ModelsAdditionalSkills::deleteAll(['id' => $this->id]);
        }
        if ($this->additional_skills){
            foreach ($this->additional_skills as $value){
                $row = new ModelsAdditionalSkills();
                $row->id = $this->id;
                $row->additional_skill = $value;
                $row->save();
            }
        }

        if (!$this->new) {
            CompensationModel::deleteAll(['id' => $this->id]);
        }
        if ($this->compensation){
            foreach ($this->compensation as $value){
                $row = new CompensationModel();
                $row->id = $this->id;
                $row->compensation = $value;
                $row->profession = TypeUserForm::MODEL;
                $row->save();
            }
        }

        if (!$this->new) {
            ReasonsModel::deleteAll(['id' => $this->id]);
        }
        if ($this->main_reason){
            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }
    }


}