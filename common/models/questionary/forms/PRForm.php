<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 20:59
 */

namespace common\models\questionary\forms;


use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\media\MediaReason;
use common\models\questionary\QuestionaryMedia;
use common\models\questionary\QuestionaryPr;
use Yii;

class PRForm extends QuestionaryPr
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета PR-менеджера');
    }

    public $view = '_pr';

    const CHIEF_EDITOR = 'chief_editor';
    const EDITOR = 'editor';
    const BLOGGER = 'blogger';

    public function getPositions()
    {
        return [
            self::CHIEF_EDITOR => Yii::t('app', 'Менеджер'),
            self::EDITOR => Yii::t('app', 'Директор'),
            self::BLOGGER => Yii::t('app', 'Директор'),
            'other' => Yii::t('app', 'Другая:'),
        ];
    }


    public function getProfessionalExperiences()
    {
        return [
            3 => Yii::t('app', 'до 5 лет'),
            5 => Yii::t('app', '5-10 лет'),
            9 => Yii::t('app', 'Более 10 лет'),
        ];
    }

    public function getTravelList()
    {
        return [
            0 => Yii::t('app', 'Не возможен'),
            1 => Yii::t('app', 'Возможен')
        ];
    }

    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsPr();
    }

    public function rules()
    {
        return [
            [['workplace', 'professional_experience'], 'required'],
            [['workplace', 'scope_of_activity',], 'string'],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryPr::attributeLabels();
         $labels += [
             'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте')

         ];
        return $labels;
    }

    public $new;

 /*   public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


*/
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

//        if (!$this->new) {
//            ReasonsModel::deleteAll(['id' => $this->id]);
//        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}