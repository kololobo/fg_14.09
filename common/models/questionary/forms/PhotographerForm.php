<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 12:59
 */

namespace common\models\questionary\forms;

use common\models\forms\TypeUserForm;
use common\models\questionary\forms\related\CompensationModel;
use common\models\questionary\forms\related\GenresModel;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\photographers\PhotographerAreas;
use common\models\questionary\photographers\PhotographerCompensation;
use common\models\questionary\photographers\PhotographerReason;
use common\models\questionary\photographers\PhotographerType;
use Yii;
use common\models\questionary\QuestionaryPhotographer;

class PhotographerForm extends QuestionaryPhotographer
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета фотографа');
    }

    public $view = '_photographer';

    public function getProfessionalExperiences()
    {
        return [
            3 => Yii::t('app', 'до 5 лет'),
            5 => Yii::t('app', '5-10 лет'),
            9 => Yii::t('app', 'Более 10 лет'),
        ];
    }


    public $work_areas;

    const PHOTO_ADVERTISING = 1;
    const PHOTO_BEAUTY_HAIR = 2;
    const PHOTO_BLACK_WHITE = 3;
    const PHOTO_CHILDREN = 4;
    const PHOTO_CELEBRITY = 5;
    const PHOTO_FASHION = 6;
    const PHOTO_FITNESS = 7;
    const PHOTO_NUDE = 8;
    const PHOTO_PEOPLE = 9;
    const PHOTO_PORTRAIT = 10;
    const PHOTO_SPORT = 11;
    const PHOTO_REPORTAGE = 12;
    const PHOTO_RUNWAY = 13;
    const PHOTO_SWIMWEAR = 14;
    const PHOTO_TRAVEL = 15;
    const PHOTO_UNDERWATER = 16;
    const PHOTO_RETOUCHING = 17;
    const FILM_PRODUCTION = 18;
    const COMMERCIAL_PRODUCTION = 19;
    const TV_PRODUCTION = 20;
    const FILM_POST_PRODUCTION = 21;
    const CGI_3D = 22;
    const MOTION_GRAPHICS = 23;
    const SOUND_DESIGN = 24;
    const RENTAL_STYDIOS = 25;

    public function getAreas()
    {
        return [
            self::PHOTO_ADVERTISING => Yii::t('app', 'Фотограф рекламы'),
            self::PHOTO_BEAUTY_HAIR => Yii::t('app', 'Бьюти фотограф'),
            self::PHOTO_BLACK_WHITE => Yii::t('app', 'Фотограф чёрно-белых фотографий'),
            self::PHOTO_CHILDREN => Yii::t('app', 'Детский фотограф'),
            self::PHOTO_CELEBRITY => Yii::t('app', 'Фотограф селебрити/развлекательных ивэнтов'),
            self::PHOTO_FASHION => Yii::t('app', 'Мода / Fashion/Editorial фотограф'),
            self::PHOTO_FITNESS => Yii::t('app', 'Фотограф направления фитнес'),
            self::PHOTO_NUDE => Yii::t('app', 'Фотограф жанра ню'),
            self::PHOTO_PEOPLE => Yii::t('app', 'Стрит-фотограф'),
            self::PHOTO_PORTRAIT => Yii::t('app', '(Портретный фотограф'),
            self::PHOTO_SPORT => Yii::t('app', 'Фотограф спортивной тематики'),
            self::PHOTO_REPORTAGE => Yii::t('app', 'Фотограф репортажей'),
            self::PHOTO_RUNWAY => Yii::t('app', 'Подиумный фотограф'),
            self::PHOTO_SWIMWEAR => Yii::t('app', 'Фотограф купальников/нижнего белья'),
            self::PHOTO_TRAVEL => Yii::t('app', 'Фотограф-путешественник'),
            self::PHOTO_UNDERWATER => Yii::t('app', 'Фотограф подводной съёмки'),
            self::PHOTO_RETOUCHING => Yii::t('app', 'Фото постпродакшн (ретушь) '),
            self::FILM_PRODUCTION => Yii::t('app', 'Видео/фильм продакшн'),
            self::COMMERCIAL_PRODUCTION => Yii::t('app', 'Рекламные ролики'),
            self::TV_PRODUCTION => Yii::t('app', 'ТВ/видео ролики'),
            self::FILM_POST_PRODUCTION => Yii::t('app', 'Видео постпродакшн & CGI'),
            self::CGI_3D => Yii::t('app', 'CGI & 3D'),
            self::MOTION_GRAPHICS => Yii::t('app', 'Анимационный дизайн'),
            self::SOUND_DESIGN => Yii::t('app', 'Звуковое оформление видео ряда'),
            self::RENTAL_STYDIOS => Yii::t('app', 'Фотограф, предоставляющий студию в аренду')
        ];
    }


    public $work_type;
    public function getTypes()
    {
        return GenresModel::getGenres();
    }

    public function getProvides() {
        return [
            0 => Yii::t('app', 'Не предоставляется'),
            1 => Yii::t('app', 'Предоставляется'),
        ];
    }

    public $compensation;

    public function getCompensations()
    {
        return CompensationModel::getCompensationsModel();
    }

    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsPhotographer();
    }


    public function rules()
    {
        return [
            [['professional_experience', 'work_areas', 'work_type', 'studio', 'makeup_artist', 'hair_stylist' ], 'required'],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            ['work_type', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->types)]],
            ['work_areas', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->areas)]],
            ['equipment', 'string'],
            [['studio', 'makeup_artist', 'hair_stylist'], 'in', 'range' => array_keys($this->provides)],
            ['compensation', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->compensations)]],
            ['main_reason','each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
            [['rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryPhotographer::attributeLabels();
        $labels += [
            'areas' => Yii::t('app', 'Специализация'),
            'work_type' => Yii::t('app', 'Тип работ'),
            'work_areas' => Yii::t('app', 'Тип фотографа'),
            'compensation'      => Yii::t('app', 'Оплата'),
            'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте')
        ];
        return $labels;
    }

    public $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if (!$this->new) {
            PhotographerAreas::deleteAll(['id' => $this->id]);
        }
        foreach ($this->work_areas as $value){
            $row = new PhotographerAreas();
            $row->id = $this->id;
            $row->work_area = $value;
            $row->save();
        }

//        if (!$this->new) {
//            GenresModel::deleteAll(['photographer_id' => $this->id]);
//        }
        foreach ($this->work_type as $value){
            $row = new GenresModel();
            $row->id = $this->id;
            $row->genre = $value;
            $row->profession = TypeUserForm::PHOTOGRAPHER;
            $row->save();
        }

//        if (!$this->new) {
//            CompensationModel::deleteAll(['id' => $this->id]);
//        }
        if  ($this->compensation) {

            foreach ($this->compensation as $value) {
                $row = new CompensationModel();
                $row->id = $this->id;
                $row->compensation = $value;
                $row->profession = TypeUserForm::PHOTOGRAPHER;
                $row->save();
            }
        }

//        if (!$this->new) {
//            PhotographerReason::deleteAll(['photographer_id' => $this->id]);
//        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}