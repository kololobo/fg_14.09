<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 17.10.2016
 * Time: 20:59
 */

namespace common\models\questionary\forms;


use common\models\questionary\forms\related\ProfessionalExperience;
use common\models\questionary\forms\related\ReasonsModel;
use common\models\questionary\QuestionaryRepresentative;
use Yii;

class RepresentativeForm extends QuestionaryRepresentative
{
    public function getTitle()
    {
        return \Yii::t('app', 'Анкета представителя компании');
    }

    public $view = '_representative';

    const MANAGER = 1;
    const DIRECTOR = 2;

    public function getPositions()
    {
        return [
            self::MANAGER => Yii::t('app', 'Менеджер'),
            self::DIRECTOR => Yii::t('app', 'Директор'),
        ];
    }


    public function getProfessionalExperiences()
    {
        return ProfessionalExperience::getExperiences();
    }



    public $main_reason;
    public function getReasons()
    {
        return ReasonsModel::getReasonsRepresentative();
    }

    public function rules()
    {
        return [
            [['position', 'workplace', 'professional_experience'], 'required'],
            [['workplace', 'scope_of_activity'], 'string'],
            ['professional_experience', 'in', 'range' => array_keys($this->professionalExperiences)],
            ['main_reason','each', 'rule' => ['in', 'allowArray' => true, 'range' => array_keys($this->reasons)]],
        ];
    }

    public function attributeLabels()
    {
        $labels = QuestionaryRepresentative::attributeLabels();
         $labels += [
             'main_reason'       => Yii::t('app', 'Главные причины нахождения на сайте')

         ];
        return $labels;
    }

    public $new;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord){
                $this->new = true;
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

//        if (!$this->new) {
//            ReasonsModel::deleteAll(['id' => $this->id]);
//        }
        if ($this->main_reason){

            foreach ($this->main_reason as $value){
                $row = new ReasonsModel();
                $row->id = $this->id;
                $row->reason = $value;
                $row->save();
            }
        }

    }

}