<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 22.10.2016
 * Time: 11:20
 */

namespace common\models\questionary\forms\related;

use Yii;
use common\models\questionary\Compensation;

class CompensationModel extends Compensation
{

    const PAYMENT_ONLY = 1;
    const EXCHANGE_EXPERIENCE = 2;
    const TEST_SHOOT = 3;
    const NEGOTIABLE = 4;
    const TIME_FOR_PRINT = 5;

    static public function getCompensations()
    {
        return [
            self::PAYMENT_ONLY => Yii::t('app', 'Только оплата'),
            self::EXCHANGE_EXPERIENCE => Yii::t('app', 'Обмен опытом'),
            self::TEST_SHOOT => Yii::t('app', 'Test shoot'),
            self::NEGOTIABLE => Yii::t('app', 'Договорная'),
        ];
    }
    
    static public function getCompensationsModel()
    {
        return [
            self::PAYMENT_ONLY => Yii::t('app', 'Только оплата'),
            self::TIME_FOR_PRINT => Yii::t('app', 'Время за отпечатки'),
            self::TEST_SHOOT => Yii::t('app', 'Test shoot'),
            self::NEGOTIABLE => Yii::t('app', 'Договорная'),
        ];
    }

}