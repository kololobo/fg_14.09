<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 22.10.2016
 * Time: 11:19
 */

namespace common\models\questionary\forms\related;

use Yii;
use common\models\questionary\Genres;

class GenresModel extends Genres
{

    const EDITORIALS = 1;
    const RUNWAY = 2;
    const BEAUTY = 3;
    const PORTRAITS_CELEBRITY = 4;
    const GLAMOUR_NUDE = 5;
    const COMMERCIAL_ADVERTISING = 6;
    const STREET_FASHION = 7;
    const CREATIVE_PROJECTS = 8;
    const TYPE_TEST_SHOOT = 9;
    const OTHER = 10;

    static function getGenres() {
        return [
            self::EDITORIALS => Yii::t('app', 'Editorials'),
            self::RUNWAY => Yii::t('app', 'Подиумное фото'),
            self::BEAUTY => Yii::t('app', 'Beauty фото'),
            self::PORTRAITS_CELEBRITY => Yii::t('app', 'Портреты/знаменитости'),
            self::GLAMOUR_NUDE => Yii::t('app', 'Гламур/Ню'),
            self::COMMERCIAL_ADVERTISING => Yii::t('app', 'Рекламное фото'),
            self::STREET_FASHION => Yii::t('app', 'Уличная мода'),
            self::CREATIVE_PROJECTS => Yii::t('app', 'Креативное фото'),
            self::TYPE_TEST_SHOOT => Yii::t('app', 'Test Shoot'),
            self::OTHER => Yii::t('app', 'Другое')
        ];
    }
}