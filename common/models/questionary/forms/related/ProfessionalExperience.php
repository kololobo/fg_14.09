<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 22.10.2016
 * Time: 12:07
 */

namespace common\models\questionary\forms\related;

use Yii;
use yii\base\Model;

class ProfessionalExperience extends Model
{
    
    static public function getExperiences()
    {
        return [
            3 => Yii::t('app', 'до 5 лет'),
            5 => Yii::t('app', '5-10 лет'),
            9 => Yii::t('app', 'Более 10 лет'),
        ];
    }
    
    static public function getExperiencesSmall()
    {
        return [
            1 => Yii::t('app', 'До 1 года'),
            3 => Yii::t('app', '1-5 лет'),
            5 => Yii::t('app', 'Более 5 лет'),
        ];
    }
}