<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 22.10.2016
 * Time: 11:18
 */

namespace common\models\questionary\forms\related;

use Yii;
use common\models\questionary\Reasons;

class ReasonsModel extends Reasons
{
    const SEARCH_WORK_OFFER = 1;
    const SIGNUP_MASTER_CLASS = 2;
    const CARRY_MASTER_CLASS = 3;
    const SEARCH_PHOTOGRAPHER = 4;
    const TO_PARTICIPATE = 5;
    const FASHION_WEEK = 6;
    const MONITOR_NEWS = 7;
    const ESTABLISH_NEW_CONTRACT = 8;
    const SELL_SERVISES = 9;
    const SEARCH_CLIENTS = 10;
    const SUBCONTRACTS = 11;
    const SEARCH_MODELS = 12;
    const EXPERIENCE_EXCHANGE = 13;
    const SIGN_NEW_CONTRACTS = 14;
    const PROMOTE_SERVICES = 15;
    const COOPERATION_OTHER = 16;
    const ACCESS_EVENTS = 17;

    static public function getReasonsBrand()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', ''),
            self::CARRY_MASTER_CLASS => Yii::t('app', ''),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', ''),
            self::TO_PARTICIPATE => Yii::t('app', ''),
            self::FASHION_WEEK => Yii::t('app', ''),
            self::MONITOR_NEWS => Yii::t('app', ''),
            self::SELL_SERVISES => Yii::t('app', ''),
            self::SEARCH_CLIENTS => Yii::t('app', ''),
            self::SUBCONTRACTS => Yii::t('app', ''),
            self::SEARCH_MODELS => Yii::t('app', ''),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', ''),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', ''),
            self::PROMOTE_SERVICES => Yii::t('app', ''),
            self::COOPERATION_OTHER => Yii::t('app', ''),
            self::ACCESS_EVENTS => Yii::t('app', ''),
            self::SEARCH_WORK_OFFER => Yii::t('app', '')
        ];
    }

    static public function getReasonsBuyer()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', ''),
            self::CARRY_MASTER_CLASS => Yii::t('app', ''),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', ''),
            self::TO_PARTICIPATE => Yii::t('app', ''),
            self::FASHION_WEEK => Yii::t('app', ''),
            self::MONITOR_NEWS => Yii::t('app', ''),
            self::SELL_SERVISES => Yii::t('app', ''),
            self::SEARCH_CLIENTS => Yii::t('app', ''),
            self::SUBCONTRACTS => Yii::t('app', ''),
            self::SEARCH_MODELS => Yii::t('app', ''),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', ''),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', ''),
            self::PROMOTE_SERVICES => Yii::t('app', ''),
            self::COOPERATION_OTHER => Yii::t('app', ''),
            self::ACCESS_EVENTS => Yii::t('app', ''),
            self::SEARCH_WORK_OFFER => Yii::t('app', '')
        ];
    }

    static public function getReasonsDesigner()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', 'Запись на мастер классы'),
            self::CARRY_MASTER_CLASS => Yii::t('app', 'Проведение мастер классов'),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', 'Поиск фотографов'),
            self::TO_PARTICIPATE => Yii::t('app', 'Участие в конкурсах и кастингах'),
            self::FASHION_WEEK => Yii::t('app', 'Fashion Weeks, Show Rooms'),
            self::MONITOR_NEWS => Yii::t('app', 'Обзор новостей мира моды'),
            self::SELL_SERVISES => Yii::t('app', 'Продажа услуг'),
            self::SEARCH_CLIENTS => Yii::t('app', 'Поиск новых клиентов, партнёров'),
            self::SUBCONTRACTS => Yii::t('app', 'Субподряд'),
            self::SEARCH_MODELS => Yii::t('app', 'Поиск моделей'),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', 'Обмен опытом'),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', 'Подписание новых контрактов'),
            self::PROMOTE_SERVICES => Yii::t('app', 'Продвижение интересов/торговой марки/услуг'),
            self::COOPERATION_OTHER => Yii::t('app', 'Cooperation with other fashion professionals'),
            self::ACCESS_EVENTS => Yii::t('app', 'Доступ к эксклюзивным мировым VIP ивэнтам закрытого типа'),
            self::SEARCH_WORK_OFFER => Yii::t('app', 'Поиск работы')
        ];
    }

    static public function getReasonsExpert()
    {
        return [
            self::SEARCH_WORK_OFFER => Yii::t('app', 'Поиск работы'),
            self::SIGNUP_MASTER_CLASS => Yii::t('app', 'Запись на мастер классы'),
            self::CARRY_MASTER_CLASS => Yii::t('app', 'Проведение мастер классов'),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', 'Поиск фотографов'),
            self::TO_PARTICIPATE => Yii::t('app', 'Участие в конкурсах и кастингах'),
            self::FASHION_WEEK => Yii::t('app', 'Fashion Weeks, Show Rooms'),
            self::MONITOR_NEWS => Yii::t('app', 'Обзор новостей мира моды'),
            self::SELL_SERVISES => Yii::t('app', 'Продажа услуг'),
            self::SEARCH_CLIENTS => Yii::t('app', 'Поиск новых клиентов, партнёров'),
            self::SUBCONTRACTS => Yii::t('app', 'Субподряд'),
            self::SEARCH_MODELS => Yii::t('app', 'Поиск моделей'),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', 'Обмен опытом'),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', 'Подписание новых контрактов'),
            self::PROMOTE_SERVICES => Yii::t('app', 'Продвижение интересов/торговой марки/услуг'),
            self::COOPERATION_OTHER => Yii::t('app', 'Cooperation with other fashion professionals'),
            self::ACCESS_EVENTS => Yii::t('app', 'Доступ к эксклюзивным мировым VIP ивэнтам закрытого типа'),
        ];
    }

    static public function getReasonsMedia()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', ''),
            self::CARRY_MASTER_CLASS => Yii::t('app', ''),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', ''),
            self::TO_PARTICIPATE => Yii::t('app', ''),
            self::FASHION_WEEK => Yii::t('app', ''),
            self::MONITOR_NEWS => Yii::t('app', ''),
            self::SELL_SERVISES => Yii::t('app', ''),
            self::SEARCH_CLIENTS => Yii::t('app', ''),
            self::SUBCONTRACTS => Yii::t('app', ''),
            self::SEARCH_MODELS => Yii::t('app', ''),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', ''),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', ''),
            self::PROMOTE_SERVICES => Yii::t('app', ''),
            self::COOPERATION_OTHER => Yii::t('app', ''),
            self::ACCESS_EVENTS => Yii::t('app', ''),
            self::SEARCH_WORK_OFFER => Yii::t('app', '')
        ];
    }

    static public function getReasonsModel()
    {
        return [
            self::SEARCH_WORK_OFFER => Yii::t('app', 'Поиск работы'),
            self::SIGNUP_MASTER_CLASS => Yii::t('app', 'Участие в мастер-классах'),
            self::CARRY_MASTER_CLASS => Yii::t('app', 'Проведение мастер-классов'),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', 'Поиск фотографов'),
            self::TO_PARTICIPATE => Yii::t('app', 'Участие в конкурсах и кастингах'),
            self::FASHION_WEEK => Yii::t('app', 'Fashion Weeks, Show Rooms'),
            self::MONITOR_NEWS => Yii::t('app', 'Обзор новостей мира моды'),
            self::ESTABLISH_NEW_CONTRACT => Yii::t('app', 'Установление новых контактов')
        ];
    }

    static public function getReasonsPhotographer()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', 'Участвовать в мастер-классах'),
            self::CARRY_MASTER_CLASS => Yii::t('app', 'Организовывать мастер-классы'),
            self::TO_PARTICIPATE => Yii::t('app', 'Участвовать в кастингах и соревнованиях'),
            self::ESTABLISH_NEW_CONTRACT => Yii::t('app', 'Получить новый контракт'),
            self::SELL_SERVISES => Yii::t('app', 'Продажа услуг'),
            self::SEARCH_CLIENTS => Yii::t('app', 'Поиск новых клиентов, партнёров'),
            self::SUBCONTRACTS => Yii::t('app', 'Субподряд'),
            self::SEARCH_MODELS => Yii::t('app', 'Поиск моделей'),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', 'Обмен опытом'),
        ];
    }

    static public function getReasonsPr()
    {
        return [
            self::SIGNUP_MASTER_CLASS => Yii::t('app', ''),
            self::CARRY_MASTER_CLASS => Yii::t('app', ''),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', ''),
            self::TO_PARTICIPATE => Yii::t('app', ''),
            self::FASHION_WEEK => Yii::t('app', ''),
            self::MONITOR_NEWS => Yii::t('app', ''),
            self::SELL_SERVISES => Yii::t('app', ''),
            self::SEARCH_CLIENTS => Yii::t('app', ''),
            self::SUBCONTRACTS => Yii::t('app', ''),
            self::SEARCH_MODELS => Yii::t('app', ''),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', ''),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', ''),
            self::PROMOTE_SERVICES => Yii::t('app', ''),
            self::COOPERATION_OTHER => Yii::t('app', ''),
            self::ACCESS_EVENTS => Yii::t('app', ''),
            self::SEARCH_WORK_OFFER => Yii::t('app', '')
        ];
    }

    static public function getReasonsRepresentative()
    {
        return [
            self::SEARCH_WORK_OFFER => Yii::t('app', 'Поиск работы'),
            self::SIGNUP_MASTER_CLASS => Yii::t('app', 'Запись на мастер классы'),
            self::CARRY_MASTER_CLASS => Yii::t('app', 'Проведение мастер классов'),
            self::SEARCH_PHOTOGRAPHER => Yii::t('app', 'Поиск фотографов'),
            self::TO_PARTICIPATE => Yii::t('app', 'Участие в конкурсах и кастингах'),
            self::FASHION_WEEK => Yii::t('app', 'Fashion Weeks, Show Rooms'),
            self::MONITOR_NEWS => Yii::t('app', 'Обзор новостей мира моды'),
            self::SELL_SERVISES => Yii::t('app', 'Продажа услуг'),
            self::SEARCH_CLIENTS => Yii::t('app', 'Поиск новых клиентов, партнёров'),
            self::SUBCONTRACTS => Yii::t('app', 'Субподряд'),
            self::SEARCH_MODELS => Yii::t('app', 'Поиск моделей'),
            self::EXPERIENCE_EXCHANGE => Yii::t('app', 'Обмен опытом'),
            self::SIGN_NEW_CONTRACTS => Yii::t('app', 'Подписание новых контрактов'),
            self::PROMOTE_SERVICES => Yii::t('app', 'Продвижение интересов/торговой марки/услуг'),
            self::COOPERATION_OTHER => Yii::t('app', 'Cooperation with other fashion professionals'),
            self::ACCESS_EVENTS => Yii::t('app', 'Доступ к эксклюзивным мировым VIP ивэнтам закрытого типа'),
        ];
    }

}