<?php

namespace common\models\questionary\models;
use common\models\questionary\QuestionaryModel;

use Yii;

/**
 * This is the model class for table "models_additional_skills".
 *
 * @property integer $id
 * @property integer $additional_skill
 *
 * @property QuestionaryModel $id0
 */
class ModelsAdditionalSkills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models_additional_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'additional_skill'], 'required'],
            [['id', 'additional_skill'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'additional_skill' => Yii::t('app', 'Дополнительный навык'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }
}
