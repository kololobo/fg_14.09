<?php

namespace common\models\questionary\models;
use common\models\questionary\QuestionaryModel;

use Yii;

/**
 * This is the model class for table "models_category".
 *
 * @property integer $id
 * @property integer $models_category
 *
 * @property QuestionaryModel $id0
 */
class ModelsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'models_category'], 'required'],
            [['id', 'models_category'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'models_category' => Yii::t('app', 'Категория'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }
}
