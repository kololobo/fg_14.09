<?php

namespace common\models\questionary\models;

use common\models\questionary\QuestionaryModel;
use Yii;

/**
 * This is the model class for table "models_piercing".
 *
 * @property integer $id
 * @property integer $piercing_place
 *
 * @property QuestionaryModel $id0
 */
class ModelsPiercing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models_piercing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'piercing_place'], 'required'],
            [['id', 'piercing_place'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'piercing_place' => Yii::t('app', 'Место пирсинга'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }
}
