<?php

namespace common\models\questionary\models;
use common\models\questionary\QuestionaryModel;

use Yii;

/**
 * This is the model class for table "models_tatoos".
 *
 * @property integer $id
 * @property integer $tatoo_place
 *
 * @property QuestionaryModel $id0
 */
class ModelsTatoos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models_tatoos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tatoo_place'], 'required'],
            [['id', 'tatoo_place'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryModel::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'tatoo_place' => Yii::t('app', 'Место татуировки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryModel::className(), ['id' => 'id']);
    }
}
