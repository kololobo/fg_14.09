<?php

namespace common\models\questionary\photographers;

use common\models\questionary\QuestionaryPhotographer;
use Yii;

/**
 * This is the model class for table "photographer_areas".
 *
 * @property integer $id
 * @property integer $work_area
 *
 * @property QuestionaryPhotographer $id0
 */
class PhotographerAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photographer_areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_area'], 'required'],
            [['id', 'work_area'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionaryPhotographer::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id пользователя'),
            'work_area' => Yii::t('app', 'Область работы фотографа'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionaryPhotographer::className(), ['id' => 'id']);
    }
}
