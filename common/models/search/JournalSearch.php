<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\forms\JournalForm;

/**
 * JournalSearch represents the model behind the search form about `common\models\forms\JournalForm`.
 */
class JournalSearch extends JournalForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'photo_id', 'category', 'author_id', 'redactor_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JournalForm::find()->with('photo.user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'photo_id' => $this->photo_id,
            'category' => $this->category,
            'author_id' => $this->author_id,
            'redactor_id' => $this->redactor_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }

    public static function getContent($category)
    {

        switch ($category) {
            case 'blog':
                $category = 0;
                break;
            case 'fashionshows':
                $category = 2;
                break;
            case 'persons':
                $category = 3;
                break;
            case 'fashionlife':
                $category = 4;
                break;
            default:
                $category = 1;
                break;
        }

        return self::Find()->where(['category' => $category, 'status' => self::STATUS_PUBLIC])->all();
    }

    public function getCategoryById()
    {
        switch ($this->category) {
            case self::FG_NEWS:
                return 'fgnews';
                break;
            case self::FASHION_SHOWS:
                return 'fashionshows';
                break;
            case self::PERSONS:
                return 'persons';
                break;
            case self::FASHION_LIFE:
                return 'fashionlife';
                break;
            case self::BLOG:
                return 'blog';
                break;
        }
    }



}
