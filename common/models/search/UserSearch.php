<?php

namespace common\models\search;

use common\models\OurTeam;
use common\models\User;
use yii\helpers\ArrayHelper;
use Yii;

class UserSearch extends User
{
    public function getEmails()
    {
        $team = OurTeam::find()->select('user_id')->all();
        $in_team = ArrayHelper::map($team, 'user_id', 'user_id');
        $users = User::Find()->select(['email', 'id'])->all();
        $emails = ArrayHelper::map($users, 'id', 'email');

        foreach ($in_team as $id) {
            unset($emails[$id]);
        }
        return $emails;
    }
}