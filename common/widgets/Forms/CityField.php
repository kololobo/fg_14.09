<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 22.09.2016
 * Time: 21:55
 */

namespace common\widgets\Forms;

use common\models\forms\GeoCityForm;
use dosamigos\typeahead\Bloodhound;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use yii\bootstrap\Html;
use Yii;
use yii\bootstrap\InputWidget;
use yii\helpers\Url;
use dosamigos\typeahead\TypeAhead;

class CityField extends InputWidget
{
    public $class = 'form-control selectpicker';
    public $style = 'btn-primary';

    public $idForm      = '#form';
    public $idContainer = '#pjaxBlock';

    public $country_id;

    public $typeahead = true;

    public function init()
    {
        parent::init();
        /*if (empty($this->country_id)) {
            throw new InvalidConfigException(\Yii::t('app', 'Пустое значение "country_id"'));
        }*/
    }

    public function run()
    {
        if ($this->typeahead) {
            $engine = new Bloodhound([
                'name' => 'countriesEngine',
                'clientOptions' => [
                    'datumTokenizer' => new \yii\web\JsExpression("Bloodhound.tokenizers.obj.whitespace('name')"),
                    'queryTokenizer' => new \yii\web\JsExpression("Bloodhound.tokenizers.whitespace"),
                    'remote' => [
                        'url' => Url::to(['/geo/set-city', 'id'=> $this->country_id, 'q'=>'QRY']),
                        'wildcard' => 'QRY'
                    ]
                ]
            ]);

            if ($this->country_id != null) {
                echo TypeAhead::widget([
                    'name' => 'countriesEngine',
                    'options' => ['class' => 'form-control'],
                    'engines' => [ $engine ],
                    'clientOptions' => [
                        'highlight' => true,
                        'minLength' => 2,
                    ],
                    'clientEvents' => [
                        'typeahead:selected' => new \yii\web\JsExpression(
                            'function(obj, datum, name) {  
                        $("#city-id").val(datum.id);
                    }'
                        ),
                    ],
                    'dataSets' => [
                        [
                            'name' => 'city',
                            'displayKey' => 'city',
                            'source' => $engine->getAdapterScript(),
                            'templates' => [
                                'suggestion' => new \yii\web\JsExpression("function(data){ return '<div class=\"col-xs-12 item-container\"><div class=\"item-header\">' + data.city + '</div><div class=\"item-hint\">' + data.region + '</div></div>'; }"),
                            ],
                        ]
                    ]
                ]);
            } else {
                echo TypeAhead::widget([
                    'name' => 'countriesEngine',
                    'options' => ['class' => 'form-control', 'disabled' => 'true'],
                    'engines' => [ $engine ],
                    'clientOptions' => [
                        'highlight' => true,
                        'minLength' => 2,
                    ],
                    'clientEvents' => [
                        'typeahead:selected' => new \yii\web\JsExpression(
                            'function(obj, datum, name) {  
                        $("#city-id").val(datum.id);
                    }'
                        ),
                    ],
                    'dataSets' => [
                        [
                            'name' => 'city',
                            'displayKey' => 'city',
                            'source' => $engine->getAdapterScript(),
                            'templates' => [
                                'suggestion' => new \yii\web\JsExpression("function(data){ return '<div class=\"col-xs-12 item-container\"><div class=\"item-header\">' + data.city + '</div><div class=\"item-hint\">' + data.region + '</div></div>'; }"),
                            ],
                        ]
                    ]
                ]);
            }

            echo Html::activeHiddenInput($this->model, $this->attribute, ['id' => 'city-id']);
        } else {
            $this->registerClientScript();
            echo Html::activeDropDownList($this->model, $this->attribute, GeoCityForm::getCitiesList(), [
                'class'  => $this->class,
                'data' => [
                    'style' => $this->style,
                    'title' => Yii::t('app', 'Выберите город'),
                ],
                'onchange' => '
                    $.pjax({
                        type: "POST",
                        url: "'.Url::to(['/geo/set-cities']).'",
                        data: jQuery("'.$this->idForm.'").serialize(),
                        container: "'.$this->idContainer.'",
                        push: false, 
                        scrollTo: false
                })'
            ]);
        }
    }

    public function registerClientScript()
    {
        $view = $this->getView();
        BootstrapSelectAsset::register($view);
    }
}