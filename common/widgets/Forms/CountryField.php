<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 22.09.2016
 * Time: 20:46
 */

namespace common\widgets\Forms;

use common\models\forms\GeoCountryForm;
use yii\bootstrap\Html;
use yii\bootstrap\InputWidget;
use yii\helpers\Url;
use Yii;
use phpnt\bootstrapSelect\BootstrapSelectAsset;

class CountryField extends InputWidget
{
    public $class = 'form-control selectpicker';
    public $style = 'btn-primary';

    public $idForm      = '#form';
    public $idContainer = '#pjaxBlock';

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
{
    $this->registerClientScript();

    if (\Yii::$app->setting->show_all_countries == 0 || \Yii::$app->setting->show_all_countries == null) {
        $items = GeoCountryForm::getCountriesSelectedList();
        $liveSearch = '0';
    } else {
        $items = GeoCountryForm::getCountriesFullList();
        $liveSearch = '1';
    }

    echo Html::activeDropDownList($this->model, $this->attribute, $items, [
        'class'  => $this->class,
        'data' => [
            'style' => $this->style,
            'live-search' => $liveSearch,
            'size' => 7,
            'title' => Yii::t('app', 'Выберите страну'),
        ],
        'onchange' => '
                $.pjax({
                    type: "POST",
                    url: "'.Url::to(['/geo/set-country']).'",
                    data: jQuery("'.$this->idForm.'").serialize(),
                    container: "'.$this->idContainer.'",
                    push: false,
                    scrollTo: false
                })'
    ]);
}

    public function registerClientScript()
    {
        $view = $this->getView();
        BootstrapSelectAsset::register($view);
    }
}