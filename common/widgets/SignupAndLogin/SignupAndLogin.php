<?php
/**
 * Created by PhpStorm.
 * User: phpNT
 * Date: 28.05.2016
 * Time: 23:37
 */

namespace common\widgets\SignupAndLogin;

use yii\base\Widget;
use common\models\forms\LoginForm;
use common\models\forms\UserForm;

class SignupAndLogin extends Widget
{
    public $modelLoginForm;
    public $modelSignupForm;

    public function init()
    { 
        $this->modelLoginForm = new LoginForm();
        $this->modelSignupForm = new UserForm(['scenario' => 'visitor']);
        parent::init();
    }

    public function run() {
        return $this->render(
            'view',
            [
                'widget' => $this,
            ]);
    }
}