<?php
/**
 * Created by PhpStorm.
 * User: phpNT
 * Date: 28.05.2016
 * Time: 23:43
 */
/* @var $this yii\web\View */
/* @var $widget \common\widgets\SignupAndLogin\SignupAndLogin */
/* @var $modelLoginForm \common\models\forms\LoginForm */
use yii\bootstrap\Modal;
use yii\helpers\Html;

$model = $widget->modelLoginForm;
?>
<?= Html::a(Yii::t('app', 'Авторизация'), ['#'],
    [
        'class' => 'btn btn-primary menu2e',
        'data-toggle' => 'modal',
        'data-target' => '#loginModal',
        'style' => 'outline: none;',
    ]) ?>
<?php
Modal::begin([
    'id' => 'loginModal',
    'header' => '<h2 class="text-center">'.Yii::t('app', 'Вход').'</h2>',
    'toggleButton' => false,
]);
?>
<?= $this->render('@frontend/views/site/_login-form', ['model' => $model]) ?>
<?php
Modal::end();
?>