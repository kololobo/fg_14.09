<?php
/**
 * Created by PhpStorm.
 * User: phpNT
 * Date: 28.05.2016
 * Time: 23:43
 */
/* @var $this yii\web\View */
/* @var $widget \common\widgets\SignupAndLogin\SignupAndLogin */
/* @var $modelSignupForm \common\models\forms\ProfileUserForm */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use yii\widgets\Pjax;

$model = $widget->modelSignupForm;
?>
<?= Html::a(\Yii::t('app', 'Регистрация'), ['#'],
    [
        'class' => 'btn btn-success menu2e',
        'data-toggle' => 'modal',
        'data-target' => '#signinModal',
        'style' => 'outline: none;',
    ]) ?>
<?php
Modal::begin([
    'id' => 'signinModal',
    'size'  => 'lay-hid-wrap',
    'header' => '<div class="join-form form-group"><label class="control-label custom-class_label1 custom-class_label2"><h1>'.\Yii::t('app', 'Регистрация').'</h1><p>Выберите Ваш ценовой пакет</p></label></div>',
    'toggleButton' => false,
]); 
?>

    <div class="row">

        <?= $this->render('@frontend/views/site/_form-signup', ['model' => $model]) ?>


    </div>

<?php
Modal::end();