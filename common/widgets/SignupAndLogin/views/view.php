<?php
/**
 * Created by PhpStorm.
 * User: phpNT
 * Date: 28.05.2016
 * Time: 23:37
 */
/* @var $this yii\web\View */
/* @var $widget \common\widgets\SignupAndLogin\SignupAndLogin */
/* @var $user \common\models\User */
use yii\widgets\Pjax;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use common\widgets\ICheck\ICheckAsset;

$user = Yii::$app->user->identity;
?>
<!-- <div class="row"> -->
    <!-- <div class="col-xs-6"> -->
        <?php
        /*Pjax::begin([
            'id' => 'pjaxBlockSignup',
            'enablePushState' => false,
        ]);
        BootstrapSelectAsset::register($this);
        ICheckAsset::register($this);*/
        ?>
        <?= $this->render('_signup',
            [
                'widget' => $widget,
            ]); ?>
        <?php
        //Pjax::end();
        ?>
    <!-- </div> -->
    <!-- <div class="col-xs-6"> -->
        <?= $this->render('_login',
            [
                'widget' => $widget,
            ]) ?>
    <!-- </div> -->
<!-- </div> -->

