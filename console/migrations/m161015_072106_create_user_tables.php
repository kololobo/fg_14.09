<?php

use yii\db\Migration;

class m161015_072106_create_user_tables extends Migration
{

    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey()->comment('ID пользователя'),
            'alias'                 => $this->string()->unique()->comment('Уникальная ссылка'),
            'payment_status'        => $this->smallInteger(2)->comment('Статус оплаты'),
            'email'                 => $this->string(64)->unique()->comment('Электронная почта'),
            'lang'                  => $this->string(2)->comment('Язык'),
            'full_phone'            => $this->string(15)->unique()->comment('Полный номер'),
            'description'           => $this->text()->comment('Описание'),
            'status'                => $this->smallInteger(1)->notNull()->comment('Статус'),
            'image_main'            => $this->string(20)->defaultValue('mainUser')->comment('Метка изображения'),
            'password_hash'         => $this->string()->comment('Пароль'),
            'password_encrypted'    => $this->string()->comment('Зашифрованный пароль'),
            'auth_key'              => $this->string(32)->comment('Ключ авторизации'),
            'password_reset_token'  => $this->string()->comment('Ключ сброса пароля'),
            'email_confirm_token'   => $this->string()->comment('Ключ подтверждения эл. адреса'),
            'created_at'            => $this->integer()->comment('Дата создания'),
            'updated_at'            => $this->integer()->comment('Дата изменения')
        ], $tableOptions);

        /* Пользователи онлайн */
        $this->createTable('{{%user_online}}', [
            'user_id'           => $this->primaryKey(),
            'online'            => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('online_user_fk', '{{%user_online}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

        /* Профиль компании */
        $this->createTable('{{%profile_company}}', [
            'id'                    => $this->primaryKey()->comment('ID компании'),
            'type'                  => $this->integer()->comment('Тип компании'),
            'status'                => $this->smallInteger(1)->notNull()->comment('Статус'),
            'name'                  => $this->string()->notNull()->unique()->comment('Название компании'),
            'description'           => $this->text()->comment('Описание'),
            'image_main'            => $this->string(20)->defaultValue('mainCompany')->comment('Метка изображения'),
        ], $tableOptions);

        /* Профиль пользователя */
        $this->createTable('{{%profile_user}}', [
            'id'                    => $this->primaryKey()->comment('ID пользователя'),

            'first_name'            => $this->string(32)->comment('Имя'),
            'last_name'             => $this->string(32)->comment('Фамилия'),
            'gender'                => $this->smallInteger(1)->comment('Пол'),
            'tariff'                => $this->string(20)->comment('Тариф'),
            'type'                  => $this->integer()->comment('Тип пользователя'),           // например (фотограф, модель), а роли определяют тип аккаунта

            'expert_list'           => $this->string(255)->comment('Перечень специализаций эксперта'),
            'skype'                 => $this->string()->comment('Skype'),
            'web_site'              => $this->string()->comment('Веб-сайт'),
            'what_show'             => $this->smallInteger(1)->comment('Что показывать (возраст/дата рождения)'),
            'date_of_birth'         => $this->date()->comment('Дата рождения'),

            'contracted_or_not'     => $this->smallInteger(1)->comment('Связан контрактом'),
            'travel_ability'        => $this->smallInteger(1)->comment('Готовность к переезду'),


            'member_since'          => $this->date()->comment('Дата регистрации'),
            'last_login'            => $this->dateTime()->comment('Последний вход'),

            'company_id'            => $this->integer()->comment('Компания'),
        ], $tableOptions);

        $this->addForeignKey('usermain_fk', '{{%profile_user}}', 'id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('company_fk', '{{%profile_user}}', 'company_id', '{{%profile_company}}', 'id', 'CASCADE', 'CASCADE');     // компания пользователя

        /* Список городов и стран */
        $this->createTable('{{%locations}}', [
            'id'               => $this->primaryKey()->comment('ID города'),
            'city'             => $this->string(50)->comment('Город'),
            'country'          => $this->string(32)->comment('Страна'),
            ], $tableOptions);

        $this->batchInsert('{{%locations}}', ['city', 'country'], [
                    ['Beijing', 'China'],
                    ['Hong Kong', 'China'],
                    ['Shanghai', 'China'],
                    ['Other', 'China'],
                    ['London', 'England'],
                    ['London area', 'England'],
                    ['Other', 'England'],
                    ['Paris', 'France'],
                    ['Paris area', 'France'],
                    ['Other', 'France'],
                    ['Berlin', 'Germany'],
                    ['Berlin area', 'Germany'],
                    ['Other', 'Germany'],
                    ['Milan', 'Italy'],
                    ['Milan area', 'Italy'],
                    ['Rome', 'Italy'],
                    ['Rome area', 'Italy'],
                    ['Other', 'Italy'],
                    ['Los Angeles', 'USA'],
                    ['Los Angeles area', 'USA'],
                    ['New York', 'USA'],
                    ['New York area', 'USA'],
                    ['Other', 'USA'],
                    ['Ukraine', 'Other'],
                    ['Russia', 'Other'],
                    ['Other', 'Other']
                ]);

        /* Cвязь пользователей и локаций */
        $this->createTable('{{%user_locations}}', [
            'user_id'       => $this->integer()->comment('ID пользователя'),
            'location_id'   => $this->integer()->comment('ID локации'),
            ], $tableOptions);

        $this->addForeignKey('loc_user_fk', '{{%user_locations}}', 'user_id', '{{%profile_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('loc_location_fk', '{{%user_locations}}', 'location_id', '{{%locations}}', 'id', 'CASCADE', 'CASCADE');

        /* Владение языками у ПОЛЬЗОВАТЕЛЯ */
        $this->createTable('{{%users_languages}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'language'      => $this->string(2)->notNull()->comment('Язык'),
        ]);
        $this->addForeignKey(
            'FK_user_languages', '{{%users_languages}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Оплата */
        $this->createTable('{{%compensation}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'profession'     => $this->smallInteger(2)->notNull()->comment('профессия / специализация'),
            'compensation'   => $this->smallInteger(2)->notNull()->comment('Компенсация'),
        ]);
        $this->addForeignKey(
            'FK_user_compensation', '{{%compensation}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );
        /* Основные причины быть на сайте  */
        $this->createTable('{{%reasons}}', [
            'id'  => $this->integer()->notNull()->comment('id пользователя'),
            'reason'    => $this->smallInteger(2)->notNull()->comment('Причина'),
        ]);
        $this->addForeignKey(
            'FK_user_reason', '{{%reasons}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Жанры  */
        $this->createTable('{{%genres}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'profession'  => $this->smallInteger(2)->notNull()->comment('профессия / специализация'),
            'genre'   => $this->smallInteger(2)->notNull()->comment('Жанр работы'),
        ]);
        $this->addForeignKey(
            'FK_profession_genre', '{{%genres}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

    }


    public function safeDown()
    {
        $this->dropTable('{{%genres}}');
        $this->dropTable('{{%users_languages}}');
        $this->dropTable('{{%reasons}}');
        $this->dropTable('{{%compensation}}');
        $this->dropTable('{{%user_locations}}');
        $this->dropTable('{{%locations}}');
        $this->dropTable('{{%profile_user}}');
        $this->dropTable('{{%profile_company}}');
        $this->dropTable('{{%user_online}}');
        $this->dropTable('{{%user}}');
    }

}
