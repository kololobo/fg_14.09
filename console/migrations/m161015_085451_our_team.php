<?php

use yii\db\Migration;

class m161015_085451_our_team extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('our_team',
            [
                'user_id'       => $this->integer()->notNull(),
                'position'      => $this->integer(4)->unique() . ' AUTO_INCREMENT',
                'name'          => $this->string(),
                'profession'    => $this->string(),
                'citate'        => $this->text(),
                'photo_id'      => $this->integer(),
                'email'         => $this->string(50)->notNull(),
                'show'          => $this->smallInteger(1)->defaultValue(0),
            ], $tableOptions
        );

        $this->addForeignKey('user_fk', '{{%our_team}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');
        $this->addForeignKey('photo_fk', '{{%our_team}}', 'photo_id', '{{%photo}}', 'id', ' NO ACTION');

    }


    public function safeDown()
    {
        $this->dropTable('our_team');
    }

}
