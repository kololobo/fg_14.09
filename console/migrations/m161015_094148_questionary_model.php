<?php

use yii\db\Migration;

class m161015_094148_questionary_model extends Migration
{

    public function safeUp()
    {
        //Создаем таблицу для каждой професии
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        //Анкета для моделей

        $this->createTable('{{%questionary_model}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),

            'legal_agree'               => $this->integer(1)->notNull()->comment('Разрешение работать (до 18 лет)'),
            'agency_info'               => $this->text()->comment('Информация об агенстве'),

            // Все данные храним в БД в метрической системе
            'height'                    => $this->smallInteger(3)->notNull()->comment('Рост'),
            'weight'                    => $this->smallInteger(3)->notNull()->comment('Вес'),
            'chest'                     => $this->smallInteger(3)->notNull()->comment('Объем груди'),
            'waist'                     => $this->smallInteger(3)->notNull()->comment('Объем талии'),
            'hips'                      => $this->smallInteger(3)->notNull()->comment('Объем бедер'),
            'dress'                     => $this->smallInteger(3)->notNull()->comment('Размер одежды, EU'),
            'cup'                       => $this->smallInteger(2)->comment('Размер чашечки/груди'),
            'shoe'                      => $this->smallInteger(2)->notNull()->comment('Размер обуви, EU'),
            'ethnicity'                 => $this->string(50)->notNull()->comment('Национальность'),

            'skin_color'                => $this->smallInteger(2)->notNull()->comment('Цвет кожи'),
            'eye_color'                 => $this->smallInteger(2)->notNull()->comment('Цвет глаз'),
            'natural_hair_color'        => $this->smallInteger(2)->notNull()->comment('Натуральный цвет волос'),
            'hair_coloration'           => $this->smallInteger(2)->notNull()->comment('Цвет окраса волос'),
            'readness_to_change_color'  => $this->smallInteger(1)->notNull()->comment('Готовность изменить цвет волос'),
            'hair_lenght'               => $this->smallInteger(2)->notNull()->comment('Длина волос'),
            'has_tatoo'                 => $this->smallInteger(1)->notNull()->comment('Наличие татуировки'),
            'has_piercing'              => $this->smallInteger(1)->notNull()->comment('Наличие пирсинга'),

            //профессиональные детали
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
            'rates_for_photoshoot_hour' => $this->integer()->comment('Цена за фотосессию (в час), $'),
            'rates_for_photoshoot_half' => $this->integer()->comment('Цена за фотосессию (за пол дня / 4 часа), $'),
            'rates_for_photoshoot_full' => $this->integer()->comment('Цена за фотосессию (полный день / 8 часов), $'),

            'rates_for_runway'          => $this->text()->comment('Цены за переезд'),
       ], $tableOptions);
        /* Привязка анкет моделей к пользователям*/
        $this->createIndex('FK_model_id', '{{%questionary_model}}', 'id');
        $this->addForeignKey(
            'FK_model_id', '{{%questionary_model}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Татуировки у моделей*/
        $this->createTable('{{%models_tatoos}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'tatoo_place'   => $this->smallInteger(2)->notNull()->comment('Место татуировки'),
        ]);
        $this->addForeignKey(
            'FK_model_tatoo', '{{%models_tatoos}}', 'id', '{{%questionary_model}}', 'id', 'CASCADE'
        );
        /* Пирсинги у моделей*/
        $this->createTable('{{%models_piercing}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'piercing_place'   => $this->smallInteger(2)->notNull()->comment('Место пирсинга'),
        ]);
        $this->addForeignKey(
            'FK_model_piercing', '{{%models_piercing}}', 'id', '{{%questionary_model}}', 'id', 'CASCADE'
        );
        /* Категории у моделей */
        $this->createTable('{{%models_category}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'models_category'   => $this->smallInteger(2)->notNull()->comment('Категория'),
        ]);
        $this->addForeignKey(
            'FK_model_category', '{{%models_category}}', 'id', '{{%questionary_model}}', 'id', 'CASCADE'
        );

        /* Дополнительные навыки у моделей */
        $this->createTable('{{%models_additional_skills}}', [
            'id'          => $this->integer()->notNull()->comment('id пользователя'),
            'additional_skill'   => $this->smallInteger(2)->notNull()->comment('Дополнительный навык'),
        ]);
        $this->addForeignKey(
            'FK_model_skill', '{{%models_additional_skills}}', 'id', '{{%questionary_model}}', 'id', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%models_tatoos}}');
        $this->dropTable('{{%models_piercing}}');
        $this->dropTable('{{%models_category}}');
        $this->dropTable('{{%models_additional_skills}}');
        $this->dropTable('{{%questionary_model}}');
    }

}
