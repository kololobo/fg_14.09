<?php

use yii\db\Migration;

class m161017_092806_questionary_photographer extends Migration
{
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Анкета для фотографов

        $this->createTable('{{%questionary_photographer}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
            'equipment'                 => $this->text()->comment('Тип фотооборудования'),
            'studio'                    => $this->smallInteger(1)->notNull()->comment('Студия'),
            'makeup_artist'             => $this->smallInteger(1)->notNull()->comment('Визажист'),
            'hair_stylist'              => $this->smallInteger(1)->notNull()->comment('Парикмахер'),
            'rates_for_photoshoot_hour' => $this->integer()->comment('Рейты на фотосъемку (в час), $'),
            'rates_for_photoshoot_half' => $this->integer()->comment('Рейты на фотосъемку (за пол дня / 4 часа), $'),
            'rates_for_photoshoot_full' => $this->integer()->comment('Рейты на фотосъемку (полный день / 8 часов), $'),
        ]);


        /* Привязка анкет фотографов к пользователям*/
        $this->createIndex('FK_photographer_id', '{{%questionary_photographer}}', 'id');
        $this->addForeignKey(
            'FK_photographer_id', '{{%questionary_photographer}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Области работы фотографов */
        $this->createTable('{{%photographer_areas}}', [
            'id'   => $this->integer()->notNull()->comment('id пользователя'),
            'work_area'         => $this->smallInteger(2)->notNull()->comment('Область работы фотографа'),
        ]);
        $this->addForeignKey(
            'FK_photographer_area', '{{%photographer_areas}}', 'id', '{{%questionary_photographer}}', 'id', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%photographer_areas}}');
        $this->dropTable('{{%questionary_photographer}}');
    }

}
