<?php

use yii\db\Migration;

class m161017_173659_questionary_designer extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Анкета для дизайнеров

        $this->createTable('{{%questionary_designer}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
            'workplace'                 => $this->text()->comment('Рабочее пространство'),
            'education'                 => $this->text()->comment('Образование / Курсы / Мастер-классы'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);

        /* Привязка анкет дизайнеров к пользователям*/
        $this->createIndex('FK_designer_id', '{{%questionary_designer}}', 'id');
        $this->addForeignKey(
            'FK_designer_id', '{{%questionary_designer}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Тип дизайнера */
        $this->createTable('{{%designer_type}}', [
            'id'   => $this->integer()->notNull()->comment('id пользователя'),
            'work_type'     => $this->smallInteger(2)->notNull()->comment('Область работы дизайнера'),
        ]);
        $this->addForeignKey(
            'FK_designer_type', '{{%designer_type}}', 'id', '{{%questionary_designer}}', 'id', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%designer_type}}');
        $this->dropTable('{{%questionary_designer}}');
    }

}
