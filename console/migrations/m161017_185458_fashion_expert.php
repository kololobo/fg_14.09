<?php

use yii\db\Migration;

class m161017_185458_fashion_expert extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Анкета для экспертов (с множественным выбором профессий)

        $this->createTable('{{%questionary_expert}}', [
            'id'  => $this->primaryKey()->comment('id пользователя'),
        ]);

        /* Привязка анкет экспертов к пользователям*/
        $this->createIndex('FK_expert_id', '{{%questionary_expert}}', 'id');
        $this->addForeignKey(
            'FK_expert_id', '{{%questionary_expert}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Специализация: дизайнер */
        $this->createTable('{{%expert_designer}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
            'workplace'                 => $this->text()->comment('Рабочее пространство'),
            'education'                 => $this->text()->comment('Образование / Курсы / Мастер-классы'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_designer', '{{%expert_designer}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );
        /* Дизайнер: тип работы */
        $this->createTable('{{%expert_designer_type}}', [
            'id'            => $this->integer()->notNull()->comment('id пользователя'),
            'work_type'     => $this->smallInteger(2)->notNull()->comment('Область работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_designer_type', '{{%expert_designer_type}}', 'id', '{{%expert_designer}}', 'id', 'CASCADE'
        );

        /* Специализация: стилист */
        $this->createTable('{{%expert_stylist}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
            'workplace'                 => $this->text()->comment('Рабочее пространство'),
            'education'                 => $this->text()->comment('Образование / Курсы / Мастер-классы'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_stylist', '{{%expert_stylist}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );
        /* Стилист: тип работы */
        $this->createTable('{{%expert_stylist_type}}', [
            'id'            => $this->integer()->notNull()->comment('id пользователя'),
            'work_type'     => $this->smallInteger(2)->notNull()->comment('Область работы'),
        ]);
        $this->addForeignKey(
            'FK_stylist_type', '{{%expert_stylist_type}}', 'id', '{{%expert_stylist}}', 'id', 'CASCADE'
        );

        /* Специализация: Парикмахер */
        $this->createTable('{{%expert_hair}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
            'workplace'                 => $this->text()->comment('Рабочее пространство'),
            'education'                 => $this->text()->comment('Образование / Курсы / Мастер-классы'),
            'cosmetic_brands'           => $this->text()->comment('Бренды косметики, используемые в работе'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_hair', '{{%expert_hair}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );

        /* Специализация: MUA */
        $this->createTable('{{%expert_mua}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
            'workplace'                 => $this->text()->comment('Рабочее пространство'),
            'education'                 => $this->text()->comment('Образование / Курсы / Мастер-классы'),
            'cosmetic_brands'           => $this->text()->comment('Бренды косметики, используемые в работе'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_mua', '{{%expert_mua}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );

        /* Специализация: фотограф */
        $this->createTable('{{%expert_photographer}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
            'equipment'                 => $this->text()->comment('Тип фотооборудования'),
            'studio'                    => $this->smallInteger(1)->notNull()->comment('Студия'),
            'makeup_artist'             => $this->smallInteger(1)->notNull()->comment('Визажист'),
            'hair_stylist'              => $this->smallInteger(1)->notNull()->comment('Парикмахер)'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
        ]);
        $this->addForeignKey(
            'FK_expert_photographer', '{{%expert_photographer}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );

        /* Фотограф: жанры работы */
        $this->createTable('{{%expert_photographer_areas}}', [
            'id'       => $this->integer()->notNull()->comment('id пользователя'),
            'area'    => $this->smallInteger(2)->notNull()->comment('Жанры работы'),
        ]);
        $this->addForeignKey(
            'FK_expert_photographer_areas', '{{%expert_photographer_areas}}', 'id', '{{%expert_photographer}}', 'id', 'CASCADE'
        );

        /* Специализация: модель */
        $this->createTable('{{%expert_model}}', [
            'id'                        => $this->primaryKey()->comment('id пользователя'),
            // Все данные храним в БД в метрической системе
            'height'                    => $this->smallInteger(3)->notNull()->comment('Рост'),
            'weight'                    => $this->smallInteger(3)->notNull()->comment('Вес'),
            'chest'                     => $this->smallInteger(3)->notNull()->comment('Объем груди'),
            'waist'                     => $this->smallInteger(3)->notNull()->comment('Объем талии'),
            'hips'                      => $this->smallInteger(3)->notNull()->comment('Объем бедер'),
            'dress'                     => $this->smallInteger(3)->notNull()->comment('Размер одежды, EU'),
            'cup'                       => $this->smallInteger(2)->comment('Размер чашечки/груди'),
            'shoe'                      => $this->smallInteger(2)->notNull()->comment('Размер обуви, EU'),
            'ethnicity'                 => $this->string(50)->notNull()->comment('Национальность'),
            'skin_color'                => $this->smallInteger(2)->notNull()->comment('Цвет кожи'),
            'eye_color'                 => $this->smallInteger(2)->notNull()->comment('Цвет глаз'),
            'natural_hair_color'        => $this->smallInteger(2)->notNull()->comment('Натуральный цвет волос'),
            'hair_coloration'           => $this->smallInteger(2)->notNull()->comment('Цвет окраса волос'),
            'readness_to_change_color'  => $this->smallInteger(1)->notNull()->comment('Готовность изменить цвет волос'),
            'hair_lenght'               => $this->smallInteger(2)->notNull()->comment('Длина волос'),
            'has_tatoo'                 => $this->smallInteger(1)->notNull()->comment('Наличие татуировки'),
            'has_piercing'              => $this->smallInteger(1)->notNull()->comment('Наличие пирсинга'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
//            'contracted_or_not'         => $this->smallInteger(1)->notNull()->comment('Связан контрактом'),
        ]);
        $this->addForeignKey(
            'FK_expert_model', '{{%expert_model}}', 'id', '{{%questionary_expert}}', 'id', 'CASCADE'
        );

        /* Модели: татуировки*/
        $this->createTable('{{%expert_model_tatoos}}', [
            'id'            => $this->integer()->notNull()->comment('id пользователя'),
            'tatoo_place'   => $this->smallInteger(2)->notNull()->comment('Место татуировки'),
        ]);
        $this->addForeignKey(
            'FK_expert_model_tatoo', '{{%expert_model_tatoos}}', 'id', '{{%expert_model}}', 'id', 'CASCADE'
        );
        /* Модели: пирсинг*/
        $this->createTable('{{%expert_model_piercing}}', [
            'id'               => $this->integer()->notNull()->comment('id пользователя'),
            'piercing_place'   => $this->smallInteger(2)->notNull()->comment('Место пирсинга'),
        ]);
        $this->addForeignKey(
            'FK_expert_model_piercing', '{{%expert_model_piercing}}', 'id', '{{%expert_model}}', 'id', 'CASCADE'
        );
        /* Модели: категории (жанры) */
        $this->createTable('{{%expert_model_category}}', [
            'id'         => $this->integer()->notNull()->comment('id пользователя'),
            'category'   => $this->smallInteger(2)->notNull()->comment('Категория'),
        ]);
        $this->addForeignKey(
            'FK_expert_model_category', '{{%expert_model_category}}', 'id', '{{%expert_model}}', 'id', 'CASCADE'
        );
        /* Дополнительные навыки у моделей */
        $this->createTable('{{%expert_model_additional_skills}}', [
            'id'          => $this->integer()->notNull()->comment('id пользователя'),
            'additional_skill'   => $this->smallInteger(2)->notNull()->comment('Дополнительный навык'),
        ]);
        $this->addForeignKey(
            'FK_expert_model_skill', '{{%expert_model_additional_skills}}', 'id', '{{%expert_model}}', 'id', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%expert_designer_type}}');
        $this->dropTable('{{%expert_stylist_type}}');
        $this->dropTable('{{%expert_photographer_areas}}');

        $this->dropTable('{{%expert_model_tatoos}}');
        $this->dropTable('{{%expert_model_piercing}}');
        $this->dropTable('{{%expert_model_category}}');
        $this->dropTable('{{%expert_model_additional_skills}}');

        $this->dropTable('{{%expert_designer}}');
        $this->dropTable('{{%expert_stylist}}');
        $this->dropTable('{{%expert_hair}}');
        $this->dropTable('{{%expert_mua}}');
        $this->dropTable('{{%expert_photographer}}');
        $this->dropTable('{{%expert_model}}');

        $this->dropTable('{{%questionary_expert}}');

    }

}
