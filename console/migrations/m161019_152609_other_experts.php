<?php

use yii\db\Migration;

class m161019_152609_other_experts extends Migration
{
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Анкета для других экспертов (без множественного выбора профессий)

        /* Бренд */
        $this->createTable('{{%questionary_brand}}', [
            'id'                     => $this->primaryKey()->comment('id пользователя'),
            'company_name'           => $this->string()->unique()->notNull()->comment('Название компании'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
            'trading'                   => $this->smallInteger(2)->comment('Торговля'),
        ]);

        /* Привязка анкеты бренда к пользователю*/
        $this->createIndex('FK_brand_id', '{{%questionary_brand}}', 'id');
        $this->addForeignKey(
            'FK_brand_id', '{{%questionary_brand}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Продукция */
        $this->createTable('{{%brand_range}}', [
            'id'   => $this->integer()->notNull()->comment('id пользователя'),
            'range'  => $this->string(255)->notNull()->comment('Ассортимент товара'),
        ]);
        $this->addForeignKey(
            'FK_brand_range', '{{%brand_range}}', 'id', '{{%questionary_brand}}', 'id', 'CASCADE'
        );

        /* Байер */
        $this->createTable('{{%questionary_buyer}}', [
            'id'                     => $this->primaryKey()->comment('id пользователя'),
            'clients_work'           => $this->string()->notNull()->comment('Клиенты, на которых работаете:'),
            'brands_work'            => $this->string()->comment('Бренды, с которыми вы работаете:'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
            'trading'                   => $this->smallInteger(2)->comment('Торговля'),
        ]);


        /* Привязка анкеты байера к пользователю*/
        $this->createIndex('FK_buyer_id', '{{%questionary_buyer}}', 'id');
        $this->addForeignKey(
            'FK_buyer_id', '{{%questionary_buyer}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* Ассортимент товара */
        $this->createTable('{{%buyer_range}}', [
            'id'   => $this->integer()->notNull()->comment('id пользователя'),
            'range'  => $this->string(255)->notNull()->comment('Тип продукции'),
        ]);
        $this->addForeignKey(
            'FK_buyer_range', '{{%buyer_range}}', 'id', '{{%questionary_buyer}}', 'id', 'CASCADE'
        );


        /* Представитель компании */
        $this->createTable('{{%questionary_representative}}', [
            'id'                  => $this->primaryKey()->comment('id пользователя'),
            'position'            => $this->smallInteger(1)->notNull()->comment('Должность'),
            'scope_of_activity'   => $this->string()->comment('Сфера деятельности'),
            'workplace'           => $this->string()->notNull()->comment('Рабочее место'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        /* Привязка анкеты к пользователю*/
        $this->createIndex('FK_representative_id', '{{%questionary_representative}}', 'id');
        $this->addForeignKey(
            'FK_representative_id', '{{%questionary_representative}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );


        /* Медиа */
        $this->createTable('{{%questionary_media}}', [
            'id'         => $this->primaryKey()->comment('id пользователя'),
            'name'       => $this->string()->notNull()->comment('Название журнала / блога'),
            'position'   => $this->string()->comment('Должность'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        /* Привязка анкеты к пользователю*/
        $this->createIndex('FK_media_id', '{{%questionary_media}}', 'id');
        $this->addForeignKey(
            'FK_media_id', '{{%questionary_media}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

        /* PR-менеджер */
        $this->createTable('{{%questionary_pr}}', [
            'id'            => $this->primaryKey()->comment('id пользователя'),
            'workplace'     => $this->string()->notNull()->comment('Рабочее место'),
            'scope_of_activity'   => $this->string()->comment('Сфера деятельности'),
            'professional_experience'   => $this->smallInteger(2)->notNull()->comment('Опыт работы'),
        ]);
        /* Привязка анкеты к пользователю*/
        $this->createIndex('FK_pr_id', '{{%questionary_pr}}', 'id');
        $this->addForeignKey(
            'FK_pr_id', '{{%questionary_pr}}', 'id', '{{%profile_user}}', 'id', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%brand_range}}');
        $this->dropTable('{{%buyer_range}}');
        $this->dropTable('{{%questionary_pr}}');
        $this->dropTable('{{%questionary_brand}}');
        $this->dropTable('{{%questionary_buyer}}');
        $this->dropTable('{{%questionary_representative}}');
        $this->dropTable('{{%questionary_media}}');
    }

}
