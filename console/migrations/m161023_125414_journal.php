<?php

use yii\db\Migration;

class m161023_125414_journal extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('journal',
            [
                'id'            => $this->primaryKey()->comment('ID статьи'),
                'title'         => $this->string()->notNull()->comment('Название статьи'),
                'content'       => $this->text()->notNull()->comment('Текст статьи'),
                'photo_id'      => $this->integer()->comment('ID картинки, выводимой в списке'),
                'category'      => $this->smallInteger(2)->defaultValue(0)->comment('Категория статьи'),
                'author_id'     => $this->integer()->notNull()->comment('ID автора'),
                'redactor_id'   => $this->integer()->notNull()->comment('ID редактора'),
                // 'content_id'    => $this->integer()->unique()->comment('ID контента (для перевода)'),
                // 'title_id'      => $this->integer()->unique()->comment('ID названия (для перевода)'),
                'status'        => $this->smallInteger(1)->defaultValue(0)->comment('Статус (публиковать?)'),
                'created_at'    => $this->integer()->notNull()->comment('Создано'),
                'updated_at'    => $this->integer()->notNull()->comment('Обновлено'),
            ], $tableOptions
        );

        $this->addForeignKey('redactor_journal_fk', '{{%journal}}', 'redactor_id', '{{%user}}', 'id', 'RESTRICT');
        $this->addForeignKey('author_journal_fk', '{{%journal}}', 'author_id', '{{%user}}', 'id', 'RESTRICT');
        // $this->addForeignKey('photo_journal_fk', '{{%journal}}', 'photo_id', '{{%photo}}', 'id', ' NO ACTION');
        // $this->addForeignKey('content_journal_fk', '{{%journal}}', 'content_id', '{{%content}}', 'id', 'CASCADE', 'CASCADE');
        // $this->addForeignKey('title_journal_fk', '{{%journal}}', 'title_id', '{{%content}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('journal');
    }
}
