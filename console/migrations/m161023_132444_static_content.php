<?php

use yii\db\Migration;

class m161023_132444_static_content extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->batchInsert('{{%content}}', ['category', 'description', 'location', 'message', 'user_id', 'created_at', 'updated_at'], [
            ['static', 'about page', 'frontend/static/about', '<h2>Здесь будет контент для страницы "О нас"</h2> ', 1, time(), time()],
            ['static', 'contacts page', 'frontend/static/contacts', '<h2>Здесь будет контент для страницы "Контакты"</h2> ', 1, time(), time()],
            ['static', 'faq page', 'frontend/static/faq', '<h2>Здесь будет контент для страницы "FAQ"</h2> ', 1, time(), time()],
            ['static', 'mentors page', 'frontend/static/mentors', '<h2>Здесь будет контент для страницы "Наши наставники"</h2> ', 1, time(), time()],
            ['static', 'servises page', 'frontend/static/servises', '<h2>Здесь будет контент для страницы "Наши сервисы"</h2> ', 1, time(), time()],
        ]);
    }

    public function safeDown()
    {
        echo "m161023_132444_static_content cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
