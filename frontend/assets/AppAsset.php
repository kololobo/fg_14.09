<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site/main.css',
        'css/site/screen.css',
        'css/site/customers-style.css',
        'css/form.css',
        'css/old_main.css',
        'css/modal.css',
        'css/site.css',
    ];
    //public $js = [
        //'js/site/main.js',
        //'https://code.jquery.com/jquery-1.10.2.min.js',
    //];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}


