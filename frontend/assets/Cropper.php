<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ImgAreaSelect extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/imgareaselect';
    public $css = [
        'css/imgareaselect-default.css'
    ];
    public $js = [
        'scripts/jquery.imgareaselect.pack.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
