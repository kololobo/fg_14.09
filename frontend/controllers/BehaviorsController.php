<?php
/**
 * Created by PhpStorm.
 * User: phpNT
 * Date: 30.06.2015
 * Time: 5:48
 */

namespace frontend\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use common\components\UserOnlineBehavior;

class BehaviorsController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                /*'denyCallback' => function ($rule, $action) {
                    throw new \Exception('Нет доступа.');
                },*/
                'rules' => [
                    [
                        'allow' => true,
                        'controllers' => ['site'],
                        'actions' => ['logout'],
                        'verbs' => ['POST'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['site'],
                        'actions' => ['index', 'language', 'album', 'logout', 'contact', 'test', 'login', 'signup', 'settings', 'search', 'questionary', 'journal', 'visitor', 'signup-test', 'user-signup', 'company-signup', 'activate-account', 'set-expert',
                            'request-password-reset', 'reset-password', 'set-scenario', 'set-country', 'set-city', 'set-model', 'set-month', 'error', 'subscription', 'recommendations', 'events', 'classic', 'corporate', 'joboffer', 'joboffermy', 'klient', 'paypal', 'searchevents', 'searchjob', 'searchpeople', 'sett', 'vacancies', 'vip'],
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['journal'],
                        'actions' => ['index', 'category', 'article'],
                        'roles' => ['user', 'admin'] /*Админа убрать! Сделать через роли*/
                    ],
                ]
            ],
            'UserOnlineBehavior' => [
                'class' => UserOnlineBehavior::className()
            ]
        ];
    }
}