<?php
namespace frontend\controllers;

use common\models\search\JournalSearch;

use Yii;


class JournalController extends BehaviorsController
{

    public function actionIndex()
    {
        return $this->redirect('category');
    }

    public function actionCategory($category = 'fgnews')
    {
        $contents = JournalSearch::getContent($category);
        return $this->render('index', [
            'contents' => $contents,
            'article' => NULL,
            'category' => $category
            ]);
    }

    public function actionArticle($id)
    {
        $article = JournalSearch::FindOne($id);
        if ($article->status != JournalSearch::STATUS_PUBLIC) {
            unset($article->content);
            $article->title = Yii::t('app', 'Запрашиваемая статья пока не опубликована');
        }
        return $this->render('index', [
            'contents' => NULL,
            'article' => $article,
            'category' => $article->getCategoryById()
            ]);
    }



}