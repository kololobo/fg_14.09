<?php
namespace frontend\controllers;

use common\models\AccountActivation;
use common\models\forms\TypeUserForm;
use common\models\OurTeam;
use common\models\forms\ProfileUserForm;
use common\models\forms\UserForm;
use common\models\questionary\forms\BrandForm;
use common\models\questionary\forms\BuyerForm;
use common\models\questionary\forms\ExpertForm;
use common\models\questionary\forms\MediaForm;
use common\models\questionary\forms\ModelsForm;
use common\models\questionary\QuestionaryExpert;
use common\models\User;

use common\models\Identity;
use common\models\forms\LoginForm;
use common\models\ProfileCompanyForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;

//анкеты
use common\models\questionary\General;
ini_set('display_errors', 'On'); // сообщения с ошибками будут показываться
error_reporting(E_ALL);

class SiteController extends BehaviorsController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('site/settings');
        }
        $ourTeam = (new OurTeam)->getTeam();
        $contactForm = new ContactForm;
        return $this->render('index', [
            'ourTeam' => $ourTeam,
            'contactForm' => $contactForm
            ]);
    }

    public function actionTest()
    {
        return $this->render('test');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->load(Yii::$app->request->post());
        if ($model->login()) {
            return $this->goBack();
        } else {
            return $this->render('_login-form', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSetScenario()
    {
        $model = new UserForm();
        $model->load(Yii::$app->request->post());
        $model->scenario = $model->tariff_name;
        return $this->render('_form-signup', ['model' => $model]);
    }

    public function actionSignup()
    {

        if (!Yii::$app->user->isGuest) {
            /* @var $user Identity */
            Yii::$app->user->logout();
            return $this->redirect('/');
        }
        $model = new UserForm(/*['scenario' => 'visitor']*/);
        $model->load(Yii::$app->request->post());
        $model->scenario = $model->tariff_name;

        if ($model->save()) {

            if ($model->status === Identity::STATUS_ACTIVE && $model->tariff_name != 'visitor') {
                if (\Yii::$app->getUser()->login($model, 3600*24*365)) {
                    /* Временно!!! В продакшне убрать */
                    return $this->redirect('/site/questionary');    

                    return $this->redirect('/site/index');
                }
            } else {

                if ($model->tariff_name == 'visitor') {

                    if ($mail = $model->sendActivationEmail($model)) {
                        \Yii::$app->session->set(
                            'message',
                            [
                                'type' => 'success',
                                'message' => \Yii::t('app', 'Письмо с активацией отправленно на <strong> {email} </strong> (проверьте папку спам).', ['email' => $model->email]),
                            ]
                        );
                        Yii::$app->user->login($model, 3600 * 24 * 366);
                        return $this->redirect('/site/settings');
                    } else {
                        \Yii::$app->session->set(
                            'message',
                            [
                                'type' => 'danger',
                                'message' => \Yii::t('app', 'Ошибка. Письмо не отправлено.'),
                            ]
                        );
                        \Yii::error(\Yii::t('app', 'Error. The letter was not sent.'));
                    }

                    return $this->refresh();
                } else {
                    \Yii::$app->session->set(
                        'message',
                        [
                            'type' => 'success',
                            'message' => \Yii::t('app', 'Вы успешно зарегистрировались. После одобрения администрацией сайта, вам придет письмо на указанную электронную почту.'),
                        ]
                    );
                    Yii::$app->user->login($model, 3600 * 24 * 366);
                    return $this->redirect('/site/questionary');
                }
            }

            return $this->redirect('/site/index');
        }

        if ($model->errors) {
            return $this->render('_form-signup', ['model' => $model]);
        }

        return $this->render('_form-signup', ['model' => $model]);
    }

/*
    public function actionCompanySignup()
    {
        if (!Yii::$app->user->isGuest) { return $this->redirect('/'); }

        $model = new ProfileCompanyForm(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->status === Identity::STATUS_ACTIVE) {
                if (\Yii::$app->getUser()->login($model)) {
                    return $this->redirect('/site/index');
                }
            } else {
                if ($mail = $model->sendActivationEmail($model)) {
                    \Yii::$app->session->set(
                        'message',
                        [
                            'type' => 'success',
                            'message' => \Yii::t('app', 'Письмо с активацией отправленно на <strong> {email} </strong> (проверьте папку спам).', ['email' => $model->email]),
                        ]
                    );
                    return $this->redirect(Url::to(['/site/index']));
                } else {
                    \Yii::$app->session->set(
                        'message',
                        [
                            'type' => 'danger',
                            'message' => \Yii::t('app', 'Ошибка. Письмо не отправлено.'),
                        ]
                    );
                    \Yii::error(\Yii::t('app', 'Error. The letter was not sent.'));
                }

                return $this->refresh();
            }
            return $this->redirect('/site/index');
        }
        return $this->render('signup', ['model' => $model]);
    }

*/
    public function actionActivateAccount($key)
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        try {
            $user = new AccountActivation($key);
        }
        catch(\HttpInvalidParamException $e) {
            \Yii::$app->session->set(
                'message',
                [
                    'type'      => 'danger',
                    'message'   => \Yii::t('app', 'Неправильный ключ. Повторите регистрацию.'),
                ]
            );
            throw new BadRequestHttpException($e->getMessage());
        }

        if($user = $user->activateAccount()) {
            /* @var $user Identity */
            \Yii::$app->session->set(
                'message',
                [
                    'type'      => 'success',
                    'message'   => \Yii::t('app', 'Активация прошла успешно.'),
                ]
            );
            \Yii::$app->getUser()->login($user);
            return $this->redirect(['/site/index']);
        } else {
            \Yii::$app->session->set('message',
                [
                    'type'      => 'danger',
                    'message'   => \Yii::t('app', 'Ошибка активации.'),
                ]
            );
        }

        return $this->redirect(Url::to(['/site/index']));
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->session->set('message',
                    [
                        'type'      => 'success',
                        'message'   => \Yii::t('app', 'Проверьте ваш емайл и следуйте дальнейшим инструкциям.'),
                    ]
                );
                return $this->goHome();
            } else {
                \Yii::$app->session->set('message',
                    [
                        'type'      => 'danger',
                        'message'   => \Yii::t('app', 'Извините, мы не можем сбросить пароль для введенной электронной почты.'),
                    ]
                );
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            \Yii::$app->session->set('message',
                [
                    'type'      => 'success',
                    'message'   => \Yii::t('app', 'Новый пароль сохранен.'),
                ]
            );
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionContact()
    {
        $contactForm = new ContactForm;

        if ($contactForm->load(Yii::$app->request->post()) && $contactForm->validate()) {
            if($contactForm->sendEmail()){
                Yii::$app->getSession()->setFlash('contactForm', 'Спасибо за сообщение. <br>Мы свяжимся с вами как только сможем.');
            } else{
                Yii::$app->getSession()->setFlash('contactForm', 'Ошибка при отправке письма. <br> Обратитесь напрямую к администратору сайта support@fashiongreatness.com');
            }
        }
        var_dump($contactForm);
        return $this->render('index', [
//            'ourTeam' => OurTeam::getTeam(),
            'contactForm' => $contactForm
            ]);
    }

    public function actionSettings()
    {
        $msg  = "";
        // if ($post = Yii::$app->request->post()) {
        //     $form = $post['Users'];
        //     $password_form = ChangePassword::findIdentity(Yii::$app->user->getId());
        //     $password_form->old_password = $form['old_password'];
        //     $password_form->new_password = $form['new_password'];
        //     $password_form->new_password_repeat = $form['repeat_password'];
        //     if (!$msg = $password_form->change()) {
        //         $msg = "Пароль успешно изменен";
        //     }
        // }
        return $this->render('settings', [
            'msg' => $msg]);
    }

    public function actionJournal()
    {
        return $this->render('journal');
    }

    public function actionSearch()
    {
        return $this->render('search');
    }

    public function actionVisitor()
    {
        return $this->render('visitor');
    }

    /* Переводы */

    public function actionLanguage($lang)
    {
        \Yii::$app->language = $lang;

        return $this->redirect(Url::to(['/site/index']));
    }

    /*от 26.09 */
    public function actionEvents()
    {
        return $this->render('events');
    }
    public function actionRecommendations()
    {
        return $this->render('recommendations');
    }
    public function actionSubscription()
    {
        return $this->render('subscription');
    }
    public function actionClassic()
    {
        return $this->render('classic');
    }
    public function actionCorporate()
    {
        return $this->render('corporate');
    }
    public function actionJoboffer()
    {
        return $this->render('joboffer');
    }
    public function actionJoboffermy()
    {
        return $this->render('joboffermy');
    }
    public function actionKlient()
    {
        return $this->render('klient');
    }
    public function actionPaypal()
    {
        return $this->render('paypal');
    }
    public function actionSearchevents()
    {
        return $this->render('searchevents');
    }
    public function actionSearchjob()
    {
        return $this->render('searchjob');
    }
    public function actionSearchpeople()
    {
        return $this->render('searchpeople');
    }
    public function actionSett()
    {
        return $this->render('sett');
    }
    public function actionVacancies()
    {
        return $this->render('vacancies');
    }
    public function actionVip()
    {
        return $this->render('vip');
    }
    public function actionAlbum()
    {
        return $this->render('album');
    }
    /**/

    public function actionQuestionary()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/index');
        }

        $general = General::findOne(\Yii::$app->user->identity->id);
        /* Задаем основную профессию */
        $profession = General::getQuestionaryModels($general->type, $general->id);

        if (Yii::$app->request->isPjax || Yii::$app->request->post()) {
            $profession->load(Yii::$app->request->post());
        }
//        var_dump($profession->specialization);
        /* Выборка по экспертам */
        $professions_expert = ExpertForm::getExperts($profession);



        if (Yii::$app->request->post() && !Yii::$app->request->isPjax) {
            $general->load(Yii::$app->request->post());
            if ($general->validate()) {

                $err = FALSE;
                if ($profession) {
                    $profession->load(Yii::$app->request->post());
                    $profession->setAttribute('id', $general->id);

                    if (!$profession->validate()) {
                        $err = TRUE;
                    }

                }

                if ($general->type == TypeUserForm::EXPERT) {
                    foreach ($professions_expert as $expert_prof) {
                        $expert_prof->load(Yii::$app->request->post());
                        $expert_prof->setAttribute('id', $general->id);
                        if (!$expert_prof->validate()) {
                            $err = TRUE;
                        }
                    }
                }
                if (!$err) {

                    $general->save();
                    if ($profession){
                        $profession->save();
                    }
                    foreach ($professions_expert as $expert_prof) {
                        $expert_prof->save();
                    }

                    die('ok');
                }
            }
        }

        return $this->render('questionary', [
            'general' => $general,
            'profession' => $profession,
            'professions_expert' => $professions_expert
            ]);
    }

}