<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\search\ContentSearch;


class StaticController extends Controller
{

    public function actionAbout()
    {
        $content = ContentSearch::find()->where(['location' => 'frontend/static/about'])->one();
        return $this->render('about', ['content' => $content->message]);
    }

    public function actionWrite()
    {
        return $this->render('write');
    }

    public function actionContacts()
    {
        $content = ContentSearch::find()->where(['location' => 'frontend/static/contacts'])->one();
        return $this->render('contacts', ['content' => $content->message]);
    }

        public function actionFaq()
    {
        $content = ContentSearch::find()->where(['location' => 'frontend/static/faq'])->one();
        return $this->render('faq', ['content' => $content->message]);
    }

    public function actionMentors()
    {
        $content = ContentSearch::find()->where(['location' => 'frontend/static/mentors'])->one();
        return $this->render('mentors', ['content' => $content->message]);
    }

    public function actionServices()
    {
        $content = ContentSearch::find()->where(['location' => 'frontend/static/services'])->one();
        return $this->render('services', ['content' => $content->message]);
    }


}