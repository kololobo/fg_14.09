<?php

namespace frontend\models;
// namespace frontend\models\AdminContacts;

use Yii;
use yii\base\Model;
use frontend\models\AdminContacts;
use frontend\models\ContactForm;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $category_question;
    public $body;

    public function categories()
    {
        $arr = array();
        array_push($arr, Yii::t('app', 'Общие вопросы'));
        array_push($arr, Yii::t('app', 'Техническая поддержка'));
        array_push($arr, Yii::t('app', 'Партнерство / Сотрудничество'));
        array_push($arr, Yii::t('app', 'Мероприятия'));
        array_push($arr, Yii::t('app', 'Претензии и предложения'));
        array_push($arr, Yii::t('app', 'Пресс-службы'));
        array_push($arr, Yii::t('app', 'Другие'));
        return $arr;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body'], 'required'],
            ['category_question', 'required', 'message' => Yii::t('app', 'Выберите категорию вопроса')],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
        {
            return [
                'name' => Yii::t('app', 'Имя'),
                'body' => Yii::t('app', 'Сообщение'),
                'category_question' => Yii::t('app', 'Категория вопроса'),
            ];
        }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        switch ($this->category_question) {
            case '0': $email = 'info@fashiongreatness.com'; break;
            case '1': $email = 'support@fashiongreatness.com'; break;
            case '2': $email = 'contacts@fashiongreatness.com'; break;
            case '3': $email = 'contacts@fashiongreatness.com'; break;
            case '4': $email = 'support@fashiongreatness.com'; break;
            case '5': $email = 'contacts@fashiongreatness.com'; break;
            case '6': $email = 'info@fashiongreatness.com'; break;
        }

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->category_question)
            ->setTextBody($this->body)
            ->send();
    }

}
