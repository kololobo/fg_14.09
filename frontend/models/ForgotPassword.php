<?php

namespace frontend\models;

use Yii;

use common\models\Identity;
use frontend\models\Registration;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $desc
 * @property string $phone
 * @property string $phone2
 * @property string $lang
 * @property string $email
 * @property integer $city_id
 * @property integer $country_id
 * @property integer $status
 * @property string $image_main
 * @property string $images
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class ForgotPassword extends Identity
{
    public $ok = FALSE;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email' , 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'email'),
        ];
    }


    private function sendEmail()
    {
        $from = 'robot@fashiongreatness.com';
        return Yii::$app->mailer->compose('new_password', [
            'name' => $this->first_name,
            'password' => $this->new_password
            ])
            ->setTo($this->email)
            ->setFrom($from)
            ->setSubject('Изменение пароля на сайте fashiongreatness.com')
            ->send();
    }
}
