<?php

namespace frontend\models;

use Yii;
use common\models\User;
use common\models\Identity;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $desc
 * @property string $phone
 * @property string $phone2
 * @property string $lang
 * @property string $email
 * @property integer $city_id
 * @property integer $country_id
 * @property integer $status
 * @property string $image_main
 * @property string $images
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class Registration extends Identity
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'lang', 'city_id', 'email'/*, 'password_hash', 'created_at'*/] , 'required'],
            [['description'], 'string'],
            [['city_id', 'status', 'created_at', 'updated_at'], 'integer'],
            // [['company_name', 'password_hash', 'password_reset_token', 'email_confirm_token'], 'string', 'max' => 255],
            // [['first_name', 'last_name', 'middle_name', 'auth_key'], 'string', 'max' => 32],
            [['phone'], 'string', 'max' => 20],
            // [['phone'], 'match', 'pattern' => "/(\+|\d+|\s){6,20}$/"],
            [['lang'], 'string', 'max' => 2],
            [['email'], 'ajaxValidate'],
            [['email'], 'string', 'max' => 64],
            // [['image_main', 'images'], 'string', 'max' => 20],
            ['email', 'unique'],
            ['email', 'email'],
            // ['email'],
            // [['image_main'], 'unique'],
            // [['images'], 'unique'],
            // [['password_reset_token'], 'unique'],
            // [['email_confirm_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'desc' => Yii::t('app', 'Desc'),
            'phone' => Yii::t('app', 'Phone'),
            'phone2' => Yii::t('app', 'Phone2'),
            'lang' => Yii::t('app', 'Lang'),
            'email' => Yii::t('app', 'Email'),
            'city_id' => Yii::t('app', 'City ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'status' => Yii::t('app', 'Status'),
            'image_main' => Yii::t('app', 'Image Main'),
            'images' => Yii::t('app', 'Images'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email_confirm_token' => Yii::t('app', 'Email Confirm Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function ajaxValidate($attribute)
    {
        $user = User::findOne(['email' => $this->email]);
        if($user){
            $this->addError($attribute, 'This email is already registered');
        }
    }

    public $profs_count;
    public $profession;

    public function getAccountsType()
    {
        return array(
            'VISITOR' => 'ПОСЕТИТЕЛЬ',
            'CLASSIC' => 'КЛАССИЧЕСКИЙ',
            'VIP' => 'ВИП',
            'CORPORATE' => 'КОРПОРАТИВНЫЙ'
            );
    }

    public function languages()
    {
        return array(
            'ru' => 'РУССКИЙ',
            'en' => 'АНГЛИЙСКИЙ',
            'ch' => 'КИТАЙСКИЙ',
            'fr' => 'ФРАНЦУЗСКИЙ'
            );
    }

    public function countries()
    {
        return array(
            '1' => 'РОССИЯ',
            '2' => 'АНГЛИЯ',
            '3' => 'КИТАЙ',
            '4' => 'ФРАНЦИЯ'
            );
    }

    public function sel_profs()
    {
        return array(
            '1' => 'МОДЕЛЬ',
            '2' => 'ФОТОГРАФ',
            '3' => 'ДИЗАЙНЕР',
            '4' => 'АГЕНСТВО',
            '5' => 'ЭКСПЕРТ',

            );
    }

    public function registration()
    {
        $password = $this->genPassword();
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        $this->created_at = time();
        if (!$this->validate()) {
            return false;
        }
        if ($this->save() && $this->sendEmail($password)) {
            Yii::$app->user->login($this, 3600 * 24 * 366);
            return true;
        } else {
            die('ошибка при отправке письма');
        }
    }

    private function genPassword ($length=10) {
         $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP!@#$%^&*()_";
         $length = intval($length);
         $size=strlen($chars)-1;
         $password = "";
         while($length--) $password.=$chars[rand(0,$size)];
         // $password = '12345678';
         return $password;
    }

    private function sendEmail($password)
    {
        $from = 'robot@fashiongreatness.com';
        return Yii::$app->mailer->compose('registration', [
            'name' => $this->first_name,
            'password' => $password
            ])
            ->setTo($this->email)
            ->setFrom($from)
            ->setSubject('Регистрация на сайте fashiongreatness.com')
            ->send();
    }
}
