<?php
use yii\helpers\Url;
?>
<!-- Навигация для журнала -->
<div class="nav-bar">
    <ul class="nav2">
        <li <?php if ($active == 'fgnews') echo 'class="active"'?>>
            <a href="<?= Url::to(['/journal/category', 'category' => 'fgnews'])?>">FG News</a>
        </li>
        <li <?php if ($active == 'fashionshows') echo 'class="active"'?>>
            <a href="<?= Url::to(['/journal/category', 'category' => 'fashionshows'])?>">Fashion Shows</a>
        </li>
        <li <?php if ($active == 'persons') echo 'class="active"'?>>
            <a href="<?= Url::to(['/journal/category', 'category' => 'persons'])?>">Persons</a>
        </li>
        <li <?php if ($active == 'fashionlife') echo 'class="active"'?>>
            <a href="<?= Url::to(['/journal/category', 'category' => 'fashionlife'])?>">Fashion life</a>
        </li>
        <li <?php if ($active == 'blog') echo 'class="active"'?>>
            <a href="<?= Url::to(['/journal/category', 'category' => 'blog'])?>">Blog</a>
        </li>
    </ul>
</div>