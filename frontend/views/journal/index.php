<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<?= $this->render('../site/_visitor_nav') ?>
<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Журнал'); ?>

<div class="person-wrapper">
	<div class="j-main">
		<h2>Journal</h2>
		<span class="hr"></span>
			<?= $this->render('_navbar', ['active' => $category]) ?>
			<?php if ($contents): ?>
				<?php foreach ($contents as $content): ?>
				<div class="j-item journal_item">
					<div class="j-img">
						<img src="">
					</div>
					<div class="j-txt">
						<h3><a href="<?= Url::to(['/journal/article/', 'id' => $content->id]) ?>"><?= $content->title ?></a></h3>
						<p><?= StringHelper::truncate($content->content, 250, '...', NULL, TRUE) ?></p>
						<br>
						<p><?= gmdate("Y-m-d H:i", $content->updated_at) ?></p>
					</div>
				</div>
				<?php endforeach ?>
			<?php endif ?>
			<?php if ($article): ?>
				<br>
				<h3><?= $article->title ?></h3>
				<span class="hr"></span>
				<?= $article->content ?>
			<?php endif ?>

	</div>
	<div class="j-sidebar">
		<div class="j-t-port journal_profile">
			<div class="j-t-head">
				Top profile
			</div>
			<div class="j-port-cont">
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
		<div class="j-t-phot journal_photo">
			<div class="j-t-head">
				Top photo
			</div>
			<div class="j-phot-cont">
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
	</div>
</div>