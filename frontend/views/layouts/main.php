<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $user \common\models\User */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use phpnt\bootstrapNotify\BootstrapNotify;
use phpnt\fontAwesome\FontAwesomeAsset;
use yii\helpers\Url;

AppAsset::register($this);
FontAwesomeAsset::register($this);
$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

<?php if (Yii::$app->user->isGuest) {?>
<div class="head">
    <div class="lang">
        <a style="color:#fff;" href=<?= Url::to(["/site/language", 'lang' => "en"])?>><div class="lang_N tlanguage" data="N">en</div></a>
        <a style="color:#fff;" href=<?= Url::to(["/site/language", 'lang' => "ru"])?>><div class="lang_Y tlanguage" data="Y">ru</div></a>
        <a style="color:#fff;" href=<?= Url::to(["/site/language", 'lang' => "fr"])?>><div class="lang_N tlanguage" data="N">fr</div></a>
        <a style="color:#fff;" href=<?= Url::to(["/site/language", 'lang' => "ch"])?>><div class="lang_N tlanguage" data="N">ch</div></a>
    </div>

    <div class="title">
        <a class="logo" href="/">
            <span class="slogan">эксклюзивная презентация в сети</span>
        </a>
        <div class="clear"></div>
    </div>

    <div class="menu1" id="top-menu">
        <ul>
            <li class="active">
                <a href="home" rel="insert">ДОМОЙ</a>
            </li>
            <li>
                <a href="about" rel="insert">О НАС</a>
            </li>
            <li>
                <a href="features" rel="insert">ПРЕИМУЩЕСТВА</a>
            </li>
            <li>
                <a href="team" rel="insert">КОМАНДА</a>
            </li>
            <li>
                <a href="prices" rel="insert">ЦЕНОВЫЕ ПАКЕТЫ</a>
            </li>
            <li>
                <a href="contactus" rel="insert">СВЯЖИТЕСЬ С НАМИ</a>
            </li>
        </ul>
            <div class="clear"></div>
    </div>
</div>

<?php } else {?>
<div class="head logedHead">
        <div class="title">
            <p class="logo">
                <!--span class="slogan"><?= Yii::t('app', "слоган")?></span-->
            </p>
        </div>
        <div class="c-navigation">
            <div class="c-nav-text">
                <p><a href="id<?= $user->id ?>"><?= Yii::t('app', 'Имя')?></a></p>
                <p><a href=<?= Url::to("/site/settings") ?>><?= Yii::t('app', "Настройки")?></a>&nbsp;
                    <a href=<?= Url::to("/site/logout") ?>><?= Yii::t('app', "Выйти")?></a></p>
            </div>
            <div class="c-nav-img">
                <!-- < ?php if ($user->image_main): ?> -->
                    <!-- <img src="../images/userpics/< ?= $user->image_main ?>" alt="< ?= Yii::t('content', 'Фотография профиля') ?>" width='40px' > -->
                <!-- < ?php else: ?> -->
<!--                    <img src="--><?//= Url::to('@web/images/userpics/no-avatar.png') ?><!--" alt="--><?//= Yii::t('content', 'Фотография профиля') ?><!--" width='40px' >-->
                <!-- < ?php endif ?> -->
            </div>
    </div>
<!--     <div class="c-bg visitor">
        <a class="c-bg-btn" href="">< ?= Yii::t('app', "Сменить фон")?></a> <?/* Сменить фон */?>
    </div> -->
</div>
<?php } ?>


<?= $content ?>

<?php if (!(Yii::$app->controller->route == 'site/index')){ ?>
    <div class="footer">
            <a href="<?= Url::to('/static/write') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-pencil-square-o"></i>
                    <br>write to us
            </a>
            <a href="<?= Url::to('/static/about') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-info-circle"></i>
                    <br>about us
            </a>
            <a href="<?= Url::to('/static/services') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-cogs"></i>
                    <br>our services
            </a>
            <a href="<?= Url::to('/static/mentors') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-users"></i>
                    <br>our mentors
            </a>
            <a href="<?= Url::to('/static/faq') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-question-circle"></i>
                    <br>faq
            </a>
            <a href="<?= Url::to('/static/contacts') ?>">
                    <i aria-hidden="true" class="fa fa-4x fa-envelope-o"></i>
                    <br>contacts
            </a>
    </div>
<?php } ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
