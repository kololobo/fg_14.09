<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\agency */
/* @var $form ActiveForm */

?>
<div class="questionary-_agency">
    <h3>Анкета для агенства</h3>

        <?= $form->field($model, 'adress')->textInput() ?>
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'age')->textInput() ?>
<!--        --><?//= $form->field($model, 'model_category')->checkboxList($model->model_category) ?>
        <?= $form->field($model, 'professional_experience') ?>
        <?= $form->field($model, 'total_models') ?>
        <?= $form->field($model, 'specialization') ?>
        <?= $form->field($model, 'other_requipments') ?>
        <?= $form->field($model, 'social_media') ?>
        <?= $form->field($model, 'main_reason_on_site') ?>

</div><!-- questionary-_agency -->
