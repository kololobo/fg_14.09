<?php

use yii\widgets\ActiveForm;
?>

<?= $form->field($model, 'company_name') ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences) ?>
<?= $form->field($model, 'product_range')->checkboxList($model->productRanges) ?>
<?= $form->field($model, 'trading')->dropDownList($model->tradingList) ?>
