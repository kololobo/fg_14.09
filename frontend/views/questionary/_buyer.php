<?php

use yii\widgets\ActiveForm;
?>

<?= $form->field($model, 'clients_work')->textarea() ?>
<?= $form->field($model, 'brands_work')->textarea() ?>
<?= $form->field($model, 'product_range')->checkboxList($model->productRanges) ?>
<?= $form->field($model, 'trading')->dropDownList($model->tradingList) ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences) ?>
