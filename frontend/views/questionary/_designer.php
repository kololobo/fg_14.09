<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\questionary\forms\DesignerForm */
/* @var $form ActiveForm */
?>
<div class="questionary-_designer">

        <?= $form->field($model, 'work_type')->checkboxList($model->types) ?>
        <?= $form->field($model, 'workplace')->textarea() ?>
        <?= $form->field($model, 'education')->textarea() ?>
        <?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences) ?>
        <?= $form->field($model, 'compensation')->checkboxList($model->compensations) ?>

</div><!-- questionary-_designer -->
