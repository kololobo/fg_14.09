<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
?>

        <?= $form->field($model, 'work_type')->checkboxList($model->designerForm->types) ?>
        <?= $form->field($model, 'workplace')->textarea() ?>
        <?= $form->field($model, 'education')->textarea() ?>
        <?= $form->field($model, 'professional_experience')->dropDownList($model->designerForm->professionalExperiences) ?>
        <?= $form->field($model, 'compensation')->checkboxList($model->designerForm->compensations) ?>

