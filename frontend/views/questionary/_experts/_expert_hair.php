<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
?>

<?= $form->field($model, 'genre')->checkboxList($model->types) ?>
<?//= $form->field($model, 'contracted_or_not')->dropDownList($model->contractedList) ?>
<?= $form->field($model, 'workplace')->textarea() ?>
<?= $form->field($model, 'education')->textarea() ?>
<?= $form->field($model, 'cosmetic_brands')->textarea() ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences) ?>
<?= $form->field($model, 'compensation')->checkboxList($model->compensations) ?>


