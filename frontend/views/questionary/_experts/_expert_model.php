<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
$this->registerJSFile('@web/js/questionary/models.js', ['depends' => 'yii\web\JqueryAsset']);

?>
<!--   Пока не работает     --><?//= $form->field($model, 'metrical')->radioList($model->metricSys) ?>
<?= $form->field($model, 'height') ?>
<?= $form->field($model, 'weight') ?>
<?= $form->field($model, 'waist') ?>
<?= $form->field($model, 'hips') ?>
<?= $form->field($model, 'chest') ?>
<?= $form->field($model, 'dress') ?>
<?= $form->field($model, 'shoe') ?>
<?= $form->field($model, 'cup')->dropDownList($model->modelsForm->cups) ?>

<?= $form->field($model, 'ethnicity')->dropDownList($model->modelsForm->ethnicityList) ?>

<?= $form->field($model, 'ethnicity_input', [
    'options' => [
        'style' => 'display:none',
        'id' => 'ethnicity_input'
    ]
])->textInput() ?>

<?= $form->field($model, 'skin_color')->dropDownList($model->modelsForm->skins)->hint('Цвет кожи') ?>
<?= $form->field($model, 'eye_color')->dropDownList($model->modelsForm->eyes) ?>
<?= $form->field($model, 'natural_hair_color')->dropDownList($model->modelsForm->hairs) ?>
<?= $form->field($model, 'hair_coloration')->dropDownList($model->modelsForm->hairColorations) ?>
<?= $form->field($model, 'readness_to_change_color')->dropDownList($model->modelsForm->readnessList) ?>
<?= $form->field($model, 'hair_lenght')->dropDownList($model->modelsForm->hairLenghts) ?>
<?= $form->field($model, 'has_tatoo', [
    'options' => ['id' => 'has_tatoos'],
    'inputOptions' => ['class' => 'custom-radio']
])->radioList($model->modelsForm->hasTatoos) ?>
<?= $form->field($model, 'tatoos',[
    'options' => [
        'style' => 'display:none',
        'id' => 'tatoos'
    ]
])->checkBoxList($model->modelsForm->tatoo) ?>
<?= $form->field($model, 'has_piercing', [
    'options' => ['id' => 'has_piercing']
])->radioList($model->modelsForm->hasPiercing) ?>
<?= $form->field($model, 'piercing', [
    'options' => [
        'style' => 'display:none',
        'id' => 'piercing'
    ]
])->checkBoxList($model->modelsForm->piercings) ?>

<?= $form->field($model, 'model_category')->checkBoxList($model->modelsForm->categories) ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->modelsForm->professionalExperiences) ?>
<?= $form->field($model, 'additional_skills')->checkBoxList($model->modelsForm->additionalSkills) ?>
<?= $form->field($model, 'compensation')->checkBoxList($model->modelsForm->compensations) ?>
<?//= $form->field($model, 'contracted_or_not')->dropDownList($model->modelsForm->contractedList) ?>




