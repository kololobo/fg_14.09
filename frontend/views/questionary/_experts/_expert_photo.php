<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\questionary\expert\forms\ExpertPhotographerForm */
/* @var $form ActiveForm */
?>

<?= $form->field($model, 'work_areas')->checkBoxList($model->photographerForm->areas) ?>
<?= $form->field($model, 'work_type')->checkBoxList($model->photographerForm->types) ?>
<?= $form->field($model, 'equipment')->textarea() ?>
<?= $form->field($model, 'studio')->dropDownList($model->photographerForm->provides) ?>
<?= $form->field($model, 'makeup_artist')->dropDownList($model->photographerForm->provides) ?>
<?= $form->field($model, 'hair_stylist')->dropDownList($model->photographerForm->provides) ?>
<?= $form->field($model, 'compensation')->checkBoxList($model->photographerForm->compensations) ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->photographerForm->professionalExperiences) ?>

