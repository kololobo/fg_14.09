<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $professions_expert common\models\questionary\expert\ */
$this->registerJSFile('@web/js/questionary/expert.js', ['depends' => 'yii\web\JqueryAsset']);

?>
<?php
Pjax::begin([
    'id' => 'pjaxBlock',
    'enablePushState' => false,
]);
?>

    <?= $form->field($model, 'specialization')->checkboxList($model->specializationList,
        [
            'onchange' => '
                    $.pjax({
                        type: "POST",
                        url: "'.Url::to(['/site/questionary']).'",
                        data: jQuery("#w0").serialize(),
                        container: "#pjaxBlock",
                        push: false,
                        scrollTo: false
                    });
                    ',
        ])
        ->hint(Yii::t('app', 'укажите Вашу специализацию')) ?>

    <?php foreach ($professions_expert as $profession) : ?>
        <h3><?= $profession->title ?></h3>
        <?= $this->render('../questionary/_experts/'.$profession->view , [
            'form'	 => $form,
            'model' => $profession
        ]) ?>
    <?php endforeach ?>
    <?php Pjax::end(); ?>

