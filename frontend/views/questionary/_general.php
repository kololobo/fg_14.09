<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\questionary\General */
/* @var $form ActiveForm */
BootstrapSelectAsset::register($this);
$this->registerJSFile('@web/js/questionary/general.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="questionary-_general">

        <?= $form->field($general, 'first_name')->textInput() ?>
        <?= $form->field($general, 'last_name')->textInput() ?>

<!--        --><?=$form->field($general->user, 'full_phone', ['template' => '{label}<div class="input-group"><span class="input-group-addon">+</span><div class="text-left" slyle="background-color: #cc0000;">{input}</div></div>']) ?>


<!--        --><?= $form->field($general->user, 'email')->textInput() ?>
        <?= $form->field($general, 'skype')->textInput() ?>
        <?= $form->field($general, 'gender')->dropDownList($general->genders) ?>

        <?php if ($models) echo $form->field($models, 'agency_info')->textArea() ?>
        <?= $form->field($general, 'web_site')->textInput() ?>
        <?= $form->field($general, 'date_of_birth')->widget(DatePicker::classname(), [
            'dateFormat' => 'yyyy-MM-dd',
         ]) ?>

        <?= $form->field($general, 'what_show')->radioList($general->whatShows, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<label class=\"custom-radio\"><input type=\"radio\" name=\"$name\" value=\"$value\"$check><span><div></div></span> $label</label>";
            }
        ]) ?>
        <?php if ($models) echo  $form->field($models, 'legal_agree', [
                'options' => [
                    'id' => 'legal_agree',
                    'style' => 'display:none']
                ])->radioList($models->legalList)?>
    <div class="form-group">
        <label class="control-label" for="location">Локации</label>
        <div class="details-data general-input">
            <?php foreach ($general->locsList as $country => $city_list) : ?>
               <label class="q-check"><input type="checkbox" name="" id = '<?= $country ?>' onchange = 'showOrHide("#<?= $country ?>", "#<?= $country ?>-city");'/><span><div></div></span><?= $country ?>:</label>
                <br>
                <?= $form->field($general, $country, [
                    'options' => [
                        'style' => 'display:none',
                        'id' => $country."-city"
                    ],
                ])->checkboxList($city_list, [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $check = $checked ? ' checked="checked"' : '';
                        return "<label class=\"custom-radio q-check\"><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span><div></div></span>$label</label>";
                    }
                ])->label(false)
                    ?>
                <div class="clearfix"></div>

            <?php endforeach; ?>
        </div>
    </div>


    <?= $form->field($general, 'languages')->checkBoxList($general->langList, [
        'item' => function ($index, $label, $name, $checked, $value) {
            $check = $checked ? ' checked="checked"' : '';
            return "<label class=\"custom-radio\"><input type=\"checkbox\" name=\"$name\" value=\"$value\"$check><span><div></div></span> $label</label>";
        }
    ]) ?>

</div><!-- questionary-_general -->
