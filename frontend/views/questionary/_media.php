
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'position')->dropDownList($model->positions) ?>
<?= $form->field($model, 'position_input') ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences) ?>
