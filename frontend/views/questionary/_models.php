<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */

$this->registerJSFile('@web/js/questionary/models.js', ['depends' => 'yii\web\JqueryAsset']);

?>
<div class="questionary-_models">
        <?= $form->field($model, 'age')->hiddenInput(['id' => 'age_input'])->label(false) ?>
<!--   Пока не работает     --><?//= $form->field($model, 'metrical')->radioList($model->metricSys) ?>
        <?= $form->field($model, 'height') ?>
        <?= $form->field($model, 'weight') ?>
        <?= $form->field($model, 'waist') ?>
        <?= $form->field($model, 'hips') ?>
        <?= $form->field($model, 'chest') ?>
        <?= $form->field($model, 'dress') ?>
        <?= $form->field($model, 'shoe') ?>
        <?= $form->field($model, 'cup', [
            'options' => [
                'id' => 'cup',
                ]
        ])->dropDownList($model->cups) ?>
        <?= $form->field($model, 'gender')->hiddenInput([
                'id' => 'gender_model'
        ])->label(false) ?>

        <?= $form->field($model, 'ethnicity')->dropDownList($model->ethnicityList) ?>

        <?= $form->field($model, 'ethnicity_input', [
                'options' => [
                    'style' => 'display:none',
                    'id' => 'ethnicity_input'
                ]
        ])->textInput() ?>

        <?= $form->field($model, 'skin_color')->dropDownList($model->skins)->hint(Yii::t('app', 'Укажите цвет Вашей кожи')) ?>
        <?= $form->field($model, 'eye_color')->dropDownList($model->eyes)->hint(Yii::t('app', 'Укажите цвет Ваших глаз')) ?>
        <?= $form->field($model, 'natural_hair_color')->dropDownList($model->hairs)->hint(Yii::t('app', 'Укажите естественный цвет Ваших волос')) ?>
        <?= $form->field($model, 'hair_coloration')->dropDownList($model->hairColorations)->hint(Yii::t('app', 'У Вас окрашены волосы? В какой цвет?')) ?>
        <?= $form->field($model, 'readness_to_change_color')->dropDownList($model->readnessList)->hint(Yii::t('app', 'Готовы ли Вы сменить цвет волос, если это потребуется для работы?')) ?>
        <?= $form->field($model, 'hair_lenght')->dropDownList($model->hairLenghts)->hint(Yii::t('app', 'Какой длинны Ваши волосы')) ?>
        <?= $form->field($model, 'has_tatoo', [
            'options' => ['id' => 'has_tatoos'],
                'inputOptions' => ['class' => 'custom-radio']
        ])->radioList($model->hasTatoos)->hint(Yii::t('app', 'Имеются ли на Вашем теле татуировки?')) ?>
        <?= $form->field($model, 'tatoos',[
            'options' => [
                'style' => 'display:none',
                'id' => 'tatoos'
            ]
        ])->checkBoxList($model->tatoo) ?>
        <?= $form->field($model, 'has_piercing', [
            'options' => ['id' => 'has_piercing']
        ])->radioList($model->hasPiercing)->hint(Yii::t('app', 'Укажите имеется ли у Вас пирсинг не считая сережек в ушах?')) ?>
        <?= $form->field($model, 'piercing', [
            'options' => [
                'style' => 'display:none',
                'id' => 'piercing'
            ]
        ])->checkBoxList($model->piercings) ?>


        <h3>Профессиональные детали</h3>
        <?= $form->field($model, 'model_category')->checkBoxList($model->categories)->hint(Yii::t('app', 'Укажите категории в которых вы работали')) ?>
        <?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences)->hint(Yii::t('app', 'Укажите Ваш опыт работы в данной сфере')) ?>
        <?= $form->field($model, 'additional_skills')->checkBoxList($model->additionalSkills)->hint(Yii::t('app', 'Укажите какими дополнительными навыками Вы владеете')) ?>
        <?= $form->field($model, 'compensation')->checkBoxList($model->compensations)->hint(Yii::t('app', 'Какой вид компенсации Вы хотите получать за свою работы?')) ?>

        <p class="formSubHead">Rates for photoshoot</p>
        <?= $form->field($model, 'rates_for_photoshoot_hour')->hint(Yii::t('app', 'Укажите стоимость одного часа Вашей работы')) ?>
        <?= $form->field($model, 'rates_for_photoshoot_half')->hint(Yii::t('app', 'Сколько будет стоить Ваша работа, если придется работать половину дня (4 часа)')) ?>
        <?= $form->field($model, 'rates_for_photoshoot_full')->hint(Yii::t('app', 'Стоимость Вашей работы за полный день (8 часов)')) ?>
        <?= $form->field($model, 'rates_for_runway')->textarea()->hint(Yii::t('app', 'На какую сумму Вы рассчитываете за выход на подиум?')) ?>




</div><!-- questionary-_models -->
