<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\questionary\forms\PhotographerForm */
/* @var $form ActiveForm */
?>
<div class="questionary-_photographer">

        <?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences)->hint(Yii::t('app', 'Укажите Ваш опыт работы в данной сфере')) ?>
        <?= $form->field($model, 'work_areas')->checkBoxList($model->areas)->hint(Yii::t('app', 'Уажите в какой специализации Вы работаете')) ?>
        <?= $form->field($model, 'work_type')->checkBoxList($model->types)->hint(Yii::t('app', 'Укажите тип Вашей работы')) ?>
        <?= $form->field($model, 'equipment')->textarea()->hint(Yii::t('app', 'Укажите какой тип оборудования у Вас имеется')) ?>
        <?= $form->field($model, 'studio')->dropDownList($model->provides)->hint(Yii::t('app', 'Имеется ли у Вас своя студия?')) ?>
        <?= $form->field($model, 'makeup_artist')->dropDownList($model->provides)->hint(Yii::t('app', 'Имеется ли у Вас визажист?')) ?>
        <?= $form->field($model, 'hair_stylist')->dropDownList($model->provides)->hint(Yii::t('app', 'Имеется ли у Вас стилист?')) ?>
        <?= $form->field($model, 'compensation')->checkBoxList($model->compensations)->hint(Yii::t('app', 'Какой вид компенсации Вы хотите получать за свою работы?')) ?>
        <?= $form->field($model, 'rates_for_photoshoot_hour')->hint(Yii::t('app', 'Укажите стоимость одного часа Вашей работы')) ?>
        <?= $form->field($model, 'rates_for_photoshoot_half')->hint(Yii::t('app', 'Сколько будет стоить Ваша работа, если придется работать половину дня (4 часа)')) ?>
        <?= $form->field($model, 'rates_for_photoshoot_full')->hint(Yii::t('app', 'Стоимость Вашей работы за полный день (8 часов)')) ?>


</div><!-- questionary-_photographer -->
