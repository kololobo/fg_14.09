<?php

use yii\widgets\ActiveForm;
?>

<?= $form->field($model, 'position')->dropDownList($model->positions)->hint(Yii::t('app', 'Укажите занимаемую должность')) ?>
<?= $form->field($model, 'scope_of_activity')->textarea()->hint(Yii::t('app', 'Опишите чем занимается Ваша компания')) ?>
<?= $form->field($model, 'workplace')->textarea()->hint(Yii::t('app', 'Рабочее место')) ?>
<?= $form->field($model, 'professional_experience')->dropDownList($model->professionalExperiences)->hint(Yii::t('app', 'Укажите Ваш опыт работы в данной сфере')) ?>
