<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 15.09.2016
 * Time: 10:05
 */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\forms\ProfileUserForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use yii\widgets\MaskedInput;
use phpnt\bootstrapSelect\BootstrapSelectAsset;
use common\models\forms\TypeUserForm;
use yii\widgets\Pjax;
?>
<?php

Pjax::begin([
    'id' => 'pjaxBlock',
    'enablePushState' => false,
]);
BootstrapSelectAsset::register($this);
//ICheckAsset::register($this);
?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['/site/signup']),
     'options' => ['data-pjax' => true],
    'id' => 'form']) ?>
    <!--'id' => 'formSignup',]) ?> -->

<?php
$model->tariff_name = $model->scenario;

?>
    <div>
        <?= $form->field($model, 'tariff_name')
            ->radioList(
                $model->tariffesList,
                [
//                    'checked' => 'visitor',
                    'onchange' => '
                    $.pjax({
                        type: "POST",
                        url: "'.Url::to(['/site/set-scenario']).'",
                        data: jQuery("#form").serialize(),
                        container: "#pjaxBlock",
                        push: false,
                        scrollTo: false
                    });
                    ',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $check = $checked ? ' checked="checked"' : '';
                        return "<label class=\"custom-radio\"><input type=\"radio\" name=\"$name\" value=\"$value\"$check><span><div></div></span><p> $label</p></label>";
                    }
                ]) ?>

        <?php
//        $this->registerJs('
//        $("#userform-tariff_name input").on("ifChecked", function(event){
//          $.pjax({
//                type: "POST",
//                url: "'.Url::to(['/site/set-scenario']).'",
//                data: jQuery("#formSignup").serialize(),
//                container: "#pjaxBlockSignup",
//                push: false,
//                scrollTo: false
//            });
//    });');
        ?>

    </div>
    <div class="clearfix"></div>
    <div class="col-md-6">
        <?= $form->field($model, 'first_name')->textInput(['placeholder' => Yii::t('app', 'Имя')]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'last_name')->textInput(['placeholder' => Yii::t('app', 'Фамилия')]) ?>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'E-mail')]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'phone')->textInput()->widget(MaskedInput::className(),[
        'name' => 'phone',
        'mask' => '+999999999999']) ?>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-6">
        <?= $form->field($model, 'lang')->dropDownList($model->langsList, [
            'class' => 'form-sel',
            'prompt' => Yii::t('app', 'Язык'),
            ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'city_id')->dropDownList($model->cityList, [
            'class' => 'form-sel',
            'prompt' => Yii::t('app', 'Страна'),
        ]) ?>
    </div>
    <div class="clearfix"></div>

<?php
if ($model->scenario == 'classic' || $model->scenario == 'vip'):
    ?>

    <div class="col-md-6">
        <?= $form->field($model, 'profession_num')->dropDownList($model->numberOfProfession, [
            'class' => 'form-sel',
            'data'  => [
                'style' => 'btn-default btn-dropdown-default',
                'size' => 7,
                'title' => Yii::t('app', 'Количество профессий'),
            ]]) ?>
    </div>

    <div class="col-md-6">

        <?= $form->field($model, 'type_user', [

        ])->dropDownList(TypeUserForm::getUserTypeList(), [
               'class' => 'form-sel',
                'id' => 'type_user',
                'prompt' => Yii::t('app', 'Тип пользователя'),
                'onchange' => '
                        ProfExpert("#type_user",'. TypeUserForm::EXPERT .' );
                        ',
                ]) ?>

        <?= $form->field($model, 'type_expert', [
            'options' => [
                'id' => 'type_expert',
                'style' => 'display:none',
            ]
        ])->dropDownList(TypeUserForm::getExpertList(), [
            'class'     => 'form-control selectpicker ',
            'multiple'  => true,
            'data' => [
                'style'         => 'btn-primary btn-dropdown-default',
                'size'          => 7,
                'max-options'   => 3,
                'title'         => Yii::t('app', 'Профессия'),
            ]]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'description', [
            'template' => '<div class="row">
                            <div class="col-md-12">{label}</div>
                            <div class="col-md-12 form-pay">
                            <p>Description</p>{input}</div>
                            <div class="col-md-12"><i>{error}</i></div>
                            </div>'])->textarea(['rows' => 4, 'style' => 'text-align: justify; resize: vertical;']); ?>
    </div>
    <?php
elseif ($model->scenario == 'corporate'):
    ?>
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Название компании')]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'type_company', [
            'template' => '{input}'
        ])->hiddenInput(['value' => \common\models\forms\TypeCompanyForm::getOneType()]) ?>
    </div>
    <?php
endif;
?>

<?= $form->field($model, 'scenario', ['template' => '{input}'])->hiddenInput(['value' => $model->model_scenario])->label(false) ?>
<!---->
<?= Html::hiddenInput('model', 'common\models\forms\UserForm') ?>
<?= Html::hiddenInput('scenario', $model->model_scenario) ?>
<?= Html::hiddenInput('form', '@frontend/views/site/_form-signup') ?>

    <div class="clearfix"></div>
    <div class="form-group" style="margin-top: 40px;">
        <?= Html::submitButton(Yii::t('app', 'Регистрация'), ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php
Pjax::end();
?>