<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 14.09.2016
 * Time: 10:25
 */
/* @var $model \common\models\forms\LoginForm */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
//use common\widgets\ICheck\ICheckAsset;
use yii\helpers\Url;

//ICheckAsset::register($this);
?>
<?php $form = ActiveForm::begin(['action' => Url::to(['/site/login'])]); ?>

<div class="col-xs-12">
    <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Введите емайл')])->label(false) ?>
</div>

<div class="col-xs-12">
    <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Пароль')])->label(false) ?>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-sm-6 text-left" style="padding-left: 0;">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => '<div class="col-xs-12">{input} {label}</div>'
            ]) ?>
        </div>
        <div class="col-sm-6 text-right">
            <?= Html::a(Yii::t('app', 'Забыли пароль?'), ['site/request-password-reset'], ['class' => 'link-default']) ?>
        </div>
    </div>
</div>

<div class="form-group" style="margin-top: 20px;">
    <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
</div>

<?php ActiveForm::end(); ?>
