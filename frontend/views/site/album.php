<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Альбом'); ?>

<div class="person-wrapper">
	<div class="album">
		<a href="#add" class="album-add">Добавить</a>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
			<button>Удалить</button>
		</div>
		<div class="pageScroll">
			<p class="scrollLeft"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></p>
			<p class="scrollRight"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
		</div>
	</div>
<!-- ВЫВОД РЕЗУЛЬТАТОВ ПОИСКА ПО ФОТО КОНЕЦ ---------------------------------------------------------------------------------------------->
</div>
<!-- Модальное окно для фото/видео -->
<div id="openModal" class="modal modal-ph">
	<div class="modalWrapper">
		<a href="#close">X</a>
		<img src="http://img.mota.ru/upload/wallpapers/2016/07/26/13/00/49329/mota.ru_2016072621-preview.jpg">
		<div class="modal-btn"><!-- 4 кнопки: фотограф, хозяин фото, модель, альбом в которам размещена фотография -->
			<a href="#photorrapher"></a>
			<a href="#profile"></a>
			<a href="#model"></a>
			<a href="#album"></a>
		</div>
		<p></p><!-- рейтинг фото -->
	</div>
</div>
<div id="add" class="modal modal-ph">
	<div class="modalWrapper">
		<a href="#close">X</a>
		<div class="min-phot">
			<img src="http://img.mota.ru/upload/wallpapers/2016/07/26/13/00/49329/mota.ru_2016072621-preview.jpg">
		</div>
		<form>
			<input type="" name="PhotographerAdd">
		</form>
		<div class="modal-btn"><!-- 4 кнопки: фотограф, хозяин фото, модель, альбом в которам размещена фотография -->
			<a href="#photorrapher"></a>
			<a href="#profile"></a>
			<a href="#model"></a>
			<a href="#album"></a>
		</div>
		<p></p><!-- рейтинг фото -->
	</div>
</div>