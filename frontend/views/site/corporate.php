<br><br><br><br><br>
<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Корпоративный'); ?>

My Page - corporate



< ? /*
Путь самого widget = /public_html/fashingreatness.ru/protected/components
Путь HTML = /public_html/fashingreatness.ru/protected/components/views
Вызовы widget надо поставить в то место где должен выдаваться HTML widget-a
Ниже приведены вызовы widget
array( 'items'=>array("cod"=>"visitor") ) - это таблица параметров widget
*/ ?>

<div class="person-wrapper">
	<div class="c-left-col">
		<div class="vis-info">
			<div class="p-r-nr">
				<p>< ?=$this->tuser['prof']?></p>											< ? /* Профессия */ ?><!-- не отображается -->
				<p class="name id">< ?=$this->tuser['name']?>/< ?=$this->tuser['id']?></p>	< ? /* имя видно одим, а id другим типам аккаунта, а не сразу всем */ ?>
				<br/>
				<p>< ?=$this->arr_site["ratingp"]?> < ?=$this->tuser['reyting']?></p>			< ? /* Рейтинг в % в виде батарейки - макс = 100% */ ?><!-- на деле оба рейтинга в %, а точнее не рейтиг, а только слова и профессии не видать -->
				<p>< ?=$this->arr_site["ratingp"]?> < ?=$this->tuser['reyting_nom']?></p>		< ? /* Позиция в рейтинге например 399 из 399 */ ?>
				<p class="acc-type">< ?=$this->arr_site["account_type"]?> <span>< ?=Yii::app()->user->getState('package')?></span></p>	< ? /* Тип аккаунта */?>
				<!--<p class="acc-id">< ?=$this->tuser['id']?></p>			-->					< ? /* ID user */ ?><!-- Зачем здесь ID? -->
				<div class="acc-info-icons">
					<p class="like-icon"></p>
					<p class="dial-icon"></p>
				</div>
				<div class="acc-ph">
					<div class="acc-ph-edit">
					<a class="" href="" id="avatar_edit">< ?=$this->arr_site["edit"]?></a>			< ? /* Edit */ ?>
					<a class="" href="" id="avatar_remove">< ?=$this->arr_site["remove"]?></a>		< ? /* Remove */ ?>
					</div>
					<img src="./img_avatar_big/< ?=$this->tuser['avatar']?>" alt="Фотография профиля" id="avatar_img">
				</div>
			</div>


			<div class="acc-info">
				<nav class="block-btn">
					<div class="btn-item">
						<a href="/search">< ?=$this->arr_site["search"]?></a>			< ? /* Поиск */ ?>
						<div class="btn-subitem">
							<a href="/searchevents">< ?=$this->arr_site["event"]?></a>		< ? /* События */ ?>
							<a href="/searchpeople">< ?=$this->arr_site["people"]?></a>		< ? /* Люди */ ?>
							<a href="/searchjob">< ?=$this->arr_site["job"]?></a>			< ? /* Работа */ ?>
						</div>
					</div>
					<div class="btn-item">
						<a href="">< ?=$this->arr_site["subscription"]?></a>		< ? /* Подписки */ ?>
					</div>
					<div class="btn-item">
						<a href="">< ?=$this->arr_site["recommendations"]?></a>	< ? /* Рекомендации */ ?>
					</div>
					<div class="btn-item">
						<a href="">< ?=$this->arr_site["vacancies"]?></a>		< ? /* Вакансии */ ?>
					</div>
				</nav>
				< ? /* Показывать только для чужих анкет */ ?>
				<div class="acc-gen-info" style="display:none">
					< ? $this->Widget('WidGeneral', array( 'items'=>array("cod"=>"visitor") ) ); ?>
				</div>
				<div class="acc-phys-det" style="display:none">
					< ? $this->Widget('WidPhysical', array( 'items'=>array("cod"=>"visitor") ) ); ?>
				</div>
				<div class="acc-prof-det" style="display:none">
					< ? $this->Widget('WidProfessional', array( 'items'=>array("cod"=>"visitor") ) ); ?>
				</div>

			</div>
		</div>
	</div>
	<div class="c-main">
		<!--div class="c-m-nav">
			<a href="">< ?=$this->arr_site["calendar"]?></a>		< ? /* Календарь */ ?>
			<a href="">< ?=$this->arr_site["journal"]?></a>		< ? /* Журнал */ ?>
			<a href="">< ?=$this->arr_site["event"]?></a>		< ? /* События */ ?>
		</div-->

		<!--
		<hr>
		ОТЛАДКА
		<a href="index.php?r=sitevisitor/index&id_viewuser=14">Китай</a>
		<a href="index.php?r=sitevisitor/index&id_viewuser=16">Франция</a>
		<a href="index.php?r=sitevisitor/index&id_viewuser=15">Англия</a>
		<hr>
		-->

		<div class="c-m-widgets">
			<div class="c-m-calendar">
				< ? $this->Widget('WidCalendar', array( 'items'=>array("cod"=>"visitor") ) ); ?>
			</div>

			<div class="c-m-jobs">
				< ? $this->Widget('WidFeaturesJobs', array( 'items'=>array("cod"=>"visitor") ) ); ?>
			</div>

			<div class="c-m-news">
				< ? $this->Widget('WidLatestNews', array( 'items'=>array("cod"=>"visitor") ) ); ?>
			</div>

			<div class="m-jobs">
				< ? $this->Widget('WidMyJobs', array( 'items'=>array("cod"=>"visitor") ) ); ?>
			</div>

			<div class="c-m-event" style="display:none;">
				< ? $this->Widget('WidEvent', array( 'items'=>array("cod"=>"visitor") ) ); ?>
			</div>
		</div>
	</div>
</div>


<div class="avatar" id="avatar_e" style="display:none;">
<a href="" id="avatar_edit_v1"> 1. Выбрать на сервере </a>
<a href="" id="avatar_edit_v2"> 2. Загрузить из своего компа </a>
</div>

<div class="avatar" id="avatar_e1" style="display:none;">
Выбрать аватар на сервере
матрица аватор из папки img_avatar_small
< ? $this->Widget('WidAvatar'); ?>
</div>


<div class="avatar" id="avatar_e2" style="display:none;">
Загрузить аватар из своего компа
<form method="POST" enctype="multipart/form-data" action="" id="form_zag_fil" >
<input type="hidden" name="id" value="< ?=$this->tuser['id']?>">
<input type="file" id="fil_1" name="img" class="button-img11111">
<input type="submit" id="sub_zag_fil" value="Загрузить файлы" class="button-zag111">
<p id="fil_aaa"></p>

</div>



<script>
$(document).ready(function() {
	// Переменная куда будут располагаться данные файлов
	var files;
	$('input[type=file]').change(function(){
		files = this.files;
		id = this.id;
		aaa = "";
		for (i=0; i<files.length;++i) {
			//aaa += "<tr><td>" + files.item(i).name + "=" + files.item(i).size + "</td></tr>";
			aaa += "" + files.item(i).name + "=" + files.item(i).size + "<hr>";
		};
		$("#fil_aaa").append(aaa);
	});
	$('#form_zag_fil').on('submit', ( function(e) {
		e.preventDefault();
		var data = new FormData(this);
		//
		$.ajax({
			type: 'post',
			url: 'index.php?r=SiteAjax/Myavatar',
			processData: false,
			contentType: false,
			data: data,
			dataType: 'json',
			success: function(json){
				console.log(json);
				console.log(files);
				$("#avatar_img").attr('src', '../img_avatar_big/'+json['name']);
				$("#avatar_img_small").attr('src', '../img_avatar_small/'+json['name']);
				files = "";
				data  = "";
				//$("fil_0").val("");
				return false;
			},
			error: function(error){
				console.log("error");
				console.log(files);
				console.log(files[0].name);
				return false;
			}
		});
		//
    }));
	< ? /* Заменить аватар - заменить в таб.users - Варианты :  */ ?>
	< ? /* 1. из папки img_avatar_small */ ?>
	< ? /* 2. загрузить из своего компа */ ?>
	$('#avatar_edit').click( function() {
		$('#avatar_e').css('display', '');
		return false;
	});
	< ? /* 1. из папки img_avatar_small */ ?>
	$('#avatar_edit_v1').click( function() {
		$('#avatar_e').css('display', 'none');
		$('#avatar_e1').css('display', '');
		return false;
	});
	< ? /* 2. загрузить из своего компа */ ?>
	$('#avatar_edit_v2').click( function() {
		$('#avatar_e').css('display', 'none');
		$('#avatar_e2').css('display', '');

		return false;
	});
	< ? /* Удалить аватар - заменить в таб.users на фиксированное имя no_avatar.png */ ?>
	$('#avatar_remove').click( function() {

		return false;
	});
});
</script>
< ?
		/*
		aaa = $(this).attr('href');
		bbb = $('.active').attr('href');
		$('.nav-s-item').removeClass('active');
		$(this).addClass('active');
		$('.'+aaa).removeClass('hide');
		$('.'+bbb).addClass('hide');
		*/
?>