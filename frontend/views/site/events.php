<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'События'); ?>
<div class="person-wrapper">
	<div class="j-main">
		<h2>Events</h2>
		<span class="hr"></span>
		<div class="nav-bar">
			<ul class="nav2">
				<li class="active">
					<a href="#fe">FG Events</a>
				</li>
				<li>
					<a href="#me">Members Events</a>
				</li>
			</ul>
		</div>
		<div class="j-section">
			<div class="fe">
				<ul class="nav-fe">
					<li>
						<a href="">All</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Events</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Tenders</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Games&Competitions</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Others</a>
					</li>
				</ul>
			</div>
			<div class="me" style="display: none;">
				<ul class="nav-me">
					<li>
						<a href="">All</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Events</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Tenders</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Games&Competitions</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Others</a>
					</li>
					<li>
						|
					</li>
					<li>
						<a href="">Publish your event</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="j-sidebar">
		<div class="j-t-port">
			<div class="j-t-head">
				Top profile
			</div>
			<div class="j-port-cont">
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
		<div class="j-t-phot">
			<div class="j-t-head">
				Top photo
			</div>
			<div class="j-phot-cont">
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
	</div>
</div>