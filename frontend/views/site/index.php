<?php
/* @var $this yii\web\View */

use phpnt\bootstrapNotify\BootstrapNotify;
use common\widgets\SignupAndLogin\SignupAndLogin;
use frontend\assets\OwlCarousel;
use frontend\assets\IndexAsset;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

OwlCarousel::register($this);
IndexAsset::register($this);
$this->title = 'Fashion Greatness';
?>
<?= BootstrapNotify::widget() ?>

<div id="insert">
<a name="b1" id="b1"></a>
<article class="block b1" id="home" style="display:block;">
	<div class="border1">
		<div class="subh">
			<?= Yii::t('app', 'САМОЕ БОЛЬШОЕ В МИРЕ СООБЩЕСТВО') ?><!--Самый большой выбор в мире -->
		</div>
		<div class="h">
			<?= Yii::t('app', 'МОДЕЛЕЙ, МОДЕЛЬНЫХ АГЕНТСТВ, ФОТОГРАФОВ, ЭКСПЕРТОВ МОДЫ') ?>
		</div>
		<div class="subh">
			<?= Yii::t('app', 'И ДРУГИХ ПРОФЕССИОНАЛОВ ИНДУСТРИИ МОДЫ') ?>
		</div>
		<div class="menu2">
			<?= SignupAndLogin::widget() ?>
			<!--button data-toggle="modal" data-target="#lay-hid_join" class="btn btn-primary menu2e">РЕГИСТРАЦИЯ</button>
			<button data-toggle="modal" data-target="#lay-hid_signin" class="btn btn-primary menu2e">АВТОРИЗАЦИЯ</button-->
		</div>
	</div>
</article>
<a name="b2" id="b2"></a>
<article class="block b2" id="about">
	<div class="about">
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/about"><i class="fa fa-4x fa-info-circle" aria-hidden="true"></i>
					<br/><?= Yii::t('app', 'о нас' ) ?></a>
				</div>
			</div>
		</div>

		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/services"><i class="fa fa-4x fa-cogs" aria-hidden="true"></i>
					<br/><?= Yii::t('app', 'Наши услуги') ?></a>
				</div>
			</div>
		</div>

		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/mentors"><i class="fa fa-4x fa-users" aria-hidden="true"></i>
					<br/><?= Yii::t('app', 'наши наставники') ?></a>
				</div>
			</div>
		</div>

		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/faq"><i class="fa fa-4x fa-question-circle" aria-hidden="true"></i>
					<br/>вопросы-ответы</a>
				</div>
			</div>
		</div>

		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/contacts"><i class="fa fa-4x fa-envelope-o" aria-hidden="true"></i>
					<br/>контакты</a>
				</div>
			</div>
		</div>

		<h1>ДУМАЙТЕ ШИРЕ. ДУМАЙТЕ В МЕЖДУНАРОДНОМ МАСШТАБЕ. МЕЧТАЙТЕ.</h1>
		<div class="ln"></div>
	</div>
</article>
<a name="b3" id="b3"></a>
<article class="block b3" id="features">
	<div class="features">
		<div class="wrap-f">
			<div class="f-1-2">
				<div class="carousel-pagination">
					<ul class="list-pagination">
						<li class="c-p current">
							<a class="slid" id="s0" sme="0" href="#s1">МОДЕЛИ</a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-1" sme="-1" href="#s2">фотографы</a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-2" sme="-2" href="#s3">Модельные агентства</a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-3" sme="-3" href="#s4">Эксперты моды</a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-4" sme="-4" href="#s5">Другие профессионалы</a>
						</li>
					</ul>
				</div>
				<div class="carousel">
					<div class="car-wrap">
						<div class="f-2-3">
							<div id="ulsl" onmousedown="return false" onselectstart="return false">
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f1.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модели могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, модели могут находить предложения о работе, искать фотографов, устанавливать новые контакты, участвовать в конкурсах, кастингах, неделях моды, шоу-румах, иметь доступ к VIP-мероприятиям закрытого типа по всему миру, подписываться на/ проводить мастер-классы; отслеживать новости мира моды и узнавать о реалиях модельного бизнеса, загружать фотографии в лучшем качестве и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f2.jpg">
										</div>
										Добро пожаловать в моде greatness.com, многофункциональной всемирной сети для лучших профессионалов в области индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов в этой сфере. Таким образом, фотографы могут использовать такую уникальную услугу в качестве персонального менеджера по связям с общественностью, который поможет им продвигать свой профиль на нашем сайте. Кроме того, фотографы могут продать свои услуги; поиск клиентов; подписаться на / проводить мастер-классы; обмен опытом с другими людьми; знак субподряда; поиск моделей; загрузить фотографии в лучшем качестве; участвовать в конкурсах; имеют доступ к эксклюзивным VIP-мероприятий по всему миру закрытого типа и многие другие. Это лишь малая часть преимуществ, которые вы можете предпринять использования нашей сети. Мы подготовили много приятных сюрпризов для вас. Будь готов!</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f3.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модельные агентства могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать их моделей на нашем сайте; регистрацию корпоративного аккаунта с определённым количеством профилей для моделей, получение информации о совершённых их моделями сделках на сайте, заполнение календаря занятости. К тому же мы всегда готовы обсудить дополнительные уникальные возможности, предоставляемые вашей компании. Помимо всего прочего, модельные агентства смогут проводить мастер-классы; искать новых моделей; подписывать контракты по субподряду (искать фотографов, визажистов и других профессионалов); использовать огромное количество инструментов по самопродвижению, PR услуг; иметь доступ к VIP-мероприятиям закрытого типа по всему миру; устанавливать новые контакты и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f4.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, эксперты моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, эксперты моды могут искать новых клиентов, партнёров; подписывать контракты; использовать огромное количество инструментов по самопродвижению и PR услуг для того, чтобы представлять свои интересы, торговую марку или услуги; сотрудничать с другими профессионалами индустрии моды и участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f5.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, профессионалы индустрии моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, профессионалы индустрии моды могут продавать свои услуги; искать новых клиентов; получать самую свежие новости из мира моды и практическую информацию о развитии карьеры; подписываться на/ проводить мастер-классы; участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="f-2-2">
				<p class="have">У нас есть</p>
				<div class="have-list">
					<p>На сайте уже зарегистрированы</p>
					<p>МОДЕЛИ</p>
					<hr style="width:30%;">
					<p>фотографы</p>
					<hr style="width:50%;">
					<p>Модельные агентства</p>
					<hr style="width:20%;">
					<p>Эксперты моды</p>
					<hr style="width:80%;">
				</div>
			</div>

			<div class="clear"></div>

			<div class="video-wrapper">
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/iGaF4tKUl0o" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/sfbfaeG7EJU" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="clear"></div>
			</div>

			<div class="clear"></div>

		</div>
	</div>
</article>
<a name="b4" id="b4"></a>
<article class="block b4" id="team">
	<div class="border2">
		<div class="yln"></div>
		<img class="t-i" src="../img/creative-team.png">
		<p>МЫ — ПРОФЕССИОНАЛЫ СВОЕГО ДЕЛА С МНОГОЛЕТНИМ ОПЫТОМ В СФЕРЕ ВЫСОКОЙ МОДЫ..</p>
		<div class="carousel ">
			<div class="arrow l-arrow">
				<img src="../images/prev.png" class='owl-prev' id="but_team_next">
			</div>
			<div class="team-carousel owl-theme car-wrap" id="team_slid">

				<?php $i = 0;
				foreach ($ourTeam as $member):
					$i++;?>
				<div class="team-item" id="team_<?= $i ?>">
					<!-- <p class="team-name">< ?= Yii::t('content', $member->name) ?> -->
						<br>
						<!-- <span>< ?= Yii::t('content', $member->profession )?></span></p> -->
					<div class="team-inner">
						<div class="team-img">
							<?php if ($member->photo()): ?>
								<!-- <img src="../images/userpics/<?= $member->photo()->file_small ?>" alt="< ?= Yii::t('content', $member->name) ?>" width='120px' > -->
							<?php endif ?>
						</div>
						<div class="team-description">
							<!-- < ?= Yii::t('content', $member->citate) ?> -->
						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:<?= $member->email ?>"><?= $member->email ?></a>
						</div>
					</div>
				</div>
				<?php endforeach ?>

			</div>
			<div class="arrow r-arrow">
				<img src="../images/next.png" class='owl-next' id="but_team_prev">
			</div>
			<div class="clear"></div>
		</div>
	</div>
</article>
<a name="b5" id="b5"></a>
<article class="block b5" id="prices">
	<div class="price">
		<p class="p-h">ЦЕНОВЫЕ ПАКЕТЫ</p>
		<p class="p-sh">КАЖДЫЙ ЦЕНОВОЙ ПАКЕТ ПРЕДОСТАВЛЯЕТ РАЗНЫЕ ВОЗМОЖНОСТИ. ВЫБЕРИТЕ ТОТ, ЧТО ПОДХОДИТ ИМЕННО ВАМ.</p>
		<div class="price-container">
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
						АККАУНТ
						<br>
						<span class="price-name">КЛАССИЧЕСКИЙ</span>
						<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>26$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">3 ИЗДАНИЯ ВАКАНСИЙ<br>20 СОВМЕСТНАЯ ПРЕДЛОЖЕНИЯ<br>НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>ОГРАНИЧЕНОЙ в профайле<br>LIMITED ИЗГОТОВЛЕНИЕ<br></p>
						<a href="#popup-register" data-tarif="5" class="price-btn" id="join1">РЕГИСТРАЦИЯ</a>
						<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ
					<br>
					<span class="price-name">ВИП</span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>260$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>До 4-х очерков В ГОД<br>3 участников событий различного формата В ГОД<br>20 ИЗДАНИЯ ВАКАНСИЙ<br>Неограниченные предложения о сотрудничестве<br>НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>ПОЛНЫЙ возможности настройки<br></p>
					<a href="#popup-register" data-tarif="6" class="price-btn" id="join2">РЕГИСТРАЦИЯ</a>
					<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ
					<br>
					<span class="price-name">КОРПОРАТИВНЫЙ</span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>360$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>До 4-х очерков В ГОД<br>3 участников событий различного формата В ГОД<br>20 ИЗДАНИЯ ВАКАНСИЙ<br>Неограниченные предложения о сотрудничестве<br>НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>ПОЛНЫЙ возможности настройки<br>20 ДОПОЛНИТЕЛЬНЫЕ CLASSIC ПРОФИЛИ для сотрудников Вашей компании<br></p>
					<a href="#popup-register" data-tarif="7" class="price-btn" id="join3">РЕГИСТРАЦИЯ</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</article>
<a name="b6" id="b6"></a>
<article class="block b6" id="contactus">
	<img class="c-img" src="img/contactus.png" alt="Обратная связь" />

		<?php Pjax::begin([
			    'enablePushState' => false,
			]);?>
		<div class="contact">
		<?php $form = ActiveForm::begin([
		    'action' => Url::to(['/site/contact']),
		    'options' => [
		    	'data-pjax' => true,
		    	'class' => 'c-f',
		    	],
		    'fieldConfig' =>  [
		            'template' => "{input}\n{error}",
			        'inputOptions' => ['class' => 'form-control'],
			        'errorOptions' => ['class' => ''],
			        'options' => ['class' => 'c-f-i'],
			        ],
		    'id' => 'formSignup',
		    'errorCssClass' => '',
		    'enableAjaxValidation' => true]) ?>

        <?= $form->field($contactForm, 'name')->textInput(['placeholder' => Yii::t('app', 'Имя') . ' *']) ?>

        <?= $form->field($contactForm, 'email')->textInput(['placeholder' => Yii::t('app', 'Email') . ' *']) ?>

        <?= $form->field($contactForm, 'category_question')->dropDownList($contactForm->categories(), [
            'class' => 'form-control custom-class_field1 custom-class_field2 f-c-s',
            'prompt' => Yii::t('app', 'Выберите категорию') . '*',
            ]) ?>

        <?= $form->field($contactForm, 'body')->textArea(['placeholder' => Yii::t('app', 'Сообщение') . ' *']) ?>

		<div class="c-f-i tc form-group">
        <?= Html::submitButton('<i class="fa fa2 fa-envelope-o"></i>&nbsp;&nbsp;&nbsp;' . Yii::t('app', 'Отправить'), ['class' => 'btn custom-class_button1 custom-class_button2']) ?>
        </div>

        <div class="c-f-i">
			 <?= Yii::$app->session->getFlash('contactForm'); ?>
		</div>

        <?php ActiveForm::end(); ?>
		</div>
        <?php Pjax::end(); ?>
	</div>
</article>
</div>
</div>
