<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $general common\models\questionary\General */
/* @var $profession common\models\questionary\forms\ */
/* @var $professions_expert array */
/* @var $expert common\models\questionary\forms\ExpertForm */
?>
<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Анкета'); ?>

<div class="col-md-offset-3 col-md-6 person-wrapper">
<?php $form = ActiveForm::begin([
	'fieldConfig' => [
		'inputOptions' => [
			'style' => 'color:black',

		],
	],
//    'action' => Url::to(['/site/questionary']),
]);
?>


	<?php
	if ($profession) {
		if ($profession->view == '_models') {
			$questionary_model = $profession;
		} else {
			$questionary_model = NULL;
		}
	} else {
		$questionary_model = NULL;
	}
	?>

	<h2>Основная часть</h2>
	<?= $this->render('../questionary/_general', [
		'general' => $general,
		'models' => $questionary_model,
		'form'	 => $form,
		]) ?>

	<?php if ($profession): ?>
	<h2><?= $profession->title ?></h2>
<!--		Профессиональные детали -->
	<?= $this->render('../questionary/'.$profession->view , [
			'form'	 => $form,
			'model' => $profession, 
			'professions_expert' => $professions_expert
		]) ?>
		
		<!--	Общие детали -->
		<?= $form->field($general, 'contracted_or_not')->dropDownList($general->contractedList)->hint(Yii::t('app', 'У Вас уже есть подписанный контракт с каким либо агентством?')) ?>
		<?= $form->field($general, 'travel_ability')->dropDownList($general->travelList)->hint(Yii::t('app', 'Готовы ли Вы к переезду?')) ?>
		<?= $form->field($profession, 'main_reason')->checkboxList($profession->reasons)->hint(Yii::t('app', 'Укажите для каких целей Вы планируете использовать наш сайт')); ?>

	<?php endif; ?>


	<div class="clearfix"></div>
	<div class="form-group" style="margin-top: 40px;">
		<?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
	</div>
<?php $form = ActiveForm::end(); ?>

</div>




