<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Поиск'); ?>

<?php
/*
+ там фильтр по профессии - вверху справа выпадающий список
+ выбрав профессию сменяется дефолтный блок и открывается перечень альбомов имеющихся в данной профессии...
+ эти миниатюрки фильтруют по альбомам - у альбомов фиксированные названия и в профиле сразу имеются, но пустые...
после выбора альбома открывается страничка на которой отображаются выбранные альбомы всех людей из выбранной профессии с фотографией альбома , которую для обложки альбома выбрали...
пустые альбомы в поиске не учавствуют...
*/
?>

<?= $this->render('_visitor_nav') ?>

<div class="search-nav visitor">
    <div class="s-n-wrapper">
        <div class="search-type">
            <h2>Model photos</h2><!-- Сменяемая надпись в зависимости от выбраной категории поиска -->
            <span class="hr"></span>
        </div>
        <div class="prod-result">
            <p>The serch producted 68 results</p><!-- Надпись о колличестве найденых фото -->
        </div>
        <div class="prof-sel"><!-- Выбрать где искать фото  -->
            <ul>
                <li class="parent" style="padding: 7px 25px;">Выберите категорию для поиска
                    <ul>
                        <li><a href="#">Модель</a></li>
                        <li><a href="#">Фотограф</a></li>
                        <li><a href="#">Дизайнер</a></li>
                        <li><a href="#">Агентство</a></li>
                        <li class="parent">Эксперт
                            <ul>
                                <li><a href="#">Brand</a></li>
                                        <li><a href="#">Company Representative</a></li>
                                <li><a href="#">Media</a></li>
                                <li><a href="#">Stylist</a></li>
                                <li><a href="#">Hair Stylist/Artist</a></li>
                                <li><a href="#">MUA</a></li>
                            </ul>
                        </li>
                    <ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="sbph">
    <h2>Choose album for searching photo</h2>
    <span class="hr"></span>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Editorials
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Runway
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Beauty
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Portraits/Celebrity
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Glamour/Nude
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Commercial/Advertising
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Street Fashion
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Creative Projects
        </div>
    </div>
    <div class="m-j-albums fa fa-camera">
        <img src="" alt="">
        <div class="caption">
            Spec Shoot
        </div>
    </div>
    <div class="m-j-albums fa fa-video-camera">
        <img src="" alt="">
        <div class="caption">
            Videos
        </div>
    </div>
</div>


	<div class="search-results" style="display: none;">
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="pageScroll">
			<p class="scrollLeft"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></p>
			<p class="scrollRight"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
		</div>
	</div>
<!-- ВЫВОД РЕЗУЛЬТАТОВ ПОИСКА ПО ФОТО КОНЕЦ ---------------------------------------------------------------------------------------------->
</div>
<!-- Модальное окно для фото/видео -->
<div id="openModal" class="modal modal-ph">
	<div class="modalWrapper">
		<a class="close" href="#">X</a>
		<img class="popupimg" src="http://img.mota.ru/upload/wallpapers/2016/07/26/13/00/49329/mota.ru_2016072621-preview.jpg">
		<div class="modal-btn"><!-- 4 кнопки: фотограф, хозяин фото, модель, альбом в которам размещена фотография -->
			<p>Нажмите чтобы перейти на страницу</p>
			<a class="" href="#">Фотограф</a>
			<a class="" href="#">Добавил</a>
			<a class="" href="#">Модель</a>
			<a class="popupalb" href="">Альбом</a>
		</div>
		<p></p><!-- рейтинг фото -->
	</div>
</div>
<!--JS-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".idopenModal").click(function(){
			var param1 = $(this).attr("popuphref");
			var param2 = $(this).children('img').attr("src");
			alert(param1);
			alert(param2);
			$("#openModal").attr("style","display:block;");
			$(".popupalb").attr("href",param1);
			$(".popupimg").attr("src",param2);
		});
		$(".close").click(function(){
			$("#openModal").attr("style","display:none;");
			$(".popupimg").attr("src","");
			$(".popupalb").attr("href","");
		});
	});
</script>
<!--JS-->