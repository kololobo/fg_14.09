<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Поиск людей'); ?>
<!-- searchpeople
<br>
свой/чужой < ?=Yii::app()->user->getState('sv_ch')?> чужой < ?=Yii::app()->user->getState('ch_user')?> свой < ?=Yii::app()->user->getState('id_user')?> <br-->
<style type="text/css">
	#insert > article {
		display: none;
	}
</style>
<div class="search-nav">
	<div class="prof-sel">
		<ul>
			<li class="parent" style="padding: 7px 25px;">Выберите категорию для поиска
				<ul>
					<li><a href="#">Модель</a></li>
					<li><a href="#">Фотограф</a></li>
					<li><a href="#">Дизайнер</a></li>
					<li><a href="#">Агентство</a></li>
					<li class="parent" style="padding: 10px 15px;">Эксперт
						<ul>
							<li><a href="#">Brand</a></li>
							<li><a href="#">Company Representative</a></li>
							<li><a href="#">Media</a></li>
							<li><a href="#">Stylist</a></li>
							<li><a href="#">пHair Stylist/Artist</a></li>
							<li><a href="#">MUA</a></li>
						</ul>
					</li>
				<ul>
			</li>
		</ul>
	</div>
	<div class="search-btn">
		<a href="sbc" rel="insert">Choose by criteria</a>
		<a href="sbph" rel="insert">Choose by photo</a>
	</div>
</div>
<div id="insert" class="person-wrapper">
	<article id="sdf" style="display: block;">
		Выберите способ поиска
	</article>
	<article id="sbc">
		<section class="s-left-col">
			<div class="crit-data model"><!-- Модель -->
				<h1>Model Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<select>
							<option>All</option>
							<option>Male</option>
							<option>Female</option>
						</select>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Age
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Height
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Weight
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Chest
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Waist
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Hips
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Dress/Jacket, EU
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Cup
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Shoe, EU
					</div>
					<div class="details-data">

					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Modeling category/Genre
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink2" href="javascript:void(2);" onclick="viewdiv('mydiv2');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv2" style="display:none;">
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Body Parts" id = 'cb10' onchange = 'showOrHide("cb10", "cat10");'/><span><div></div></span>Body Parts:</label>
							</div>
							<div class="mc-g-h" id="cat10">
									<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
									<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
									<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
									<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
									<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Print photo model" id = 'cb11' onchange = 'showOrHide("cb11", "cat11");'/><span><div></div></span>Print photo model:</label>
							</div>
							<div class="mc-g-h" id="cat11">
									<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
									<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Glamour model" id = 'cb12' onchange = 'showOrHide("cb12", "cat12");'/><span><div></div></span>Glamour model:</label>
							</div>
							<div class="mc-g-h" id="cat12">
									<label class="q-check"><input type="checkbox" name="Lingerie/Body model"><span><div></div></span>Lingerie/Body model</label>
									<label class="q-check"><input type="checkbox" name="Swimwear model"><span><div></div></span>Swimwear model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 1 year</option>
								<option>1-5 years</option>
								<option>More that 5 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Ethnicity
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink4" href="javascript:void(4);" onclick="viewdiv('mydiv4');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv4" style="display:none;">
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="African"><span><div></div></span>African</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Asian"><span><div></div></span>Asian</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="European"><span><div></div></span>European</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Hispanic"><span><div></div></span>Hispanic</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Indian"><span><div></div></span>Indian</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Latin American"><span><div></div></span>Latin American</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Mediterranean"><span><div></div></span>Mediterranean</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Near East"><span><div></div></span>Near East</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Scandinavian"><span><div></div></span>Scandinavian</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Mixed"><span><div></div></span>Mixed</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Skin color
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink5" href="javascript:void(5);" onclick="viewdiv('mydiv5');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv5" style="display:none;">
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Pale white"><span><div></div></span>Pale white</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Olive"><span><div></div></span>Olive</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Light brown"><span><div></div></span>Light brown</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Brown"><span><div></div></span>Brown</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Dark brown"><span><div></div></span>Dark brown</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Black"><span><div></div></span>Black</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Eye color
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink6" href="javascript:void(6);" onclick="viewdiv('mydiv6');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv6" style="display:none;">
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Black"><span><div></div></span>Black</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Blue"><span><div></div></span>Blue</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Green"><span><div></div></span>Green</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Hazel"><span><div></div></span>Hazel</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Hair langth
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink7" href="javascript:void(7);" onclick="viewdiv('mydiv7');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv7" style="display:none;">
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Bald"><span><div></div></span>Bald</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Short"><span><div></div></span>Short</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Medium"><span><div></div></span>Medium</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Shoulder length"><span><div></div></span>Shoulder length</label>
							</div>
							<div class="mc-g-m">
								<label class="q-check"><input type="checkbox" name="Very long"><span><div></div></span>Very long</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Tatoos
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink8" href="javascript:void(8);" onclick="viewdiv('mydiv8');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv8" style="display:none;">
							<div class="tat-m">
								<label class="q-check"><input type="checkbox" name="No tattoos"><span><div></div></span>No tattoos</label>
							</div>
							<div class="tat-m">
								<label class="q-check"><input type="checkbox" name="Has tattoos" id = 'cb8' onchange = 'showOrHide("cb8", "cat8");'/><span><div></div></span>Has tattoos</label>
							</div>
							<div class="tat-h" id="cat8">
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
									<label class="q-check"><input type="checkbox" name="Shoulder"><span><div></div></span>Shoulder</label>
									<label class="q-check"><input type="checkbox" name="Biceps"><span><div></div></span>Biceps</label>
									<label class="q-check"><input type="checkbox" name="Wrist"><span><div></div></span>Wrist</label>
									<label class="q-check"><input type="checkbox" name="Chest"><span><div></div></span>Chest</label>
									<label class="q-check"><input type="checkbox" name="Belly"><span><div></div></span>Belly</label>
									<label class="q-check"><input type="checkbox" name="Back"><span><div></div></span>Back</label>
									<label class="q-check"><input type="checkbox" name="Loin"><span><div></div></span>Loin</label>
									<label class="q-check"><input type="checkbox" name="Hip"><span><div></div></span>Hip</label>
									<label class="q-check"><input type="checkbox" name="Shin"><span><div></div></span>Shin</label>
									<label class="q-check"><input type="checkbox" name="Ankle"><span><div></div></span>Ankle</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Tat-Others"><span><div></div></span>Others</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Piercing
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink9" href="javascript:void(9);" onclick="viewdiv('mydiv9');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv9" style="display:none;">
							<div class="pier-m">
								<label class="q-check"><input type="checkbox" name="No piercing"><span><div></div></span>No piercing (earlobes don't count)</label>
							</div>
							<div class="pier-m">
								<label class="q-check"><input type="checkbox" name="Has other piercing" id = 'cb9' onchange = 'showOrHide("cb9", "cat9");'/><span><div></div></span>Has other piercing:</label>
							</div>
							<div class="pier-h" id="cat9">
									<label class="q-check"><input type="checkbox" name="Ear tunnel"><span><div></div></span>Ear tunnel</label>
									<label class="q-check"><input type="checkbox" name="Eyebrows"><span><div></div></span>Eyebrows</label>
									<label class="q-check"><input type="checkbox" name="Tongue"><span><div></div></span>Tongue</label>
									<label class="q-check"><input type="checkbox" name="Nose"><span><div></div></span>Nose</label>
									<label class="q-check"><input type="checkbox" name="Nipple"><span><div></div></span>Nipple</label>
									<label class="q-check"><input type="checkbox" name="Navel"><span><div></div></span>Navel</label>
									<label class="q-check"><input type="checkbox" name="Body implants"><span><div></div></span>Body implants</label>
									<label class="q-check"><input type="checkbox" name="Pier-Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Additional skills
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink10" href="javascript:void(10);" onclick="viewdiv('mydiv10');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv10" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Actor or Actress</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Film Acting</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>TV Acting</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Dance</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Music</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Extreme Sports</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Stage Agting</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Commercial Acting</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Singer</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Performance Artist</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Voice Over Talent</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Languages
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink11" href="javascript:void(11);" onclick="viewdiv('mydiv11');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv11" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Compensation
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink12" href="javascript:void(12);" onclick="viewdiv('mydiv12');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv12" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Photo/Time For Print"><span><div></div></span>Photo/Time For Print</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Contracted or not
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink13" href="javascript:void(13);" onclick="viewdiv('mydiv13');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv13" style="display:none;">
							<select>
								<option>Contacted with the agency</option>
								<option>Not contacted</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Travel ability
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink14" href="javascript:void(14);" onclick="viewdiv('mydiv14');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv14" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Ready to travel"><span><div></div></span>Ready to travel</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Work within location"><span><div></div></span>Work within location</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data photographer"><!-- Фотограф -->
				<h1>Photographer Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<select>
							<option>All</option>
							<option>Male</option>
							<option>Female</option>
						</select>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 1 year</option>
								<option>1-5 years</option>
								<option>More that 5 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Work areas
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink4" href="javascript:void(4);" onclick="viewdiv('mydiv4');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv4" style="display:none;">
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Advertising photographers"><span><div></div></span>Advertising photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Beauty & Hair photographers"><span><div></div></span>Beauty & Hair photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Black & Wite photographers"><span><div></div></span>Black & Wite photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Children photographers"><span><div></div></span>Children photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Celebrity/Entertainment"><span><div></div></span>Celebrity/Entertainment</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Fashion/Editorials photographers"><span><div></div></span>Fashion/Editorials photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Fitness/body photographers"><span><div></div></span>Fitness/body photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="People & lifestyle photographers"><span><div></div></span>People & lifestyle photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Portrait photographers"><span><div></div></span>Portrait photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Sports/Action photographers"><span><div></div></span>Sports/Action photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Reportage photographers"><span><div></div></span>Reportage photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Runway/Fashion Event photographers"><span><div></div></span>Runway/Fashion Event photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Swimwear & lingerie photographers"><span><div></div></span>Swimwear & lingerie photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Travel photographers"><span><div></div></span>Travel photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Underwater photographers"><span><div></div></span>Underwater photographers</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Photo post production"><span><div></div></span>Photo post production (retouching)</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Film/Video production"><span><div></div></span>Film/Video production</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Commercial production" id='cb13' onchange='showOrHide("cb13", "cat13");'/><span><div></div></span>Commercial production:</label>
							</div>
							<div  class="w-a-h" id="cat13">
									<label class="q-check"><input type="checkbox" name="TV/Video Production"><span><div></div></span>TV/Video Production</label>
									<label class="q-check"><input type="checkbox" name="Film/Video Post"><span><div></div></span>Film/Video Post Production & CGI</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="CGI" id='cb14' onchange='showOrHide("cb14", "cat14");'/><span><div></div></span>CGI & 3D (CGI & 3D):</label>
							</div>
							<div  class="w-a-h" id="cat14">
									<label class="q-check"><input type="checkbox" name="Motion graphics"><span><div></div></span>Motion graphics</label>
									<label class="q-check"><input type="checkbox" name="Sound design/scoring"><span><div></div></span>Sound design/scoring</label>
							</div>
							<div class="w-a-m">
								<label class="q-check"><input type="checkbox" name="Rental studios"><span><div></div></span>Rental studios</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Work type
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink5" href="javascript:void(5);" onclick="viewdiv('mydiv5');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv5" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Studio
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink6" href="javascript:void(6);" onclick="viewdiv('mydiv6');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv6" style="display:none;">
							<select>
								<option>Provided</option>
								<option>Not provided</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Make-Up Artist
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink7" href="javascript:void(7);" onclick="viewdiv('mydiv7');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv7" style="display:none;">
							<select>
								<option>Provided</option>
								<option>Not provided</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Hair Stylist
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink8" href="javascript:void(8);" onclick="viewdiv('mydiv8');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv8" style="display:none;">
							<select>
								<option>Provided</option>
								<option>Not provided</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Compensation
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink12" href="javascript:void(12);" onclick="viewdiv('mydiv12');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv12" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Photo/Time For Print"><span><div></div></span>Photo/Time For Print</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Languages
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink11" href="javascript:void(11);" onclick="viewdiv('mydiv11');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv11" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Travel ability
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink14" href="javascript:void(14);" onclick="viewdiv('mydiv14');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv14" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Ready to travel"><span><div></div></span>Ready to travel</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Work within location"><span><div></div></span>Work within location</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data designer"><!-- Дизайнер -->
				<h1>Designer Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<select>
							<option>All</option>
							<option>Male</option>
							<option>Female</option>
						</select>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Work type
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink5" href="javascript:void(5);" onclick="viewdiv('mydiv5');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv5" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Contracted or not
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink13" href="javascript:void(13);" onclick="viewdiv('mydiv13');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv13" style="display:none;">
							<select>
								<option>Contacted with the agency</option>
								<option>Not contacted</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 1 year</option>
								<option>1-5 years</option>
								<option>More that 5 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Compensation
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink12" href="javascript:void(12);" onclick="viewdiv('mydiv12');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv12" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Photo/Time For Print"><span><div></div></span>Photo/Time For Print</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Languages
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink11" href="javascript:void(11);" onclick="viewdiv('mydiv11');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv11" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Travel ability
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink14" href="javascript:void(14);" onclick="viewdiv('mydiv14');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv14" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Ready to travel"><span><div></div></span>Ready to travel</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Work within location"><span><div></div></span>Work within location</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data agency"><!-- Агентство -->
				<h1>Agency Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Type/Specialization
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Agency"><span><div></div></span>Agency</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Publisher"><span><div></div></span>Publisher</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Site"><span><div></div></span>Model Site</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Scout"><span><div></div></span>Model Scout</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Event Manager"><span><div></div></span>Event Manager/Staffing company</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Casting Director"><span><div></div></span>Casting Director</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Advertiser"><span><div></div></span>Advertiser</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Manager"><span><div></div></span>Manager</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Stilist"><span><div></div></span>Stilist+MUA</label>
							</div>
							<div>
								Other:<input type="text" name="Spec-Other" placeholder="Other">
							</div>
						</div>
					</div>
				</div>
				<p>Model Requirements</p>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<select>
								<option>All</option>
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Age
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Children</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Teens</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Adults</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Senior (40+)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Model category:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Parts" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
							</div>
							<div class="mc-h" id="cat21">
									<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
									<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
									<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
									<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
									<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Print photo model" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
							</div>
							<div class="mc-h" id="cat22">
									<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
									<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Glamour model" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
							</div>
							<div class="mc-h" id="cat23">
									<label class="q-check"><input type="checkbox" name="Lingery/Body model"><span><div></div></span>Lingery/Body model</label>
									<label class="q-check"><input type="checkbox" name="Swimear model"><span><div></div></span>Swimear model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data brand"><!-- Бренд -->
				<h1>Brand Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Brand location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Product range:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Clothes"><span><div></div></span>Clothes</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Underwear"><span><div></div></span>Underwear</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Swimwear"><span><div></div></span>Swimwear</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Shoes"><span><div></div></span>Shoes</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Accessories"><span><div></div></span>Accessories</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Handbags"><span><div></div></span>Handbags</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Jewelry"><span><div></div></span>Jewelry</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Bijouterie"><span><div></div></span>Bijouterie</label>
							</div>
							<div>
								Other:<input type="text" name="Spec-Other" placeholder="Other">
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Trading:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select class="ge">
								<option>Domestic market</option>
								<option>International market</option>
								<option>Domestic and international market</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data repres"><!-- Представитель -->
				<h1>Company Representative Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Languages
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink11" href="javascript:void(11);" onclick="viewdiv('mydiv11');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv11" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data media"><!-- Медиа -->
				<h1>Media Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Position:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chief Editor "><span><div></div></span>Chief Editor</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Editor"><span><div></div></span>Editor</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Blogger"><span><div></div></span>Blogger</label>
							</div>
							<div>
								Other:<input type="text" class="w-s" placeholder="Name of magazine/blog/online journal *:"></input>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Languages
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink11" href="javascript:void(11);" onclick="viewdiv('mydiv11');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv11" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Travel ability
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink14" href="javascript:void(14);" onclick="viewdiv('mydiv14');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv14" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Ready to travel"><span><div></div></span>Ready to travel</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Work within location"><span><div></div></span>Work within location</label>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->
			<div class="crit-data stylist"><!-- Стилист -->
				<h1>Stylist Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Type/Specialization
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Agency"><span><div></div></span>Agency</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Publisher"><span><div></div></span>Publisher</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Site"><span><div></div></span>Model Site</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Scout"><span><div></div></span>Model Scout</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Event Manager"><span><div></div></span>Event Manager/Staffing company</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Casting Director"><span><div></div></span>Casting Director</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Advertiser"><span><div></div></span>Advertiser</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Manager"><span><div></div></span>Manager</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Stilist"><span><div></div></span>Stilist+MUA</label>
							</div>
							<div>
								Other:<input type="text" name="Spec-Other" placeholder="Other">
							</div>
						</div>
					</div>
				</div>
				<p>Model Requirements</p>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<select>
								<option>All</option>
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Age
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Children</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Teens</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Adults</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Senior (40+)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Model category:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Parts" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
							</div>
							<div class="mc-h" id="cat21">
									<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
									<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
									<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
									<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
									<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Print photo model" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
							</div>
							<div class="mc-h" id="cat22">
									<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
									<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Glamour model" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
							</div>
							<div class="mc-h" id="cat23">
									<label class="q-check"><input type="checkbox" name="Lingery/Body model"><span><div></div></span>Lingery/Body model</label>
									<label class="q-check"><input type="checkbox" name="Swimear model"><span><div></div></span>Swimear model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data artist"><!-- Артист -->
				<h1>Hair Stylist/Artist Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Brand location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Type/Specialization
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Agency"><span><div></div></span>Agency</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Publisher"><span><div></div></span>Publisher</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Site"><span><div></div></span>Model Site</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Scout"><span><div></div></span>Model Scout</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Event Manager"><span><div></div></span>Event Manager/Staffing company</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Casting Director"><span><div></div></span>Casting Director</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Advertiser"><span><div></div></span>Advertiser</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Manager"><span><div></div></span>Manager</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Stilist"><span><div></div></span>Stilist+MUA</label>
							</div>
							<div>
								Other:<input type="text" name="Spec-Other" placeholder="Other">
							</div>
						</div>
					</div>
				</div>
				<p>Model Requirements</p>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<select>
								<option>All</option>
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Age
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Children</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Teens</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Adults</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Senior (40+)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Model category:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Parts" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
							</div>
							<div class="mc-h" id="cat21">
									<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
									<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
									<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
									<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
									<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Print photo model" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
							</div>
							<div class="mc-h" id="cat22">
									<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
									<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Glamour model" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
							</div>
							<div class="mc-h" id="cat23">
									<label class="q-check"><input type="checkbox" name="Lingery/Body model"><span><div></div></span>Lingery/Body model</label>
									<label class="q-check"><input type="checkbox" name="Swimear model"><span><div></div></span>Swimear model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="crit-data mua"><!-- МУА -->
				<h1>Brand Search</h1>
				<span class="hr"></span>
				<a href="">Reset filter</a>
				<a href="">Show</a>
				<div class="d-wrap">
					<div class="details-item">
						Brand location
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<script type="text/javascript">
								function showOrHide(cb, cat) {
									cb = document.getElementById(cb);
									cat = document.getElementById(cat);
									if (cb.checked) cat.style.display = "block";
								else cat.style.display = "none";
								}
							</script>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
								<div class="loc loc-ch" id="cat1">
									<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
									<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
									<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
									<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
								<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
								<div class="loc loc-en" id="cat2">
									<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
									<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
									<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
								<div class="loc loc-fr" id="cat3">
									<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
									<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
									<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
								<div class="loc loc-ge" id="cat4">
									<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
									<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
									<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
								<div class="loc loc-it" id="cat5">
									<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
									<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
									<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
									<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
									<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
								<div class="loc loc-usa" id="cat6">
									<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
									<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
									<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
									<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
									<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
								</div>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
							</div>
							<div class="loc loc-m">
									<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Proffessional experience
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink3" href="javascript:void(3);" onclick="viewdiv('mydiv3');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv3" style="display:none;">
							<select>
								<option>Up to 10 years</option>
								<option>10-20 years</option>
								<option>More that 20 years</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Type/Specialization
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Agency"><span><div></div></span>Agency</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Publisher"><span><div></div></span>Publisher</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Site"><span><div></div></span>Model Site</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Model Scout"><span><div></div></span>Model Scout</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Event Manager"><span><div></div></span>Event Manager/Staffing company</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Casting Director"><span><div></div></span>Casting Director</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Advertiser"><span><div></div></span>Advertiser</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Manager"><span><div></div></span>Manager</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Stilist"><span><div></div></span>Stilist+MUA</label>
							</div>
							<div>
								Other:<input type="text" name="Spec-Other" placeholder="Other">
							</div>
						</div>
					</div>
				</div>
				<p>Model Requirements</p>
				<div class="d-wrap">
					<div class="details-item">
						Gender
					</div>
					<div class="details-data">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<select>
								<option>All</option>
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						Age
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Children</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Teens</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Adults</label>
							</div>
							<div>
								<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Senior (40+)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="d-wrap">
					<div class="details-item">
						*Model category:
					</div>
					<div class="details-data general-input">
						<a class="hiding" id="toggleLink1" href="javascript:void(1);" onclick="viewdiv('mydiv1');" data-text-show="-" data-text-hide="+">+</a>
						<div id="mydiv1" style="display:none;">
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Body Parts" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
							</div>
							<div class="mc-h" id="cat21">
									<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
									<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
									<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
									<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
									<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
									<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
									<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
									<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Print photo model" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
							</div>
							<div class="mc-h" id="cat22">
									<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
									<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Glamour model" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
							</div>
							<div class="mc-h" id="cat23">
									<label class="q-check"><input type="checkbox" name="Lingery/Body model"><span><div></div></span>Lingery/Body model</label>
									<label class="q-check"><input type="checkbox" name="Swimear model"><span><div></div></span>Swimear model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
							</div>
							<div class="mc-m">
								<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="s-main">

		</section>
		<div class="clear"></div>
	</article>
	<article id="sbph" class="sbph">
		<h2>Choose album for searching photo</h2>
		<span class="hr"></span>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Editorials
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Runway
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Beauty
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Portraits/Celebrity
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Glamour/Nude
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Commercial/Advertising
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Street Fashion
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Creative Projects
			</div>
		</div>
		<div class="m-j-albums fa fa-camera">
			<img src="" alt="">
			<div class="caption">
				Spec Shoot
			</div>
		</div>
		<div class="m-j-albums fa fa-video-camera">
			<img src="" alt="">
			<div class="caption">
				Videos
			</div>
		</div>
	</article>
	<div class="clear"></div>
</div>
<!-- Раскрывающиеся пункты параметров поиска -->
<!--a id="toggleLink" href="javascript:void(0);" onclick="viewdiv('mydiv');" data-text-show="Спрятать блок" data-text-hide="Показать блок">Показать блок</a>
<div id="mydiv" style="display:none;">text text text</div-->

<script>
    function viewdiv(id) {
        var el = document.getElementById(id);
        var link1 = document.getElementById('toggleLink1');
        var link2 = document.getElementById('toggleLink2');
        var link3 = document.getElementById('toggleLink3');
        var link4 = document.getElementById('toggleLink4');
        var link5 = document.getElementById('toggleLink5');
        var link6 = document.getElementById('toggleLink6');
        var link7 = document.getElementById('toggleLink7');
        var link8 = document.getElementById('toggleLink8');
        var link9 = document.getElementById('toggleLink9');
        var link10 = document.getElementById('toggleLink10');
        var link11 = document.getElementById('toggleLink11');
        var link12 = document.getElementById('toggleLink12');
        var link13 = document.getElementById('toggleLink13');
        var link14 = document.getElementById('toggleLink14');
        if (el.style.display == "block") {
            el.style.display = "none";
            link.innerText = link.getAttribute('data-text-hide');
        } else {
            el.style.display = "block";
            link.innerText = link.getAttribute('data-text-show');
        }
    }

</script>
<!-- Раскрывающиеся пункты параметров поиска Конец-->
<!-- Переключение способов поиска во время верстки -->
<script type="text/javascript">
	$(function() {
		var insert = $('#insert');
		$('a[rel="insert"]').click(function () {
			insert.find('article').fadeOut(600);
			insert.find('#' + $(this).attr('href')).fadeIn(600);
			return false;
		});
	});
</script>