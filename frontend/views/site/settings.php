<?php
$script = <<< JS

	$(".krutilka_page1_key").click(function(){
        console.log('sss');
		$(".krutilka_page1").attr("style","display:block;");
		$(".krutilka_page2").attr("style","display:none;");
		$(".nav-s-item").removeClass("active");
		$(".krutilka_page1_key").addClass("active");
	});
	$(".krutilka_page2_key").click(function(){
        console.log('sss');

		$(".krutilka_page2").attr("style","display:block;");
		$(".krutilka_page1").attr("style","display:none;");
		$(".nav-s-item").removeClass("active");
		$(".krutilka_page2_key").addClass("active");
	});
JS;

$this->registerJs($script, yii\web\View::POS_READY);
$this->title = 'Fashion Greatness | ' . Yii::t('app', 'Натсройки'); ?>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

echo $this->render('_visitor_nav'); ?>

<div class="person-wrapper visitor">
	<div class="set-wrapper">
		<h2>Settigs</h2>
		<span class="hr"></span>
		<nav class="nav-settings">
			<div class="nav-s-item krutilka_page1_key active" href="pass">Password</div>
			<div class="nav-s-item krutilka_page2_key" href="type">Account Type</div>
		</nav>

<div id="profile_error_password_container"></div>
<div class="krutilka_page1">
		<h2>Сменить пароль:</h2>
		<br>
 	     <form class="form_password" id="form-password" action="<?= URL::to(['site/settings']);?>" method="post">
			   <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
            <div class="row">
				<div class="textLeft">
					Old Password: *
				</div>
				<div class="textRight">
					<input type="password" name="Users[old_password]" id="passwordold" placeholder="">
				</div>
			</div>
			<div class="row">
				<div class="textLeft">
					New Password: *
				</div>
				<div class="textRight">
					<input type="password" name="Users[new_password]" id="passwordnew" placeholder="">
				</div>
			</div>
			<div class="row">
				<div class="textLeft">
					Repeat Password: *
				</div>
				<div class="textRight">
					<input type="password" name="Users[repeat_password]" id="passwordtwo" placeholder="">
				</div>
			</div>
            <div><?= $msg ?></div>
			<div class="exeptleft">
				<hr>
				Fields macked with * are mandatory
			</div>
			<div class="exeptpadding">
				<input class="price-btn lft-btn" type="submit" id="save" value="save">
				<input class="price-btn rght-btn" type="reset" value="cancel"></input>
			</div>
		</form>
</div>

<div class="type krutilka_page2" style="display:none;">

<div class="price-container">
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
						АККАУНТ<!-- Account -->
						<br>
						<span class="price-name">КЛАССИЧЕСКИЙ<!-- Classic --></span>
						<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$25</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">3 ИЗДАНИЯ ВАКАНСИЙ<br>
20 СОВМЕСТНАЯ ПРЕДЛОЖЕНИЯ<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ОГРАНИЧЕНОЙ в профайле<br>
LIMITED ИЗГОТОВЛЕНИЕ<br></p>
						<a href="#popup-register" data-tarif="5" class="price-btn" id="join1">РЕГИСТРАЦИЯ<!-- JOIN --></a>
						<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">ВИП<!-- VIP --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$250</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br></p>
					<a href="#popup-register" data-tarif="6" class="price-btn" id="join2">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">КОРПОРАТИВНЫЙ<!-- Corporate --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$350</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br>
20 ДОПОЛНИТЕЛЬНЫЕ CLASSIC ПРОФИЛИ для сотрудников Вашей компании<br></p>
					<a href="#popup-register" data-tarif="7" class="price-btn" id="join3">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>

</div>
</div>
</div>