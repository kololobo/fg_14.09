<?php
/**
 * Created by PhpStorm.
 * User: phpNT - http://phpnt.com
 * Date: 14.09.2016
 * Time: 16:29
 */

use phpnt\bootstrapNotify\BootstrapNotify;
use common\widgets\SignupAndLogin2\SignupAndLogin;
use common\models\Identity;

$this->title = Yii::t('app', 'Главная');
?>
<?= BootstrapNotify::widget() ?>
<div class="site-index" style="margin-top: 200px;">
    <?php
    $value = 'password';
    $pass = Identity::encript($value);
    d($pass);
    $value = Identity::decript($pass);
    d($value);
    ?>
    <div class="col-md-4 col-md-offset-4 text-center">
        <?= SignupAndLogin::widget() ?>
    </div>
</div>