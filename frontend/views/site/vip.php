<div class="c-bg classic">
	<a class="c-bg-btn" href="">Change</a> <?/* Сменить фон */?> 
</div>
<div class="person-wrapper">
	<div class="c-main">
		<div class="c-m-widgets">
	<div class="c-left-col">
		<div class="vis-info">
			<div class="p-r-nr">
				<p>Model</p>
				<p class="name id">Name or ID</p>
				<br/>
				<p>Rating</p>			<? /* Рейтинг в % в виде батарейки - макс = 100% */ ?>
				<p>Rating position</p>		<? /* Позиция в рейтинге например 399 из 399 */ ?>
				<p class="acc-type">Account type <span>Classic</span></p>	<? /* Тип аккаунта */?>
				<div class="acc-info-icons">
					<p class="like-icon"></p>
					<p class="dial-icon"></p>
				</div>
				<div class="acc-ph">
					<div class="acc-ph-edit">
					<a class="" href="" id="avatar_edit">Edit</a>			<? /* Edit */ ?>
					<a class="" href="" id="avatar_remove">Remove</a>		<? /* Remove */ ?>
					</div>
					<? }; ?>
					<img src="/img/vis-photo.jpg" alt="Фотография профиля" id="avatar_img">
				</div>
			</div>

			
			<div class="acc-info">
				<nav class="block-btn">
					<div class="btn-item">
						<a href="/subscription">Subscription</a>		<? /* Подписки */ ?>
					</div>
					<div class="btn-item">
						<a href="/recommendations">Recommendations</a>	<? /* Рекомендации */ ?>
					</div>
					<div class="btn-item">
						<a href="/vacancies">Vacancies</a>		<? /* Вакансии */ ?>
					</div>
				</nav>
				<? /* Показывать только для чужих анкет */ ?>
				<div class="acc-gen-info">
					<h2>General info</h2>
					<span class="hr"></span>
				</div>
				<div class="acc-phys-det">
					<h2>Physcal details</h2>
					<span class="hr"></span>
				</div>
				<div class="acc-prof-det">
					<h2>Physical details</h2>
					<span class="hr"></span>
				</div>
			</div>
		</div>
	</div>
		<div class="c-m-nav">
			<ul class="tmn">
				<li>
					<a href="/journal">Journal</a>		<? /* Журнал */ ?>
				</li>
				<li>
					<a href="/events">Events</a>		<? /* События */ ?>
				</li>
				<li>
					<a href="">Search</a>			<? /* Поиск */ ?>
					<ul>
						<li>
							<a href="/searchevents">Events</a>		<? /* События */ ?>
						</li>
						<li>
							<a href="/searchpeople">People</a>		<? /* Люди */ ?>
						</li>
						<li>
							<a href="/searchjob">Job</a>			<? /* Работа */ ?>
						</li>
					</ul>
				</li>
			</ul>
		</div>
			<div class="c-m-calendar">
				<h2>Calendar</h2>
				<span class="hr"></span>
				<div class="cal-wrap">
					<div class="c-c">
						<div class="cal-p">
							&lt;
						</div>
						<div class="cal-title">
							<font id="cal-month">Month</font> <font id="cal-year">2016</font>
						</div>
						<div class="cal-n">
							&gt;
						</div>
						<!-- Дни недели -->
						<div class="c-w-d">
							
						</div>
						<div class="c-d-d" id="cal-day">
							
						</div>
					</div>
					<div class="c-e">
						<div class="c-e-t">
							<div class="c-e-1">все</div>
							<div class="c-e-2">мои</div>
							<div class="c-e-3">занят</div>
						</div>
						<div class="c-e-b">
							lined events
						</div>
					</div>
				</div>
			</div>
			
			<div class="c-m-jobs">
				<h2>Featured jobs</h2>
				<span class="hr"></span>
				<nav class="ln-nav">
					<ul>
						<li>
							<a href="">Published vacancies</a>
						</li>
						<li>
							<a href="">My Drafts</a>
						</li>
						<li>
							<a href="">Closed Vacancies</a>
						</li>
					</ul>
				</nav>
				<div class="fj-wrap">
					<div class="fj-i-wrap">
						<a href=""><h3>Agency needs Hair Stilist, MUA</h3></a></br>
						<p><b>Professional Experience: </b> </p></br>
						<p><b>Compensation: </b>Payment Only</p></br>
						<p><b>Additional expenses to be covered: </b>Covered</p></br>
						<p><b>Place/Location: </b>Ukraine</p></br>
						<p><b>Number of views: </b>27</p></br>
						<p><b>Who published: </b>Agency</p></br>
						<p><b>Published: </b></p></br>
						<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
						<p><b>Date of project realization: </b></p></br>
						<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
						<p><b>Time of project duration: </b></p></br>
						<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
						<p><b>Expiration date: </b></p></br>
						<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
					</div>
					<div class="fj-i-wrap">
						<a href=""><h3>Model wanted, MUA</h3></a></br>
						<p><b>Professional Experience: </b> </p></br>
						<p><b>Compensation: </b>In exchange of experience</p></br>
						<p><b>Additional expenses to be covered: </b>Covered</p></br>
						<p><b>Place/Location: </b>London location</p></br>
						<p><b>Number of views: </b>22</p></br>
						<p><b>Who published: </b>Model</p></br>
						<p><b>Published:</b></p></br>
						<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
						<p><b>Date of project realization:</b></p></br>
						<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
						<p><b>Time of project duration:</b></p></br>
						<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
						<p><b>Expiration date:</b></p></br>
						<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
					</div>
					<div class="fj-i-wrap">
						<a href=""><h3>Model looking for photografer</h3></a></br>
						<p><b>Professional Experience: </b>1-5 years</p></br>
						<p><b>Compensation: </b>Payment Only</p></br>
						<p><b>Additional expenses to be covered: </b>Covered</p></br>
						<p><b>Place/Location: </b>London</p></br>
						<p><b>Number of views: </b>15</p></br>
						<p><b>Who published: </b>Model</p></br>
						<p><b>Published:</b></p></br>
						<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
						<p><b>Date of project realization:</b></p></br>
						<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
						<p><b>Time of project duration:</b></p></br>
						<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
						<p><b>Expiration date::</b></p></br>
						<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
					</div>
				</div>
			</div>
			
			<div class="c-m-news">
				<h2>Latest news</h2>

				<span class="hr"></span>
				<nav class="ln-nav">
					<ul>
						<li>
							<a href="">фотографии</a>
						</li>
						<li>
							<a href="">подписчики</a>
						</li>
						<li>
							<a href="">вакансии</a>
						</li>
						<li>
							<a href="">написанные рекоендации</a>
						</li>
						<li>
							<a href="">новые модели</a>
						</li>
					</ul>
				</nav>
				<div class="ln-item">
				<!-- Photo News -->
					<div class="ln-icon1">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

					<div class="ln-desc">
						added new photo to album ""
					</div>

					<div class="ln-icon2">
						<a href="">
							<img src="" alt="фото со ссылкой на альбом">
						</a>
					</div>

				</div>
				<div class="ln-item">
				<!-- Subscribers News -->
					<div class="ln-icon1">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

					<div class="ln-desc">
						followed
					</div>

					<div class="ln-icon2">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

				</div>
				<div class="ln-item">
				<!-- Vacancy News -->
					<div class="ln-icon1">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

					<div class="ln-desc">
						<a href="">
							added new vacancy ""
						</a>
					</div>

					<div class="ln-icon2">
						<!--a href="">
							<img src="" alt="фото со ссылкой на альбом">
						</a-->
					</div>

				</div>
				<div class="ln-item">
				<!-- Recommended News -->
					<div class="ln-icon1">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

					<div class="ln-desc">
						recommended
					</div>

					<div class="ln-icon2">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

				</div>
				<div class="ln-item">
				<!-- New Models -->
					<div class="ln-icon1">
						<a href="">
							<img src="" alt="аватарка агентства">
							<p>Имя/id</p>
						</a>
					</div>

					<div class="ln-desc">
						presents a new model
					</div>

					<div class="ln-icon2">
						<a href="">
							<img src="" alt="аватарка пользователя">
							<p>Имя/id</p>
						</a>
					</div>

				</div>
			</div>
			
			<div class="m-jobs">
				<h2>My jobs</h2>
				<span class="hr"></span>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Editorials
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Runway
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Beauty
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Portraits/Celebrity
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Glamour/Nude
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Commercial/Advertising
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Street Fashion
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Creative Projects
					</div>
				</div>
				<div class="m-j-albums fa fa-camera">
					<img src="" alt="">
					<div class="caption">
						Spec Shoot
					</div>
				</div>
				<div class="m-j-albums fa fa-video-camera">
					<img src="" alt="">
					<div class="caption">
						Videos
					</div>
				</div>
				<div class="m-j-add">
					add
				</div>
			</div>
			
			<div class="c-m-event" style="display:none;">
				
<div class="cal-modal">
	<div class="c-m-wrap">
		<div class="c-m-form">
			<div class="c-m-close">
				X
			</div>
			<h1>Месяц число год</h1>
			<form>
				<h3>Date</h3>
				<p>From:</p>
				<p>To:</p>
				<input class="date-from" value="from" type="date">
				<input class="date-to" value="to" type="date">
				<h3>Hours</h3>
				<select>
					<option>00</option>
					<option>01</option>
					<option>02</option>
					<option>03</option>
					<option>04</option>
					<option>05</option>
					<option>06</option>
					<option>07</option>
					<option>08</option>
					<option>09</option>
					<option>10</option>
					<option>11</option>
					<option>12</option>
					<option>13</option>
					<option>14</option>
					<option>15</option>
					<option>16</option>
					<option>17</option>
					<option>18</option>
					<option>19</option>
					<option>20</option>
					<option>21</option>
					<option>22</option>
					<option>23</option>
				</select>
				<select>
					<option>00</option>
					<option>01</option>
					<option>02</option>
					<option>03</option>
					<option>04</option>
					<option>05</option>
					<option>06</option>
					<option>07</option>
					<option>08</option>
					<option>09</option>
					<option>10</option>
					<option>11</option>
					<option>12</option>
					<option>13</option>
					<option>14</option>
					<option>15</option>
					<option>16</option>
					<option>17</option>
					<option>18</option>
					<option>19</option>
					<option>20</option>
					<option>21</option>
					<option>22</option>
					<option>23</option>
				</select>
				<input type="text" name="date" class="tcal" value="" />
				<input></input>
				<select style="width: 93%;">
					<option class="loc loc-m">China:</option>
					<option class="loc loc-ch">Beijing</option>
					<option class="loc loc-ch">Hong Gong</option>
					<option class="loc loc-ch">Shanghai</option>
					<option class="loc loc-ch">Other</option>
					<option class="loc loc-m">England:</option>
					<option class="loc loc-en">London</option>
					<option class="loc loc-en">London area</option>
					<option class="loc loc-en">Other</option>
					<option class="loc loc-m">France:</option>
					<option class="loc loc-fr">Paris</option>
					<option class="loc loc-fr">Paris area</option>
					<option class="loc loc-fr">Other</option>
					<option class="loc loc-m">Germany:</option>
					<option class="loc loc-ge">Berlin</option>
					<option class="loc loc-ge">Berlin area</option>
					<option class="loc loc-ge">Other</option>
					<option class="loc loc-m">Italy:</option>
					<option class="loc loc-it">Milan</option>
					<option class="loc loc-it">Milan area</option>
					<option class="loc loc-it">Rome</option>
					<option class="loc loc-it">Rome area</option>
					<option class="loc loc-it">Other</option>
					<option class="loc loc-m">USA:</option>
					<option class="loc loc-usa">Los Angeles</option>
					<option class="loc loc-usa">Los Angeles area</option>
					<option class="loc loc-usa">New York</option>
					<option class="loc loc-usa">New York area</option>
					<option class="loc loc-usa">Other</option>
					<option class="loc loc-m">Ukraine</option>
					<option class="loc loc-m">Russia</option>
					<option class="loc loc-m">Other</option>
				</select>
				<input type="submit"></input>
			</form>
		</div>
	</div>
</div>
			</div>
		</div>
	</div>
</div>


<div class="avatar" id="avatar_e" style="display:none;">
<a href="" id="avatar_edit_v1"> 1. Выбрать на сервере </a>
<a href="" id="avatar_edit_v2"> 2. Загрузить из своего компа </a>
</div>

<div class="avatar" id="avatar_e1" style="display:none;">
Выбрать аватар на сервере
матрица аватор из папки img_avatar_small
</div>


<div class="avatar" id="avatar_e2" style="display:none;">
Загрузить аватар из своего компа
<form method="POST" enctype="multipart/form-data" action="" id="form_zag_fil" >
<input type="hidden" name="id" value="">
<input type="file" id="fil_1" name="img" class="button-img11111">
<input type="submit" id="sub_zag_fil" value="Загрузить файлы" class="button-zag111">
<p id="fil_aaa"></p>

</div>

</div>