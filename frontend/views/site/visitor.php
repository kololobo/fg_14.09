
<?php
use yii\helpers\Url;
?>
<?= $this->render('_visitor_nav') ?>


<div class="search-nav visitor">
    <div class="s-n-wrapper">
        <div class="search-type">
            <h2>Model photos</h2><!-- Сменяемая надпись в зависимости от выбраной категории поиска -->
            <span class="hr"></span>
        </div>
        <div class="prod-result">
            <p>The serch producted 68 results</p><!-- Надпись о колличестве найденых фото -->
        </div>
        <div class="prof-sel"><!-- Выбрать где искать фото  -->
            <ul>
                <li class="parent" style="padding: 7px 25px;">Выберите категорию для поиска
                    <ul>
                        <li><a href="#">Модель</a></li>
                        <li><a href="#">Фотограф</a></li>
                        <li><a href="#">Дизайнер</a></li>
                        <li><a href="#">Агентство</a></li>
                        <li class="parent">Эксперт
                            <ul>
                                <li><a href="#">Brand</a></li>
                                        <li><a href="#">Company Representative</a></li>
                                <li><a href="#">Media</a></li>
                                <li><a href="#">Stylist</a></li>
                                <li><a href="#">Hair Stylist/Artist</a></li>
                                <li><a href="#">MUA</a></li>
                            </ul>
                        </li>
                    <ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Контент странички -->
<div class="person-wrapper visitor">
    <div class="set-wrapper">
        <h2>Settigs</h2>
        <span class="hr"></span>
        <nav class="nav-settings">
            <a class="nav-s-item" href="pass">Password</a>
            <a class="nav-s-item active" href="type">Account Type</a>
        </nav>
        <div class="pass hide">
            <!-- < ? $this->Widget('WidPassword', array( 'items'=>array("cod"=>"visitor") ) ); ?> -->
        </div>
        <div class="type">
            <!-- < ? $this->Widget('WidAccType', array( 'items'=>array("cod"=>"visitor") ) ); ?> -->
        </div>
    <div>
        <div class="sbph">
            <h2>Choose album for searching photo</h2>
            <span class="hr"></span>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Editorials
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Runway
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Beauty
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Portraits/Celebrity
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Glamour/Nude
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Commercial/Advertising
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Street Fashion
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Creative Projects
                </div>
            </div>
            <div class="m-j-albums fa fa-camera">
                <img src="" alt="">
                <div class="caption">
                    Spec Shoot
                </div>
            </div>
            <div class="m-j-albums fa fa-video-camera">
                <img src="" alt="">
                <div class="caption">
                    Videos
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="j-main">
            <h2>Journal</h2>
            <span class="hr"></span>
            <div class="nav-bar">
                <ul class="nav2">
                    <li class="active">
                        <a href="#fn">FG News</a>
                    </li>
                    <li>
                        <a href="#fs">Fashion Shows</a>
                    </li>
                    <li>
                        <a href="#p">Persons</a>
                    </li>
                    <li>
                        <a href="#fl">Fashion life</a>
                    </li>
                    <li>
                        <a href="#b">Blog</a>
                    </li>
                </ul>
            </div>
            <div class="j-item">
                <div class="j-img">
                    <img src="">
                </div>
                <div class="j-txt">
                    <h3></h3>
                    <p></p>
                    <br>
                    <p></p>
                </div>
            </div>
            <div class="j-item">
                <div class="j-img">
                    <img src="">
                </div>
                <div class="j-txt">
                    <h3></h3>
                    <p></p>
                    <br>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="j-sidebar">
            <div class="j-t-port">
                <div class="j-t-head">
                    Top profile
                </div>
                <div class="j-port-cont">
                    <a href="">
                        <img src="">
                    </a>
                </div>
            </div>
            <div class="j-t-phot">
                <div class="j-t-head">
                    Top photo
                </div>
                <div class="j-phot-cont">
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                    <a href="#">
                        <img src="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Модальное окно для фото/видео -->
<div id="openModal" class="modal modal-ph">
    <div class="modalWrapper">
        <a href="#close">X</a>
        <img src="">
        <div class="modal-btn"><!-- 4 кнопки: фотограф, хозяин фото, модель, альбом в которам размещена фотография -->
            <a href="#photorrapher"></a>
            <a href="#profile"></a>
            <a href="#model"></a>
            <a href="#album"></a>
        </div>
        <p></p><!-- рейтинг фото -->
    </div>
</div>
<!-- Дальше не знаю, что... -->


<div class="avatar" id="avatar_e" style="display:none;">

<a href="" id="avatar_edit_v1"> 1. Выбрать на сервере </a>

<a href="" id="avatar_edit_v2"> 2. Загрузить из своего компа </a>

</div>



<div class="avatar" id="avatar_e1" style="display:none;">

Выбрать аватар на сервере

матрица аватор из папки img_avatar_small

<!-- < ? $this->Widget('WidAvatar'); ?> -->

</div>





<div class="avatar" id="avatar_e2" style="display:none;">

Загрузить аватар из своего компа

<form method="POST" enctype="multipart/form-data" action="" id="form_zag_fil" >

<input type="hidden" name="id" value="< ?=$this->tuser['id']?>">

<input type="file" id="fil_1" name="img" class="button-img11111">

<input type="submit" id="sub_zag_fil" value="Загрузить файлы" class="button-zag111">

<p id="fil_aaa"></p>

</form>

</div>

</div>





<script>

$(document).ready(function() {

    // Переменная куда будут располагаться данные файлов

    var files;

    $('input[type=file]').change(function(){

        files = this.files;

        id = this.id;

        aaa = "";

        for (i=0; i<files.length;++i) {

            //aaa += "<tr><td>" + files.item(i).name + "=" + files.item(i).size + "</td></tr>";

            aaa += "" + files.item(i).name + "=" + files.item(i).size + "<hr>";

        };

        $("#fil_aaa").append(aaa);

    });

    $('#form_zag_fil').on('submit', ( function(e) {

        e.preventDefault();

        var data = new FormData(this);

        //

        $.ajax({

            type: 'post',

            url: 'index.php?r=SiteAjax/Myavatar',

            processData: false,

            contentType: false,

            data: data,

            dataType: 'json',

            success: function(json){

                console.log(json);

                console.log(files);

                $("#avatar_img").attr('src', '../img_avatar_big/'+json['name']);

                $("#avatar_img_small").attr('src', '../img_avatar_small/'+json['name']);

                files = "";

                data  = "";

                //$("fil_0").val("");

                return false;

            },

            error: function(error){

                console.log("error");

                console.log(files);

                console.log(files[0].name);

                return false;

            }

        });

        //

    }));

    <? /* Заменить аватар - заменить в таб.users - Варианты :  */ ?>

    <? /* 1. из папки img_avatar_small */ ?>

    <? /* 2. загрузить из своего компа */ ?>

    $('#avatar_edit').click( function() {

        $('#avatar_e').css('display', '');

        return false;

    });

    <? /* 1. из папки img_avatar_small */ ?>

    $('#avatar_edit_v1').click( function() {

        $('#avatar_e').css('display', 'none');

        $('#avatar_e1').css('display', '');

        return false;

    });

    <? /* 2. загрузить из своего компа */ ?>

    $('#avatar_edit_v2').click( function() {

        $('#avatar_e').css('display', 'none');

        $('#avatar_e2').css('display', '');



        return false;

    });

    <? /* Удалить аватар - заменить в таб.users на фиксированное имя no_avatar.png */ ?>

    $('#avatar_remove').click( function() {



        return false;

    });

});

</script>

<?

        /*

        aaa = $(this).attr('href');

        bbb = $('.active').attr('href');

        $('.nav-s-item').removeClass('active');

        $(this).addClass('active');

        $('.'+aaa).removeClass('hide');

        $('.'+bbb).addClass('hide');

        */

?>