<?php
use yii\helpers\Url;

?>

<nav class="static-nav">
    <ul>
        <li <?php echo (Yii::$app->controller->action->id == 'about') ? 'class="current"' : ''; ?>>
            <a href="<?= Url::to('/static/about') ?>">About FG</a>
        </li>
        <li <?php echo (Yii::$app->controller->action->id == 'services') ? 'class="current"' : ''; ?>>
            <a href="<?= Url::to('/static/services') ?>">Our services</a>
        </li>
        <li <?php echo (Yii::$app->controller->action->id == 'mentors') ? 'class="current"' : ''; ?>>
            <a href="<?= Url::to('/static/mentors') ?>">Our mentors</a>
        </li>
        <li <?php echo (Yii::$app->controller->action->id == 'faq') ? 'class="current"' : ''; ?>>
            <a href="<?= Url::to('/static/faq') ?>">FAQ</a>
        </li>
        <li <?php echo (Yii::$app->controller->action->id == 'contacts') ? 'class="current"' : ''; ?>>
            <a href="<?= Url::to('/static/contacts') ?>">Contacts</a>
        </li>
    </ul>
</nav>