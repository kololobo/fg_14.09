<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Наши наставники'); ?>

<div class="static-wrapper">
	<a href="/" class="link-back">Fashion Greatness</a>  >> Our mentors
	<h2>About Fashion Greatness</h2>
	<span class="hr"></span>
    <?= $this->render('_static_nav') ?>
    <?=  $content ?>


</div>