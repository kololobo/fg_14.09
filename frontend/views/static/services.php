<?php $this->title = 'Fashion Greatness | ' . Yii::t('app', 'Наши сервисы'); ?>

<div class="static-wrapper">
	<a href="/" class="link-back">Fashion Greatness</a>  >> Our services
	<h2>About Fashion Greatness</h2>
	<span class="hr"></span>
    <?= $this->render('_static_nav') ?>
    <?=  $content ?>


</div>