function SetAge() {
    today = new Date();
    birth = new Date($('#general-date_of_birth').val());
    age = today.getFullYear() - birth.getFullYear();
    $('#age_input').val(age);
    if (age < 18){
        $('#legal_agree').show();
    } else if (age == 18){
        if ((today.getMonth() - birth.getMonth()) < 0){
            $('#legal_agree').show();
        } else if ((today.getMonth() - birth.getMonth()) == 0){
            if ((today.getDate() - birth.getDate()) < 0){
                $('#legal_agree').show();
            } else {
                $('#legal_agree').hide();
            }
        } else {
            $('#legal_agree').hide();
        }
    } else {
        $('#legal_agree').hide();
    }
}

SetAge();
$('#general-date_of_birth').change(function () {
    SetAge();
});

$('#modelsform-ethnicity').change(function(){
    if ($(this).val() == 'Other:'){
        $('#ethnicity_input').show()
    } else {
        $('#ethnicity_input').hide()
    }
})

$('#has_tatoos').change(function () {
    tatoo = $('input[name="ModelsForm[has_tatoo]"]:checked').val();
    if (tatoo == 1){
        $('#tatoos').show();
    } else {
        $('#tatoos').hide();
    }
})

$('#has_piercing').change(function () {
    tatoo = $('input[name="ModelsForm[has_piercing]"]:checked').val();
    if (tatoo == 1){
        $('#piercing').show();
    } else {
        $('#piercing').hide();
    }
})

