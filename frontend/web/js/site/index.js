var tek_slid = 0;
var mou_dow = 0;
var mou_x = 0;
var mou_t = 0;
var napr = '';

function ProfExpert(id, expert) {
    if ($(id).val() == expert){
        $(id).hide();
        $('#type_expert').show();
    }
}

$(document).ready(function() {

    var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

    //CALL owl-carousel
    $(".team-carousel").owlCarousel({
        loop:true,
        nav:false,
        navText: "",
        responsive:{
            0:{
                items:1
            },
            660:{
                items:2
            },
            980:{
                items:3
            }
        }
    });
    $('#but_team_next').click( function() {
        $(".team-carousel").trigger('next.owl.carousel');
    });
    $('#but_team_prev').click( function() {
        $(".team-carousel").trigger('prev.owl.carousel');
    });

    //переход по лендингу с затемнениями
    var insert = $('#insert');
    $('a[rel="insert"]').click(function () {
        insert.find('article').fadeOut(600);
        $('#top-menu > ul > li').removeClass("active");
        var menu = $(this);
        insert.find('#' + $(this).attr('href')).fadeIn(600, function(){
            menu.parent().addClass("active");
        });
        return false;
    });

    //Преимущества (листалка)
    $('.slid').click( function() {
        sme1 = $('.item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""));
        sme3 = sme2 * $(this).attr('sme');
        tek_slid = $(this).attr('sme');
        $("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
        $(".c-p").removeClass("current");
        $(this).parent().addClass("current");
        return false;
    });
    $(window).resize(function () {
        sme1 = $('.item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""));
        sme3 = sme2 * tek_slid;
        $("#ulsl").css('left',sme3);
    });
    $('.item').mousemove(function(eventObject){
        if (mou_dow == 1) {
            sme1 = $("#ulsl").css('left');
            sme2 = Number(sme1.replace(/\D+/g,"")); // Math.abs(
            mou_t = eventObject.pageX;
            smed = mou_x - mou_t;
            sme3 = (sme2 + Number(smed)) * (-1);
            $("#ulsl").css('left',sme3);
            mou_x = mou_t;
            if (smed > 0) {napr = 'L'};
            if (smed < 0) {napr = 'R'};
            if (smed == 0) {napr = ''};
        };
    });
    $('.item').mousedown(function(eventObject){
        mou_dow = 1; mou_x = eventObject.pageX; mou_t = mou_x;
    });
    $('.item').mouseup(function(eventObject){
        mou_dow = 0;
        if ((napr == 'L') && (tek_slid != -4)) { --tek_slid; };
        if ((napr == 'R') && (tek_slid != 0) ) { ++tek_slid; };
        $('#s'+tek_slid).click();
    });
    $('.item').mouseout(function(){
        mou_dow = 0; napr = ''; // console.log(mou_dow);
    });
})



