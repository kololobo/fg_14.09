-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.9-MariaDB - mariadb.org binary distribution
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных yii2advanced
CREATE DATABASE IF NOT EXISTS `yii2advanced` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `yii2advanced`;


-- Дамп структуры для таблица yii2advanced.our_team
CREATE TABLE IF NOT EXISTS `our_team` (
  `user_id` int(11) NOT NULL,
  `position` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `citate` text NOT NULL,
  `photo` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `position` (`position`),
  CONSTRAINT `team_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы yii2advanced.our_team: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `our_team` DISABLE KEYS */;
INSERT INTO `our_team` (`user_id`, `position`, `name`, `profession`, `citate`, `photo`, `email`) VALUES
	(19, 10, 'АРТУР ФРАНЧУК ФОН МАНШТЕЙН', 'ДИРЕКТОР, МОДА ВЕЛИЧИЕ ГРУППА, ООО', '\r\nЧто делает меня по-настоящему счастливым, так это тот факт, что мы объединяем лучших профессионалов индустрии моды со всех уголков земного шара.', 't_f1.jpg', 'afvm@fashiongreatness.com'),
	(20, 20, 'АЛЕКС', 'ГЛАВНЫЙ РЕДАКТОР, FASHIONGREATNESS.COM', 'Каждый раз, когда я начинаю писать статью, я спрашиваю себя "эта информация может быть применена на практике?". Если ответ да, я это пишу', 't_f2.jpg', 'Alexandra.S@fashiongreatness.com'),
	(21, 30, 'АННА', 'МЕНЕДЖЕР ПРОЕКТА,FASHIONGREATNESS.COM', 'Будь собой, люби то, что делаешь, уважай свой талант и будь успешным!', 't_f3.jpg', 'Anna.May@fashiongreatness.com');
/*!40000 ALTER TABLE `our_team` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
